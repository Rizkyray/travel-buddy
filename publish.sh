#!/bin/bash
env=$1
if [ $env == "prod" ]; then
    cp -rf public/img/ ../public_html/ && cp -rf public/css/ ../public_html/ && cp -rf public/js/ ../public_html/ && cp -rf public/dashboard-assets/ ../public_html/ && cp public/mix-manifest.json ../public_html/
    echo "Assets published"
elif [ $env == "staging" ]; then
    cp -rf public/css/ ../public_html/stagingtb && cp -rf public/js/ ../public_html/stagingtb && cp -rf public/dashboard-assets/ ../public_html/stagingtb && cp public/mix-manifest.json ../public_html/stagingtb
    echo "Assets published"
fi
