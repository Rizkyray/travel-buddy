<?php

namespace App\Helpers;

use \App\Order;

class Helper
{
    /**
     * Convert array of string to array of integer
     * 
     * @param $arr Array of string
     * 
     * @return Illuminate\Support\Collection
     */
    public static function arrayToInt($arr) {
        $newArr = [];
        
        $newArr = collect($arr)->map(function($val) {
            $intVal = (int) $val;
            
            if ($intVal === 0) {
                return null;
            }

            return (int) $val;
        });
        return $newArr;
    }

    /**
     * 
     * Return days difference between to dates.
     * 
     * @param $start_date DateTime
     * @param $end_date DateTime
     * 
     * @return Integer
     */
    public static function daysDifference($start_date, $end_date) {
        return $end_date->diff($start_date)->d + 1;
    }

    /**
     * Generate unique order number
     * 
     * @return string 
     */
    public static function generateOrderNumber() {
        $factory = new \RandomLib\Factory;
        $generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::MEDIUM));

        while(true) {
            $orderNumber = $generator->generateString('10', '1234567890ABCDEFGHIJKLMNOPRSTUVWXYZ');
            $orderCount = Order::where('order_number', $orderNumber)->get()->count();

            if ($orderCount === 0) {
                return $orderNumber;
            }
        }
    }

    /**
     * Format currency
     * 
     * @param $amount
     * 
     */
    public static function formatCurrency($amount, $format = null) {
        if ($format === 'IDR') {
            $idrk = round($amount / 1000);
            $idrk = number_format($idrk, 0, ',', '.');
            return "IDR " . $idrk . " K";
        }
        return "Rp" . number_format($amount, 0, ',', '.');
    }

    /**
     * Print array
     * 
     * @param $arr
     */
    public static function printArray($arr) {
        if (is_string($arr)) return $arr;

        $s = '';
        foreach ($arr as $index => $item) {
            if ($index > 0 && $index !== count($arr)) {
                $s .= ', ';
            }
            $s .= $item;
        }
        
        return $s;
    }

    /**
     * Get profile picture utl
     */
    public static function getProfilePicture($user) {
        if ($user->picture_url) {
            return asset($user->picture_url);
        } else {
            return asset('/img/buddy-img/default.jpg');
        }
    }

    /**
     * Format date
     * @param $date MYSQL date
     */
    public static function formatDate($date) {
        $dt = new \Carbon\Carbon($date);
        return $dt->formatLocalized('%e %b %Y %R');
    }
}