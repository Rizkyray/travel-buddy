<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Order;
use PDF;
use Storage;

class OrderReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Order instance.
     * 
     * @var order
     */
     public $order;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->savePDF();

        return $this
        ->from('billing@travelbuddy.id')
        ->subject('Order #' . $this->order->order_number . ' Received')
        ->view('mail.order.received')
        ->attachData($this->generatePDF(), 'invoice-' . $this->order->order_number . '.pdf', [
            'mime' => 'application/pdf',
        ]);
    }

    /**
     * Save PDF and return the path to it
     */
    private function savePDF() {
        $order = $this->order;
        $pdf = PDF::loadView('pdf.order.invoice', ['order' => $order]);
        $filename = 'invoice-' . $this->order->order_number . '.pdf';
        
        Storage::disk('local')->put('order/invoice/'. $filename, $pdf->output());

        return storage_path('app/local/order/invoice/') . $filename;
    }

    /**
     * Generate Invoice as PDF
     */
    public function generatePDF() {
        $order = $this->order;
        $pdf = PDF::loadView('pdf.order.invoice', ['order' => $order]);
        return $pdf->output();
    }
}
