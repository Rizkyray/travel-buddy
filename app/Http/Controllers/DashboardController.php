<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    /**
     * Shows dashboard home.
     */
    public function index(Request $request)
    {
        if (Auth::user()->verified === 0) {
            return view('email-verif.must-verif');
        }

        $user = Auth::user();
        if (!$user->isDataComplete()) {
            $request->session()->flash('info', [
                'title' => 'Halo ' . $user->name,
                'message' => "Selamat datang di Travel Buddy. Anda telah tergabung dalam komunitas traveler Indonesia dan 'Buddies' yang berada di berbagai penjuru dunia. Anda dapat menggunakan Travel Buddy untuk mencari orang Indonesia yang akan menemani selama berjalan-jalan di luar negeri. Silahkan isi form berikut untuk memulai petualangan Anda."
            ]);
        }

        return view('dashboard.index', compact('user'));
    }

    public function buddy(Request $request)
    {
        if (Auth::user()->verified === 0) {
            return view('email-verif.must-verif');
        }

        $user = Auth::user();
        if (!$user->isDataComplete()) {
            $request->session()->flash('flash-warning', [
                'message' => 'Untuk menjadi buddy, kamu harus melengkapi data personal terlebih dahulu'
            ]);
            return redirect(route('dashboard.home'));
        }
        return view('dashboard.buddy', compact('user'));
    }


    /**
     * Shows list of order.
     */
    public function order(Request $request)
    {
        if (Auth::user()->verified === 0) {
            return view('email-verif.must-verif');
        }

        $user = Auth::user();

        if (!$user->buddy) {
            return abort(403, 'Unauthorized Action');
        }
        
        return view('dashboard.order', compact('user'));
    }


    /**
     * Shows settings page.
     */
    public function settings()
    {
        if (Auth::user()->verified === 0) {
            return view('email-verif.must-verif');
        }

        $user = Auth::user();
        return view('dashboard.settings', compact('user'));
    }
}
