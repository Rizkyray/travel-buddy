<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Rules\CurrentPassword;
use Validator;
use Auth;
use Image;

class UserController extends Controller
{
    /**
     * Update user
     */
    public function update(Request $request) {
        $user = Auth::user();
        
        $rules = [
            'name' => 'required',
            'phone' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('dashboard.home'))
                ->withErrors($validator);
        }

        $user->name = $request->input('name');
        $user->phone = $request->input('phone');
    
        if ($request->hasFile('photo')) {
            $this->updatePicture($request);
        }

        if ($user->save()) {
            $request->session()->flash('flash-info', [
                'message' => 'Profil berhasil disimpan.'
            ]);
            return redirect('/dashboard');
        }
    }

    public function updatePicture(Request $request) {
        $rules = [
            'photo' => 'image|mimes:jpeg,png|max:2048'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('dashboard.home'))
                ->withErrors($validator);
        }
        
        $user = Auth::user();

        // Delete old photo
        $this->deleteOldPicture($user);

        $imageFile = $request->file('photo');
        $ext = strtolower($imageFile->getClientOriginalExtension());

        $filename = (string) $user->id . '-' . $user->name . time() . '.' . $ext;
        $filename = str_replace(' ', '-', $filename);
        $path = public_path('img/buddy-img/') . $filename;

        $image = Image::make($imageFile);
        $image->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $user->picture_url = 'img/buddy-img/' . $filename;
        
        try {
            $user->save();
            $image->save($path);            
        } catch(Exception $e) {
            abort(500);
        }
    }

    /**
     * Change password.
     */
    public function changePassword(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'current_password' => ['required', new CurrentPassword($user)],
            'password' => 'required|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect(route('dashboard.settings'))
                ->withErrors($validator);
        }

        $user->password = bcrypt($request->input('password'));
        if ($user->save()) {
            $request->session()->flash('flash-info', [
                'message' => 'Password berhasil diganti.'
            ]);

            return redirect(route('dashboard.home'));
        }
    }

    /**
     * Delete old picture.
     */
    private function deleteOldPicture($user) {
        if ($user->picture_url) {
            if (file_exists(public_path($user->picture_url))) {
                unlink(public_path($user->picture_url));
            }
        }
    }
}
