<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Order;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use DateTime;
use Helper;
use App\Mail\OrderReceivedBuddy;
use Mail;
use Validator;

class OrderController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show all order
     * 
     */
    public function index()
    {
        if (Gate::allows('do-admin-action')) {
            $orders = Order::all();
            return view('admin.order.index', compact('orders'));
        } else {
            echo 'You are not authorized';
        }
    }

    /**
     * View individual item.
     */
    public function view($id)
    {
        if (!Gate::allows('do-admin-action')) abort(403);
        
        $order = Order::findOrFail($id);
        return view('admin.order.view', compact('order'));
    }

    public function orderStatus(Request $request)
    {
        if (!Gate::allows('do-admin-action')) abort(403);

        $order = Order::findOrFail($request->input('order_id'));
        $order->status = $request->input('status');
        $order->payment_status = $request->input('payment_status');

        if ($order->save()) {
            $request->session()->flash('success', 'Status order berhasil diubah');
            return redirect('admin/order/' . $request->input('order_id'));
        }
    }

    /**
     * Show update form
     */
    public function updateView($id)
    {
        if (!Gate::allows('do-admin-action')) abort(403);

        $order = Order::findOrFail($id);
        return view('admin.order.update', compact('order'));
    }

    public function update($id, Request $request)
    {
        if (!Gate::allows('do-admin-action')) abort(403);

        $startDate = DateTime::createFromFormat('D, d/m/Y', $request->input('start_date'));
        $endDate = DateTime::createFromFormat('D, d/m/Y', $request->input('end_date'));

        $daysDifference = 1;

        if ($startDate && $endDate) {
            $daysDifference = Helper::daysDifference($startDate, $endDate);
        }

        $rules = [
            'buddy_id' => 'required|exists:buddies,id',
            'name' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required',
            'start_date' => 'required|date_format:"D, d/m/Y"',
            'end_date' => 'required|date_format:"D, d/m/Y"|after_or_equal:start_date',
            'hours_per_day' => 'required|array|min:1|size:' . $daysDifference,
            'hours_per_day.*' => 'required|Integer|min:1|max:24',
            'trip_details' => 'required|array',
            'trip_details.trip_type' => 'required|array',
            'trip_details.trip_description' => 'required',
            'trip_details.total_adults' => 'required|Integer|min:1|max:9',
            'trip_details.total_children' => 'nullable|Integer|min:0|max:9',
            'trip_details.destination' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect(route('admin.order.update.view', ['id' => $id]))
                ->withErrors($validator)
                ->withInput();
        }
        
        $order = Order::findOrFail($id);

        // Calculate price estimation
        $buddyPrice = $order->buddy->price;
        $hoursPerDay = Helper::arrayToInt($request->input('hours_per_day'));

        $priceEstimation = collect($hoursPerDay)->sum() * (int) $buddyPrice;

        $order->name = $request->input('name');
        $order->email = $request->input('email');
        $order->phone = $request->input('phone');
        $order->start_date = $startDate->format('Y-m-d');
        $order->end_date = $endDate->format('Y-m-d');
        $order->hours_per_day = $hoursPerDay;
        $order->trip_details = $request->input('trip_details');
        $order->message_to_buddy = $request->input('message_to_buddy');
        $order->price_estimation = $priceEstimation;
        $order->price_per_day = $buddyPrice;

        if ($order->save()) {
            $request->session()->flash('success', 'Order berhasil disimpan');
            return redirect(route('admin.order.item', ['id' => $id]));
        }
    }

    /**
     * Process payment
     */
    private function processPayment($orderId, $paymentStatus)
    {
        $order = Order::findOrFail($orderId);
        $order->payment_status = $paymentStatus;

        if ($order->save()) {
            return redirect(route('admin.order'));
        }
    }

    /**
     * Confirm order.
     * Send order notification to buddy via email.
     */
    public function confirm(Request $request)
    {
        if (!Gate::allows('do-admin-action')) abort(403);

        $orderId = $request->input('id');
        $order = Order::findOrFail($orderId);
        $order->status = Order::ORDER_SUBMITTED;

        if ($order->save()) {
            Mail::to($order->email)
                ->send(new OrderReceivedBuddy($order, 'Order Baru #' . $order->order_number));

            $request->session()->flash('success', 'Order berhasil dikonfirmasi ulang. Notifikasi order telah dikirimkan ke email Buddy yang bersangkutan.');
            return redirect(route('admin.order.item', ['id' => $orderId]));
        }
    }
}
