<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Show all order
     * 
     */
    public function index()
    {
        if (Gate::allows('do-admin-action')) {
            return view('admin.index');
        } else {
            echo 'You are not authorized';
        }
    }
}
