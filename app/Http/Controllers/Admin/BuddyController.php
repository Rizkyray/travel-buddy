<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Buddy;
use Illuminate\Support\Facades\Gate;

class BuddyController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show all buddies.
     */
    public function index()
    {
        if (!Gate::allows('do-admin-action')) abort(403);

        $buddies = Buddy::all();
        return view('admin.buddy.index', compact('buddies'));
    }

    /**
     * View individual buddy.
     */
    public function view($id)
    {
        if (!Gate::allows('do-admin-action')) abort(403);

        $buddy = Buddy::findOrFail($id);
        return redirect(route('buddy.profile', ['id' => $id]));
    }

    /**
     * Delete buddy
     */
    public function delete($id)
    {
        if (!Gate::allows('do-admin-action')) abort(403);

        $buddy = Buddy::findOrFail($id);
        $buddy->delete();

        \Session::flash('success', 'Buddy telah dihapus.');
        return redirect(route('admin.buddy'));
    }
}
