<?php

namespace App\Http\Controllers;
use App\Guide;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class GuideController extends Controller
{

    public function index()
    {
      # code...
      return view('upload');
    }

    public function list()
    {
      $city =  Input::get('city');
      $guide = Guide::where('city', $city)->get();
      //count => get
      if(count($guide)<1){
        return response()->json(['message' => 'not found'],404);
      }
      return $guide;
    }

    public function show($id){
      $guide = Guide::find($id);
      if(!$guide){
        return response()->json(['message' => 'not found'],404);
      }
      return $guide;
    }
    public function store(Request $request)
    {

        $user = new Guide;
        $user->name = Input::get('name');
        $user->umur = Input::get('umur');
        $user->city = Input::get('city');
        $user->interest = Input::get('interest');
        $user->bahasa = Input::get('bahasa');
        $user->harga = Input::get('harga');
        if(Input::hasFile('image')){
          $file =Input::file('image');
          $imagedata = file_get_contents($file);
          $file->move(public_path(). '/img', $file->getClientOriginalName());
          $user->path = '/img/'.$file->getClientOriginalName();

        }
          $user->save();
          return "succes";
    }

}
