<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Validator;
use App\Mail\EmailVerification;
use App\Models\User;
use Auth;
use Session;

class RegisterController extends Controller
{   

    /**
     * Verify user given the token.
     * 
     * @param $token
     */
    public function verify($token)
    {
        $user = User::where('email_token', $token)
            ->where('verified', '0')->first();

        if (!$user) {
            abort(404);
        }

        if ($user->verified === 1) {
            abort(404);
        }

        $user->verified = 1;
        $user->email_token = 'verified';

        if ($user->save()) {
            Auth::login($user);
            Session::flash('flash-info', [
                'message' => 'Email Anda berhasil diverifikasi.'
            ]);

            return redirect(route('dashboard.home'));
        }
    }

    /**
     * Resend verification.
     */
    public function resend(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('email-verif.resend');
        }

        if ($request->isMethod('post')) {
            
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|exists:users'
            ]);

            if ($validator->fails()) {
                return redirect('/register/verification/resend')
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $email = $request->input('email');
            $user = User::where('email', $email)->first();

            if ($user->verified === 0) {
                Mail::to($email)->send(new EmailVerification($user));
            } 
            return view('email-verif.resent');
        }
        abort(404);
    }
}