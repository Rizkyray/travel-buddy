<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Helpers\Helper;
use App\Buddy;
use App\Order;
use App\Mail\OrderReceived;
use App\Mail\OrderReceivedBuddy;
use DateTime;
use PDF;
use Auth;
use Mail;
Class BuddyController extends Controller {

    /**
     * Show all buddy
     */
    public function index()
    {
        $buddies = Buddy::all();
        return view('frontend.buddy.search', compact('buddies'));
    }
    
    /**
     * Search buddy by location
     */
    public function search(Request $request)
    {
        $location = $request->input('location');

        if (empty($location)) {
            return redirect(route('buddy.home'));
        }

        // Location is saved to session to automatically select a destination when booking a budy.
        session(['trip_destination' => $location]);
        
        $buddies = Buddy::search($location)->get();
        
        return view('frontend.buddy.search', compact('location', 'buddies'));
    }

    /**
     * Show buddy's profile
     */
    public function profile($id) {
        $buddy = Buddy::findOrFail($id);
        
        return view('frontend.buddy.profile', compact('buddy'));
    }

    /**
     * Show become-a-buddy page
     */
    public function join(Request $request) {
        return view('frontend.buddy.join');
    }

    /**
     * Handle join post
     */
    public function handleJoinPost(Request $request) {
    }

    /**
     * Create a Buddy
     */
    public function create(Request $request)
    {
        $rules = [
            'nickname' => 'required',
            'birthyear' => 'required|integer|min:1920|max:' . date('Y'),
            'country' => 'required',
            'city' => 'required',
            'skills' => 'required',
            'interests' => 'required|array',
            'language' => 'required|array',
            'city_coverage' => 'required',
            'description' => 'required',
            'experience' => 'required',
            'price' => 'required|integer|min:0'
            
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect(route('dashboard.buddy'))
                ->withErrors($validator)
                ->withInput();
        }
        
        $newBuddy = false;
        $user = Auth::user();
        $buddy = $user->buddy;
        if (!$buddy) {
            $buddy = new Buddy();
            $newBuddy = true;
        }

        $buddy->nickname = $request->input('nickname');
        $buddy->birthyear = $request->input('birthyear');
        $buddy->country = $request->input('country');
        $buddy->city = $request->input('city');
        $buddy->city_coverage = $request->input('city_coverage');
        $buddy->interests = $request->input('interests');
        $buddy->language = implode(', ', $request->input('language'));
        $buddy->skills = $request->input('skills');
        $buddy->description = $request->input('description');
        $buddy->experience = $request->input('experience');
        $buddy->price = $request->input('price');

        if ($user->buddy()->save($buddy)) {
            if ($newBuddy) {
                $request->session()->flash('flash-info', [
                    'message' => 'Selamat! Kamu telah berhasil menjadi Buddy. Lihat profil kamu
                                <a href="'. route('buddy.profile', ['id' => $buddy->id]) .'">di sini</a>.'
                ]);
            } else {
                $request->session()->flash('flash-info', [
                    'message' => 'Profile Buddy berhasil disimpan. Lihat profil kamu
                                <a href="'. route('buddy.profile', ['id' => $buddy->id]) .'">di sini</a>.'
                ]);
            }
            return redirect(route('dashboard.buddy'));
        } else {
            abort('500');
        }
    }

    /**
     * Book a buddy
     */
    public function book($id, Request $request)
    {
        if ($request->isMethod('get')) {
            $buddy = Buddy::find($id);
            return view('frontend.buddy.book', compact('buddy'));
        }

        if ($request->isMethod('post')) {
            $startDate = DateTime::createFromFormat('D, d/m/Y', $request->input('start_date'));
            $endDate = DateTime::createFromFormat('D, d/m/Y', $request->input('end_date'));

            $daysDifference = 1;

            if ($startDate && $endDate) {
                $daysDifference = Helper::daysDifference($startDate, $endDate);
            }
            
            $rules = [
                'id' => 'required|exists:buddies',
                'name' => 'required|max:255',
                'email' => 'required|email',
                'phone' => 'required',
                'start_date' => 'required|date_format:"D, d/m/Y"',
                'end_date' => 'required|date_format:"D, d/m/Y"|after_or_equal:start_date',
                'hours_per_day' => 'required|array|min:1|size:' . $daysDifference,
                'hours_per_day.*' => 'required|Integer|min:1|max:24',
                'trip_details' => 'required|array',
                'trip_details.trip_type' => 'required|array',
                'trip_details.trip_description' => 'required',
                'trip_details.total_adults' => 'required|Integer|min:1|max:9',
                'trip_details.total_children' => 'nullable|Integer|min:0|max:9',
                'trip_details.destination' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect(route('buddy.book', ['id' => $id]))
                    ->withErrors($validator)
                    ->withInput();
            }


            // Calculate price estimation
            $buddyPrice = Buddy::find($request->input('id'))->price;
            $hoursPerDay = Helper::arrayToInt($request->input('hours_per_day'));

            $priceEstimation = collect($hoursPerDay)->sum() * (int) $buddyPrice;

            $order = new Order();
            $order->buddy_id = $request->input('id');
            $order->order_number = $this->generateOrderNumber();
            $order->name = $request->input('name');
            $order->email = $request->input('email');
            $order->phone = $request->input('phone');
            $order->start_date = $startDate->format('Y-m-d');
            $order->end_date = $endDate->format('Y-m-d');
            $order->hours_per_day = $hoursPerDay;
            $order->trip_details = $request->input('trip_details');
            $order->message_to_buddy = $request->input('message_to_buddy');
            $order->price_estimation = $priceEstimation;
            $order->price_per_day = $buddyPrice;
            $order->status = Order::ORDER_SUBMITTED;
            $order->payment_status = Order::PAYMENT_UNPAID;

            if ($order->save()) {
                // Send order notification to buddy.
                Mail::to($order->buddy->user->email)->send(new OrderReceivedBuddy($order));
                return redirect(route('buddy.book.success'));
            }
        }
    }

    /**
     * Accept order
     */
    public function acceptOrder(Request $request)
    {
        $order_id = $request->input('order_id');
        if (!$order_id) abort(403, 'Unauthorized Action');

        return $this->processOrder($order_id, Order::ORDER_ACCEPTED);
    }

    /**
     * Declined order
     */
    public function declineOrder(Request $request)
    {
        $order_id = $request->input('order_id');
        if (!$order_id) abort(403, 'Unauthorized Action');

        return $this->processOrder($order_id, Order::ORDER_DECLINED);
    }

    /**
     * Process an order
     */
    private function processOrder($order_id, $status)
    {
        $user = Auth::user();
        $order = $user->buddy->orders()->findOrFail($order_id);

        $order->status = $status;
        if ($order->save()) {
            switch ($status) {
                case Order::ORDER_ACCEPTED:
                    // Send invoice to traveler.
                    Mail::to($order->email)->send(new OrderReceived($order));
                    Session::flash('flash-info', [
                        'message' => 'Order berhasil disetuji.'
                    ]);
                    break;
                case Order::ORDER_DECLINED:
                    Session::flash('flash-info', [
                        'message' => 'Order berhasil ditolak.'
                    ]);
                    break;
                default:
                    abort('403', 'Unathorized action');
                    break;
            }
            return redirect(route('dashboard.order'));
        }
    }

    /**
     * Generate unique order number
     * 
     * @return string 
     */
    private function generateOrderNumber()
    {
        $factory = new \RandomLib\Factory;
        $generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::MEDIUM));

        while(true) {
            $orderNumber = $generator->generateString('10', '1234567890ABCDEFGHIJKLMNOPRSTUVWXYZ');
            $orderCount = Order::where('order_number', $orderNumber)->get()->count();

            if ($orderCount === 0) {
                return $orderNumber;
            }
        }
    }
}