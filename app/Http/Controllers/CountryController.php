<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Country;
use App\CountryPrice;

class CountryController extends Controller
{
    public function index(Request $request) {
        $countryKeyword = $request->input('country');

        if (!$countryKeyword) {
            return response()->json(['country' => []], 200, [], JSON_NUMERIC_CHECK);
        }

        $country = Country::where('country', 'like', $countryKeyword . '%')->take(5)->get();

        if ($country->count() === 0) {
            $country = Country::where('country', 'like', '%' . $countryKeyword . '%')->take(5)->get();
        }


        return response()->json(['country' => $country], 200, [], JSON_NUMERIC_CHECK);

    }

    public function city(Request $request) {
        $countryKeyword = $request->input('country');
        $cityKeyword = $request->input('city');


        if (!$cityKeyword || !$countryKeyword) {
            return response()->json(['city' => []], 200, [], JSON_NUMERIC_CHECK);
        }

        $country = Country::where('country', $countryKeyword)->first();

        if (!$country) {
            return response()->json(['city' => []], 200, [], JSON_NUMERIC_CHECK);
        }
        
        $city = $country->cities()->where('city', 'like', $cityKeyword . '%')->take(5)->get();
        
        if ($city->count() === 0) {
            $city = $country->cities()->where('city', 'like', '%' . $cityKeyword . '%')->take(5)->get();
        }

        return response()->json(['city' => $city], 200, [], JSON_NUMERIC_CHECK);
    }

    public function price(Request $request)
    {
        $country = $request->input('country');

        if (!$country) {
            return response()->json([], 200, [], JSON_NUMERIC_CHECK);
        }

        $price = CountryPrice::where('country_name', $country)->get();
        return response()->json($price, 200, [], JSON_NUMERIC_CHECK);
    }
}
