<?php

namespace App\Http\Middleware;

class SetIndonesianLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        // call your function
        $this->setLocale();
        return $next($request);
    }

    public function setLocale()
    {
        setlocale(LC_TIME, 'id_ID.UTF-8');
    }
}
