<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    const ORDER_SUBMITTED = 'ORDER_SUBMITTED';
    const ORDER_ACCEPTED = 'ORDER_ACCEPTED';
    const ORDER_DECLINED = 'ORDER_DECLINED';
    
    const PAYMENT_UNPAID = 'PAYMENT_UNPAID';
    const PAYMENT_PAID = 'PAYMENT_PAID';

    const ORDER_STATUS = [
        'ORDER_SUBMITTED',
        'ORDER_ACCEPTED',
        'ORDER_DECLINED'
    ];

    const PAYMENT_STATUS = [
        'PAYMENT_UNPAID',
        'PAYMENT_PAID'
    ];
    
    protected $casts = [
        'hours_per_day' => 'array',
        'trip_details' => 'array'
    ];

    public function buddy()
    {
        return $this->belongsTo('App\Buddy');
    }
}
