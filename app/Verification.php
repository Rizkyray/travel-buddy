<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;


class verification extends Model
{
    use Searchable;
    use SoftDeletes;
    use CascadeSoftDeletes;
    
    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'buddies_index';
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Enable soft delete cascade
     * 
     * @var array
     */
    protected $cascadeDeletes = ['orders'];

    /**
     * The table associated with the model
    */
    protected $table = 'buddies';

    protected $casts = [
        'city_coverage' => 'array',
        'interests' => 'array',
    ];

    /**
     * User that belongs to the buddy
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Order that belongs to the buddy
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function age()
    {
        return (int) date('Y') - (int) $this->birthyear;
    }

    /**
     * Check if buddy has order given the order id.
     * @param $orderId
     * 
     * @return true if buddy has that order.
     */
    public function hasOrder($orderId)
    {
        $order = $this->orders()->find($orderId);

        if ($order) {
            return true;
        } else {
            return false;
        }
    }
  
}
