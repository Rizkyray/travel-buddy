<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryPrice extends Model
{
    protected $table = 'country_price';
}
