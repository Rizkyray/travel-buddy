<?php

namespace App\Models;

use App\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Mail\ResetPassword;

class User extends Authenticatable
{
    use Notifiable;
    use CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Buddy
     */
    public function buddy()
    {
        return $this->hasOne('App\Buddy');
    }

    /**
     * The roles that belong to the user
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * Checks if the user has a role by its name.
     * 
     * @param string|array $name            Role name or array of role names.
     * @param bool         $requireAll      All roles in the array are required.
     */
    public function hasRole($name, $requiredAll = false) {
        if (is_array($name)) {
            foreach ($name as $roleName) {
                $hasRole = $this->hasRole($roleName);

                if ($hasRole && !$requiredAll) {
                    return true;
                } else if (!$hasRole && $requiredAll) {
                    return false;
                }
            }

            return $requireAll;
        } else {
            foreach ($this->roles()->get() as $role) {
                if ($role->name === $name) {
                    return true;
                }
            }
        }

        return false;
    }

    public function isDataComplete()
    {
        return $this->name && $this->email && $this->phone;
    }

    /**
     * Sends the password reset notification.
     *
     * @param  string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        \Session::flash('flash-info', [
            'message' => 'Tautan untuk mereset password telah dikirimkan ke email Anda.'
        ]);
        \Mail::to(request()->email)->send(new ResetPassword($token));
    }
}
