-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2018 at 11:13 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travelbuddy`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country`) VALUES
(1, 'Canada'),
(2, 'Cambodia'),
(3, 'Aruba'),
(4, 'Swaziland'),
(5, 'Palestine'),
(6, 'Argentina'),
(7, 'Bolivia'),
(8, 'Cameroon'),
(9, 'Bahrain'),
(10, 'Saudi Arabia'),
(11, 'Slovenia'),
(12, 'Guatemala'),
(13, 'Bosnia and Herzegovina'),
(14, 'Kuwait'),
(15, 'Spain'),
(16, 'Netherlands'),
(17, 'Pakistan'),
(18, 'Oman'),
(19, 'Tanzania'),
(20, 'Albania'),
(21, 'Gabon'),
(22, 'New Zealand'),
(23, 'Jamaica'),
(24, 'Greenland'),
(25, 'United Arab Emirates'),
(26, 'Guam'),
(27, 'Uruguay'),
(28, 'India'),
(29, 'Azerbaijan'),
(30, 'Kenya'),
(31, 'Turkey'),
(32, 'Afghanistan'),
(33, 'Bangladesh'),
(34, 'Andorra'),
(35, 'Saint Lucia'),
(36, 'San Marino'),
(37, 'French Polynesia'),
(38, 'France'),
(39, 'Slovakia'),
(40, 'Peru'),
(41, 'Norway'),
(42, 'Cuba'),
(43, 'Montenegro'),
(44, 'China'),
(45, 'Armenia'),
(46, 'Republic of Korea'),
(47, 'Dominican Republic'),
(48, 'Ukraine'),
(49, 'Ghana'),
(50, 'Finland'),
(51, 'Libya'),
(52, 'Indonesia'),
(53, 'Mauritius'),
(54, 'Liechtenstein'),
(55, 'Belarus'),
(56, 'Russia'),
(57, 'Bulgaria'),
(58, 'United States'),
(59, 'Romania'),
(60, 'Angola'),
(61, 'Cayman Islands'),
(62, 'South Africa'),
(63, 'Nicaragua'),
(64, 'Sweden'),
(65, 'Malaysia'),
(66, 'Austria'),
(67, 'Vietnam'),
(68, 'Mozambique'),
(69, 'Japan'),
(70, 'Isle of Man'),
(71, 'Brazil'),
(72, 'Faroe Islands'),
(73, 'Guinea'),
(74, 'Panama'),
(75, 'Republic of Moldova'),
(76, 'Costa Rica'),
(77, 'Luxembourg'),
(78, 'Bahamas'),
(79, 'Ireland'),
(80, 'Nigeria'),
(81, 'Ecuador'),
(82, 'Czech Republic'),
(83, 'Brunei'),
(84, 'Australia'),
(85, 'Iran'),
(86, 'Algeria'),
(87, 'Republic of Lithuania'),
(88, 'El Salvador'),
(89, 'Chile'),
(90, 'Puerto Rico'),
(91, 'Belgium'),
(92, 'Thailand'),
(93, 'Haiti'),
(94, 'Iraq'),
(95, 'Hong Kong'),
(96, 'Georgia'),
(97, 'Denmark'),
(98, 'Philippines'),
(99, 'Morocco'),
(100, 'Croatia'),
(101, 'Mongolia'),
(102, 'Switzerland'),
(103, 'Myanmar [Burma]'),
(104, 'U.S. Virgin Islands'),
(105, 'Belize'),
(106, 'Portugal'),
(107, 'Estonia'),
(108, 'Kosovo'),
(109, 'Lebanon'),
(110, 'Tunisia'),
(111, 'Antigua and Barbuda'),
(112, 'Colombia'),
(113, 'Taiwan'),
(114, 'Cyprus'),
(115, 'Barbados'),
(116, 'Madagascar'),
(117, 'Italy'),
(118, 'Sudan'),
(119, 'Nepal'),
(120, 'Malta'),
(121, 'Suriname'),
(122, 'Venezuela'),
(123, 'Israel'),
(124, 'Iceland'),
(125, 'Zambia'),
(126, 'Senegal'),
(127, 'Papua New Guinea'),
(128, 'Trinidad and Tobago'),
(129, 'Zimbabwe'),
(130, 'Germany'),
(131, 'Martinique'),
(132, 'Kazakhstan'),
(133, 'Poland'),
(134, 'Mayotte'),
(135, 'New Caledonia'),
(136, 'Macedonia'),
(137, 'Sri Lanka'),
(138, 'Latvia'),
(139, 'Hungary'),
(140, 'Hashemite Kingdom of Jordan'),
(141, 'Guadeloupe'),
(142, 'Honduras'),
(143, 'Mexico'),
(144, 'Egypt'),
(145, 'Singapore'),
(146, 'Serbia'),
(147, 'United Kingdom'),
(148, 'Congo'),
(149, 'Greece'),
(150, 'Paraguay'),
(151, 'Namibia'),
(152, 'Botswana');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
