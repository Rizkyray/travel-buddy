<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class DummyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Husni Munaya';
        $user->password = bcrypt('password');
        $user->email = 'husnimun@gmail.com';
        $user->verified = 1;
        $user->email_token = 'verified';
        $user->save();

        $user = new User();
        $user->name = 'Nurizka';
        $user->password = bcrypt('password');
        $user->email = 'nunu@meridian.com';
        $user->verified = 1;
        $user->email_token = 'verified';
        $user->save();

        $user = new User();
        $user->name = 'Kurni';
        $user->password = bcrypt('password');
        $user->email = 'adhi.kurni@gmail.com';
        $user->verified = 1;
        $user->email_token = 'verified';
        $user->save();

        $user = new User();
        $user->name = 'Sena';
        $user->password = bcrypt('password');
        $user->email = 'm.sena.luphdika@gmail.com';
        $user->verified = 1;
        $user->email_token = 'verified';
        $user->save();
    }
}
