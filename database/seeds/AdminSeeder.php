<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = new Role();
        $adminRole->name = 'admin';
        $adminRole->display_name = 'Travel Buddy Admin';
        $adminRole->description = 'Travel Buddy Admin';
        
        $admin = new User();
        $admin->name = 'Travel Buddy Admin';
        $admin->password = bcrypt('travelbuddyadmin');
        $admin->email = 'admin@travelbuddy.id';
        $admin->verified = 1;
        $admin->email_token = 'verified';
        
        if ($adminRole->save() && $admin->save())
        {
            $admin->roles()->attach($adminRole->id);
        }
    }
}
