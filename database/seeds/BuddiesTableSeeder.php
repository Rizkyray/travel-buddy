<?php

use Illuminate\Database\Seeder;

class BuddiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'          => 'Thomas',    
            'email'         => 'thomas.litaay@gmail.com',
            'phone'         => '31642582605',
            'password'      => bcrypt('thomas'),
            'verified'      => 1,
            'email_token'   => 'verified',
            'picture_url'   =>  'img/buddy-img/Thomas2.jpg',
        ]);

        DB::table('buddies')->insert([
            'user_id'       => 2,
            'nickname'      => 'Thomas',
            'birthyear'     => 1995,
            'city'          => 'The Hague',
            'city_coverage' => '["The Hague"]',
            'country'       => 'Netherlands',
            'interests'     => 'Food and Drinks, Sightseeing',
            'skills'        => 'Skill 1, Skill2, Skill 3',
            'language'      => 'Indonesian, English',
            'description'   => 'Seorang yang suka berdiskusi dari hal-hal yang umum hingga hal-hal prinsipil.   Terbuka terhadap semua pemikiran (yang masuk akal tentunya).',
            'experience'    => 'Experience',
            'price'         => '195000',            
        ]);
        
        DB::table('users')->insert([
            'name'          => 'Griyana ',
            'email'         => 'gpanelewen@gmail.com',
            'phone'         => '+4917661987087',
            'password'      => bcrypt('griyana'),
            'verified'      => 1,
            'email_token'   => 'verified',
            'picture_url'   =>  'img/buddy-img/Griyana2.jpg',
        ]);

        DB::table('buddies')->insert([
            'user_id'       => 3,
            'nickname'      => 'Griyana ',
            'birthyear'     => 1993,
            'city'          => 'Frankfurt am Main',
            'city_coverage' => '["Frankfurt am Main"]',
            'country'       => 'Germany',
            'interests'     => 'Party, Food and Drinks',
            'skills'        => 'Skill 1, Skill2, Skill 3',
            'language'      => 'German, English, Indonesian ',
            'description'   => 'I’m a social and outgoing person who currently studying in Germany. I’m a learner and always want to be improving or learning more about things especially my cookinf skills. My interests are travelling, music and food:))',
            'experience'    => 'Experience',
            'price'         => '195000',            
        ]);

        DB::table('users')->insert([
            'name'          => 'Indri ',
            'email'         => 'Ikurniaty@gmail.com',
            'phone'         => '+1 310 592 4010',
            'password'      => bcrypt('indri'),
            'verified'      => 1,
            'email_token'   => 'verified',
            'picture_url'   =>  'img/buddy-img/Indri2.jpg',
        ]);

        DB::table('buddies')->insert([
            'user_id'       => 4,
            'nickname'      => 'Indri ',
            'birthyear'     => 1983,
            'city'          => 'Seattle',
            'city_coverage' => '["Seattle", "West Coast"]',
            'country'       => 'United States',
            'interests'     => 'Nature, culture, culinary',
            'skills'        => 'Skill 1, Skill2, Skill 3',
            'language'      => 'Indonesia, English',
            'description'   => 'Cheerful, easy going, traveling',
            'experience'    => 'Experience',
            'price'         => '195000',            
        ]);

        DB::table('users')->insert([
            'name'          => 'Ado',
            'email'         => 'ado.amando@gmail.com',
            'phone'         => '+4740317048',
            'password'      => bcrypt('ado'),
            'verified'      => 1,
            'email_token'   => 'verified',
            'picture_url'   =>  'img/buddy-img/Ado2.jpg',
        ]);

        DB::table('buddies')->insert([
            'user_id'       => 5,
            'nickname'      => 'Ado',
            'birthyear'     => 1987,
            'city'          => 'Tromso',
            'city_coverage' => '["Tromso"]',
            'country'       => 'Norwegia',
            'interests'     => 'Nature, City Tour',
            'skills'        => 'Skill 1, Skill2, Skill 3',
            'language'      => 'Indonesia, english',
            'description'   => 'Enjoy talking, good humor, like to make new friends',
            'experience'    => 'Experience',
            'price'         => '195000',            
        ]);

        DB::table('users')->insert([
            'name'          => 'Chris',
            'email'         => 'crrudolph26@gmail.com',
            'phone'         => '+31657147041',
            'password'      => bcrypt('chris'),
            'verified'      => 1,
            'email_token'   => 'verified',
            'picture_url'   =>  'img/buddy-img/Chris2.jpg',
        ]);

        DB::table('buddies')->insert([
            'user_id'       => 6,
            'nickname'      => 'Chris',
            'birthyear'     => 1995,
            'city'          => 'The Hague',
            'city_coverage' => '["The Hague", "Amsterdam"]',
            'country'       => 'Netherlands',
            'interests'     => 'Travelling, Music, Football, Human Resource Studies. ',
            'skills'        => 'Skill 1, Skill2, Skill 3',
            'language'      => 'Indonesia, English, Dutch',
            'description'   => 'I am an easy-going person who like to explore new places. Currently looking for a full-time job that could sponsor me to live in the Netherlands. I am good with organizing and engaging with peopble.',
            'experience'    => 'Experience',
            'price'         => '195000',            
        ]);

        DB::table('users')->insert([
            'name'          => 'Dzulkifli',
            'email'         => 'dzulkifli.klm@gmail.com',
            'phone'         => '1739846909',
            'password'      => bcrypt('dzulkifli'),
            'verified'      => 1,
            'email_token'   => 'verified',
            'picture_url'   =>  'img/buddy-img/Dzulkifli2.jpg',
        ]);

        DB::table('buddies')->insert([
            'user_id'       => 7,
            'nickname'      => 'Dzulkifli',
            'birthyear'     => 1995,
            'city'          => 'Karlsruhe',
            'city_coverage' => '["Karlsruhe"]',
            'country'       => 'Germany',
            'interests'     => 'Nature, Culture, Cityscape',
            'skills'        => 'Skill 1, Skill2, Skill 3',
            'language'      => 'Deutsch, English, Bahasa',
            'description'   => 'Currently studying chemical engineering in KIT. I love photography and willing to travel',
            'experience'    => 'Experience',
            'price'         => '195000',            
        ]);

        DB::table('users')->insert([
            'name'          => 'Chelcian',
            'email'         => 'Prasyudhastyka@gmail.com',
            'phone'         => '650232100',
            'password'      => bcrypt('chelcian'),
            'verified'      => 1,
            'email_token'   => 'verified',
            'picture_url'   =>  'img/buddy-img/Chelcian2.jpg',
        ]);

        DB::table('buddies')->insert([
            'user_id'       => 8,
            'nickname'      => 'Chelcian',
            'birthyear'     => 1993,
            'city'          => 'The Hague',
            'city_coverage' => '["The Hague"]',
            'country'       => 'Netherlands',
            'interests'     => 'Shopping & Food',
            'skills'        => 'Skill 1, Skill2, Skill 3',
            'language'      => 'Indonesia & Belanda',
            'description'   => 'Bisa bahasa belanda',
            'experience'    => 'Experience',
            'price'         => '195000',            
        ]);

        DB::table('users')->insert([
            'name'          => 'Mushlih Muharrik',
            'email'         => 'mushliharrik@gmail.com',
            'phone'         => '+8107042947746',
            'password'      => bcrypt('mushlih'),
            'verified'      => 1,
            'email_token'   => 'verified',
            'picture_url'   =>  'img/buddy-img/Mushliharrik.jpeg',
        ]);

        DB::table('buddies')->insert([
            'user_id'       => 9,
            'nickname'      => 'Mushlih Muharrik',
            'birthyear'     => 1993,
            'city'          => 'Tokyo',
            'city_coverage' => '["Tokyo"]',
            'country'       => 'Japan',
            'interests'     => 'Nature, Food, Shopping',
            'skills'        => 'Skill 1, Skill2, Skill 3',
            'language'      => 'Indonesia, English',
            'description'   => 'I am a second year master student at TokyoTech.',
            'experience'    => 'Experience',
            'price'         => '195000',            
        ]);

        DB::table('users')->insert([
            'name'          => 'Hamzah',
            'email'         => 'rilyadihamzah@gmail.com',
            'phone'         => '1123703804',
            'password'      => bcrypt('hamzah'),
            'verified'      => 1,
            'email_token'   => 'verified',
            'picture_url'   =>  'img/buddy-img/Hamzah.jpeg',
        ]);

        DB::table('buddies')->insert([
            'user_id'       => 10,
            'nickname'      => 'Hamzah',
            'birthyear'     => 1995,
            'city'          => 'Kuala Lumpur',
            'city_coverage' => '["Kuala Lumpur"]',
            'country'       => 'Malaysia',
            'interests'     => 'Food & drinks, Shopping, Party, Nature',
            'skills'        => 'Skill 1, Skill2, Skill 3',
            'language'      => 'Indonesia, English',
            'description'   => 'Im good at sports and very good at eating. I like to travel around to see places and like to try something new.',
            'experience'    => 'Experience',
            'price'         => '195000',            
        ]);
    }
}
