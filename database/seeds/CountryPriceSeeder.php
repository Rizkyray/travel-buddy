<?php

use Illuminate\Database\Seeder;

class CountryPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
                'country_name' => 'Afghanistan',
                'price' => 120000
            ],
            [
                'country_name' => 'Albania',
                'price' => 120000
            ],
            [
                'country_name' => 'Algeria',
                'price' => 12000
            ],
            [
                'country_name' => 'Andorra',
                'price' => 175000
            ],
            [
                'country_name' => 'Angola',
                'price' => 175000
            ],
            [
                'country_name' => 'Antigua and Barbuda',
                'price' => 120000
            ],
            [
                'country_name' => 'Argentina',
                'price' => 150000
            ],
            [
                'country_name' => 'Armenia',
                'price' => 120000
            ],
            [
                'country_name' => 'Aruba',
                'price' => 195000
            ],
            [
                'country_name' => 'Australia',
                'price' => 195000
            ],
            [
                'country_name' => 'Austria',
                'price' => 175000
            ],
            [
                'country_name' => 'Azerbaijan',
                'price' => 120000
            ],
            [
                'country_name' => 'Bahamas',
                'price' => 195000
            ],
            [
                'country_name' => 'Bahrain',
                'price' => 150000
            ],
            [
                'country_name' => 'Bangladesh',
                'price' => 120000
            ],
            [
                'country_name' => 'Barbados',
                'price' => 150000
            ],
            [
                'country_name' => 'Belarus',
                'price' => 120000
            ],
            [
                'country_name' => 'Belgium',
                'price' => 175000
            ],
            [
                'country_name' => 'Belize',
                'price' => 150000
            ],
            [
                'country_name' => 'Bolivia',
                'price' => 120000
            ],
            [
                'country_name' => 'Bosnia and Herzegovina',
                'price' => 120000
            ],
            [
                'country_name' => 'Botswana',
                'price' => 120000
            ],
            [
                'country_name' => 'Brazil',
                'price' => 150000
            ],
            [
                'country_name' => 'Brunei',
                'price' => 175000
            ],
            [
                'country_name' => 'Bulgaria',
                'price' => 120000
            ],
            [
                'country_name' => 'Cambodia',
                'price' => 12000
            ],
            [
                'country_name' => 'Cameroon',
                'price' => 120000
            ],
            [
                'country_name' => 'Canada',
                'price' => 150000
            ],
            [
                'country_name' => 'Cayman Islands',
                'price' => 195000
            ],
            [
                'country_name' => 'Chile',
                'price' => 150000
            ],
            [
                'country_name' => 'China',
                'price' => 150000
            ],
            [
                'country_name' => 'Colombia',
                'price' => 120000
            ],
            [
                'country_name' => 'Congo',
                'price' => 120000
            ],
            [
                'country_name' => 'Costa Rica',
                'price' => 150000
            ],
            [
                'country_name' => 'Croatia',
                'price' => 150000
            ],
            [
                'country_name' => 'Cuba',
                'price' => 120000
            ],
            [
                'country_name' => 'Cyprus',
                'price' => 150000
            ],
            [
                'country_name' => 'Czech Republic',
                'price' => 150000
            ],
            [
                'country_name' => 'Denmark',
                'price' => 195000
            ],
            [
                'country_name' => 'Dominican Republic',
                'price' => 120000
            ],
            [
                'country_name' => 'Ecuador',
                'price' => 120000
            ],
            [
                'country_name' => 'Egypt',
                'price' => 120000
            ],
            [
                'country_name' => 'El Salvador',
                'price' => 120000
            ],
            [
                'country_name' => 'Estonia',
                'price' => 150000
            ],
            [
                'country_name' => 'Faroe Islands',
                'price' => 175000
            ],
            [
                'country_name' => 'Finland',
                'price' => 195000
            ],
            [
                'country_name' => 'France',
                'price' => 175000
            ],
            [
                'country_name' => 'French Polynesia',
                'price' => 150000
            ],
            [
                'country_name' => 'Gabon',
                'price' => 150000 
            ],
            [
                'country_name' => 'Georgia',
                'price' => 120000
            ],
            [
                'country_name' => 'Germany',
                'price' => 195000
            ],
            [
                'country_name' => 'Ghana',
                'price' => 120000
            ],
            [
                'country_name' => 'Greece',
                'price' => 150000
            ],
            [
                'country_name' => 'Greenland',
                'price' => 175000
            ],
            [
                'country_name' => 'Guadeloupe',
                'price' => 120000
            ],
            [
                'country_name' => 'Guam',
                'price' => 175000
            ],
            [
                'country_name' => 'Guatemala',
                'price' => 120000
            ],
            [
                'country_name' => 'Guinea',
                'price' => 120000
            ],
            [
                'country_name' => 'Haiti',
                'price' => 120000
            ],
            [
                'country_name' => 'Hashemite Kingdom of Jordan',
                'price' => 120000
            ],
            [
                'country_name' => 'Honduras',
                'price' => 120000
            ],
            [
                'country_name' => 'Hong Kong',
                'price' => 175000
            ],
            [
                'country_name' => 'Hungary',
                'price' => 120000
            ],
            [
                'country_name' => 'Iceland',
                'price' => 175000
            ],
            [
                'country_name' => 'India',
                'price' => 120000
            ],
            [
                'country_name' => 'Indonesia',
                'price' => 120000
            ],
            [
                'country_name' => 'Iran',
                'price' => 120000
            ],
            [
                'country_name' => 'Iraq',
                'price' => 120000
            ],
            [
                'country_name' => 'Ireland',
                'price' => 195000
            ],
            [
                'country_name' => 'Isle of Man',
                'price' => 150000
            ],
            [
                'country_name' => 'Israel',
                'price' => 150000
            ],
            [
                'country_name' => 'Italy',
                'price' => 175000
            ],
            [
                'country_name' => 'Jamaica',
                'price' => 150000
            ],
            [
                'country_name' => 'Japan',
                'price' => 175000
            ],
            [
                'country_name' => 'Kazakhstan',
                'price' => 120000
            ],
            [
                'country_name' => 'Kenya',
                'price' => 120000
            ],
            [
                'country_name' => 'Kosovo',
                'price' => 120000
            ],
            [
                'country_name' => 'Kuwait',
                'price' => 175000
            ],
            [
                'country_name' => 'Latvia',
                'price' => 120000
            ],
            [
                'country_name' => 'Lebanon',
                'price' => 150000
            ],
            [
                'country_name' => 'Libya',
                'price' => 120000
            ],
            [
                'country_name' => 'Liechtenstein',
                'price' => 120000
            ],
            [
                'country_name' => 'Luxembourg',
                'price' => 225000
            ],
            [
                'country_name' => 'Macedonia',
                'price' => 120000
            ],
            [
                'country_name' => 'Madagascar',
                'price' => 120000
            ],
            [
                'country_name' => 'Malaysia',
                'price' => 150000
            ],
            [
                'country_name' => 'Malta',
                'price' => 150000
            ],
            [
                'country_name' => 'Martinique',
                'price' => 120000
            ],
            [
                'country_name' => 'Mauritius',
                'price' => 150000
            ],
            [
                'country_name' => 'Mayotte',
                'price' => 120000
            ],
            [
                'country_name' => 'Mexico',
                'price' => 150000
            ],
            [
                'country_name' => 'Mongolia',
                'price' => 120000
            ],
            [
                'country_name' => 'Montenegro',
                'price' => 120000
            ],
            [
                'country_name' => 'Morocco',
                'price' => 120000
            ],
            [
                'country_name' => 'Mozambique',
                'price' => 150000
            ],
            [
                'country_name' => 'Myanmar [Burma]',
                'price' => 120000
            ],
            [
                'country_name' => 'Namibia',
                'price' => 120000
            ],
            [
                'country_name' => 'Nepal',
                'price' => 120000
            ],
            [
                'country_name' => 'Netherlands',
                'price' => 195000
            ],
            [
                'country_name' => 'New Caledonia',
                'price' => 175000
            ],
            [
                'country_name' => 'New Zealand',
                'price' => 175000
            ],
            [
                'country_name' => 'Nicaragua',
                'price' => 120000
            ],
            [
                'country_name' => 'Nigeria',
                'price' => 150000
            ],
            [
                'country_name' => 'Norway',
                'price' => 195000
            ],
            [
                'country_name' => 'Oman',
                'price' => 150000
            ],
            [
                'country_name' => 'Pakistan',
                'price' => 120000
            ],
            [
                'country_name' => 'Palestine',
                'price' => 150000
            ],
            [
                'country_name' => 'Panama',
                'price' => 150000
            ],
            [
                'country_name' => 'Papua New Guinea',
                'price' => 120000
            ],
            [
                'country_name' => 'Paraguay',
                'price' => 120000
            ],
            [
                'country_name' => 'Peru',
                'price' => 120000
            ],
            [
                'country_name' => 'Philippines',
                'price' => 120000
            ],
            [
                'country_name' => 'Poland',
                'price' => 150000
            ],
            [
                'country_name' => 'Portugal',
                'price' => 150000
            ],
            [
                'country_name' => 'Puerto Rico',
                'price' => 150000
            ],
            [
                'country_name' => 'Republic of Korea',
                'price' => 175000
            ],
            [
                'country_name' => 'Republic of Lithuania',
                'price' => 120000
            ],
            [
                'country_name' => 'Republic of Moldova',
                'price' => 120000
            ],
            [
                'country_name' => 'Romania',
                'price' => 120000
            ],
            [
                'country_name' => 'Russia',
                'price' => 120000
            ],
            [
                'country_name' => 'Saint Lucia',
                'price' => 120000
            ],
            [
                'country_name' => 'San Marino',
                'price' => 150000
            ],
            [
                'country_name' => 'Saudi Arabia',
                'price' => 150000
            ],
            [
                'country_name' => 'Senegal',
                'price' => 120000
            ],
            [
                'country_name' => 'Serbia',
                'price' => 120000
            ],
            [
                'country_name' => 'Singapore',
                'price' => 175000
            ],
            [
                'country_name' => 'Slovakia',
                'price' => 150000
            ],
            [
                'country_name' => 'Slovenia',
                'price' => 150000
            ],
            [
                'country_name' => 'South Africa',
                'price' => 150000
            ],
            [
                'country_name' => 'Spain',
                'price' => 150000
            ],
            [
                'country_name' => 'Sri Lanka',
                'price' => 120000
            ],
            [
                'country_name' => 'Sudan',
                'price' => 120000
            ],
            [
                'country_name' => 'Suriname',
                'price' => 150000
            ],
            [
                'country_name' => 'Swaziland',
                'price' => 150000
            ],
            [
                'country_name' => 'Sweden',
                'price' => 195000
            ],
            [
                'country_name' => 'Switzerland',
                'price' => 225000
            ],
            [
                'country_name' => 'Taiwan',
                'price' => 150000
            ],
            [
                'country_name' => 'Tanzania',
                'price' => 120000
            ],
            [
                'country_name' => 'Thailand',
                'price' => 120000
            ],
            [
                'country_name' => 'Trinidad and Tobago',
                'price' => 15000
            ],
            [
                'country_name' => 'Tunisia',
                'price' => 120000
            ],
            [
                'country_name' => 'Turkey',
                'price' => 150000
            ],
            [
                'country_name' => 'U.S. Virgin Islands',
                'price' => 195000
            ],
            [
                'country_name' => 'Ukraine',
                'price' => 120000
            ],
            [
                'country_name' => 'United Arab Emirates',
                'price' => 195000
            ],
            [
                'country_name' => 'United Kingdom',
                'price' => 195000
            ],
            [
                'country_name' => 'United States',
                'price' => 195000
            ],
            [
                'country_name' => 'Uruguay',
                'price' => 150000
            ],
            [
                'country_name' => 'Venezuela',
                'price' => 120000
            ],
            [
                'country_name' => 'Vietnam',
                'price' => 120000
            ],
            [
                'country_name' => 'Zambia',
                'price' => 225000
            ],
            [
                'country_name' => 'Zimbabwe',
                'price' => 120000
            ],
        ];

        foreach ($countries as $country) {
            DB::table('country_price')->insert($country);
        }
    }
}
