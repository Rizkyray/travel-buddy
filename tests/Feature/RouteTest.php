<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RouteTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRoute()
    {
        $response = $this->get('/');
        $response->assertStatus(200);

        $response = $this->get('/buddy');
        $response->assertStatus(200);

        $response = $this->get('/help/menggunakan-jasa-kami');
        $response->assertStatus(200);
    }
}
