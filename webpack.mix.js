let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.autoload({
    jquery: ['$', 'window.jQuery',"jQuery","window.$","jquery","window.jquery"],
    Popper: ['popper.js', 'default']
});

mix
	.js('resources/assets/js/app.js', 'public/js')
    .extract(['jquery', 'bootstrap', 'popper.js', 'slick-carousel'])

mix
    .js('resources/assets/js/admin.js', 'public/dashboard-assets/js');

mix
  .sass("resources/assets/sass/app.scss", "public/css")
  .sass("resources/assets/sass/admin.scss", "public/dashboard-assets/css")
  .options({
    processCssUrls: false
  });

mix.browserSync({
	proxy: process.env.MIX_BROWSERSYNC_PROXY || 'tb.test'
});

if (mix.config.inProduction) {
	mix.version();
}
