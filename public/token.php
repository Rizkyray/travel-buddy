<?php
// Get the PHP helper library from https://twilio.com/docs/libraries/php
require_once '/path/to/vendor/autoload.php'; // Loads the library
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\ChatGrant;

// Required for all Twilio access tokens
$twilioAccountSid = 'AC754e1db53fc2e74770026b0e5ec3c4ad';
$twilioApiKey = 'SK64c6f98a5b411b4c0d5dc2a85af8a4eb';
$twilioApiSecret = 'LEpFejTp5b5E8Qm5K0CSyHbEO5KEnXfv';

// Required for Chat grant
$serviceSid = 'ISa652466592b940778edae60354350a76';
// choose a random username for the connecting user
$identity = "rayo";

// Create access token, which we will serialize and send to the client
$token = new AccessToken(
    $twilioAccountSid,
    $twilioApiKey,
    $twilioApiSecret,
    3600,
    $identity
);

// Create Chat grant
$chatGrant = new ChatGrant();
$chatGrant->setServiceSid($serviceSid);

// Add grant to token
$token->addGrant($chatGrant);

// render token to string
echo $token->toJWT();