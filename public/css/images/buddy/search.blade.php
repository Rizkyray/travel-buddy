@extends('frontend.layouts.main')

@section('content')
    <div class="content">
        <section id="search-buddy">
            <div class="container">
                <buddy-search
                    method="GET"
                    action="{{ route('buddy.search') }}"
                    default-keyword="{{ app('request')->input('location') }}"
                    algolia-app-id="{{ env('ALGOLIA_APP_ID') }}"
                    algolia-api-key="{{ env('ALGOLIA_PUBLIC') }}">
                </buddy-search>
                @if (isset($location))
                    <p class="search-query">Hasil pencarian buddy dengan keyword "<span>{{ $location }}</span>"</p>
                @endif

                <div class="container card-container">
                    <div class="row">
                        @foreach($buddies as $buddy)
                            <div class="col-xl-3 col-lg-4 col-md-6 d-flex">
                                @include('frontend.buddy.components.buddy-card', compact('buddy'))
                            </div>
                        @endforeach
                    </div>
                </div>
        </section>
    </div>
@endsection