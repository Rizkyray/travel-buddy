<div class="card">
    <div class="profile__picture-wrapper">
        <div class="pp-wrapper">
            <img class="card-img-top" src="{{ Helper::getProfilePicture($buddy->user) }}" alt="Buddy picture">
            <div class="price-buddy">
                <p class="rate">
                    {{ Helper::formatCurrency($buddy->price, 'IDR') }}
                    <span class="dur">/jam</span>
                </p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="div-flex name-age">
            <h5 class="card-title">{{ $buddy->nickname }}</h5>
            <p>Age: <span>{{ $buddy->age() }}</span></p>
        </div>
        <p class="text-capitalize country"><span>{{ Helper::printArray($buddy->city_coverage)  }}</span> / {{ $buddy->country }}</p>
        <div class="div-flex abilities">
            <i class="fas fa-comment-alt abilities__icon"></i>
            <p class="text-capitalize">{{ $buddy->language }}</p>
        </div>
        <div class="div-flex abilities">
            <i class="fa fa-heart abilities__icon"></i>
            <p class="text-capitalize">{{ Helper::printArray($buddy->interests) }}</p>
        </div>
        <div class="div-flex button-wrapper">
            <a href="{{ route('buddy.profile', ['id' => $buddy->id]) }}" class="btn btn-search-buddy hollow">See Details</a>
            <a href="{{ route('buddy.book', ['id' => $buddy->id]) }}" class="btn btn-search-buddy">Book Me</a>
        </div>
    </div>
</div>