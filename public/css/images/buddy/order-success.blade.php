@extends('frontend.layouts.plain')

@section('content')
    <div class="popup">
        <div class="popup__dialog">
            <div class="popup__content">
                <h1 class="popup__title">
                    Terimakasih
                </h1>
                <p class="popup__body">
                    Atas booking yang dilakukan, untuk info lebih lanjut silahkan tunggu email konfirmasi kami atau dapat menghubungi hotline kami
                </p>
                
                <div class="contact">
                    <p>
                        <i class="fas fa-phone"></i>
                        <a href="tel:+62 877 8913 6398">+62 877 8913 6398</a>
                    </p>
                    <p>
                        <i class="fas fa-envelope"></i>
                        <a href="mailto:support@travelbuddy.id">support@travelbuddy.id</a>
                    </p>
                </div>                
            </div>
            <div class="popup__navigation">
                <a href="{{ url('/') }}" class="my-button my-button--orange">Kembali ke Halaman Utama</a>
            </div>
        </div>
    </div>
@endsection