@extends('frontend.layouts.main')

@section('content')

<div class="container">
    @php
        $previous = url()->previous();
        if (strpos($previous, '/buddy/search') === false) {
            $previous = route('buddy.search');
        }
    @endphp
    <div class="row">
        <div class="col">
            <nav aria-label="breadcrumb" class="d-none d-md-block">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ $previous }}">Search Buddies</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('buddy.profile', ['id' => $buddy->id]) }}">Buddy: {{ $buddy->nickname }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Book Buddy</li>
                </ol>
            </nav>
             <nav aria-label="breadcrumb" class="d-xs-block d-md-none">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active back"><a href="{{ $previous }}">Back</a></li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 order-2 order-lg-1">
                <form
                    id="bookingForm"
                    action="{{ route("buddy.book", ["id" => $buddy->id]) }}"
                    method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $buddy->id }}">
                    <h1 class="my-form__title">
                        Masukan data Anda untuk pengajuan booking buddy
                    </h1>
                    
                    <fieldset>
                        <legend class="my-form__set-title">Data personal Anda</legend>
                        <div class="my-form-group {{ $errors->has('name') ? 'my-form--danger' : ''}}">
                            <label for="name">Nama Lengkap</label>
                            <input
                                id="name"
                                name="name"
                                type="text"
                                class="my-form-control"
                                value="{{ old('name') }}"
                                placeholder="e.g. Airen Rizky"
                                required>
                            
                            @if ($errors->has('name'))
                                <div class="my-form__error">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>

                        <div class="my-form-group {{ $errors->has('name') ? 'my-form--danger' : ''}}">
                            <label for="name">Email</label>
                            <input
                                id="email"
                                name="email"
                                type="email"
                                class="my-form-control"
                                value="{{ old('email') }}"
                                placeholder="e.g. hello@meridian.id"
                                required>
                                
                            @if ($errors->has('email'))
                                <div class="my-form__error">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>

                        <div class="my-form-group {{ $errors->has('name') ? 'my-form--danger' : ''}}">
                            <label for="name">Nomor Telepon</label>
                            <input
                                id="phone"
                                name="phone"
                                type="text"
                                class="my-form-control"
                                value="{{ old('phone') }}"
                                placeholder="e.g. +62 877 8913 XXXX"
                                required>
                            
                            @if ($errors->has('phone'))
                                <div class="my-form__error">
                                    {{ $errors->first('phone') }}
                                </div>
                            @endif
                        </div>
                    </fieldset>

                    @php
                        $hoursPerDay = [];
                        
                        if (old('hours_per_day')) {
                            $hoursPerDay = collect(old('hours_per_day'))->map(function($val) {
                                $intVal = (int) $val;
                                
                                if ($intVal === 0) {
                                    return null;
                                }

                                return (int) $val;
                            });
                        }
                        $hoursPerDay = json_encode($hoursPerDay);
                    @endphp

                    <date-picker
                        :start-date-prop="'{{ old('start_date') }}'"
                        :end-date-prop="'{{ old('end_date') }}'"
                        :hours-per-day-prop="{{ $hoursPerDay }}"
                        :price="{{ $buddy->price }}">
                    </date-picker>
                    
                    <fieldset>
                        <legend class="my-form__set-title">Jumlah Rombongan Perjalanan</legend>
                        <div class="my-form-group-inline">
                            <div class="my-form-group my-form-group--icon-user">
                                <label for="totalAdults">Dewasa</label>
                                <select
                                    id="totalAdults"
                                    name="trip_details[total_adults]"
                                    class="my-form-control"
                                    required>
                                    <option value="1">1 orang</option>
                                    <option value="2">2 orang</option>
                                    <option value="3">3 orang</option>
                                    <option value="4">4 orang</option>
                                    <option value="5">5 orang</option>
                                    <option value="6">6 orang</option>
                                    <option value="7">7 orang</option>
                                    <option value="8">8 orang</option>
                                    <option value="9">9 orang</option>
                                </select>
                            </div>
                            <div class="my-form-group my-form-group--icon-user">
                                <label for="totalChildren">Anak-anak</label>
                                <select
                                    id="totalChildren"
                                    name="trip_details[total_children]"
                                    class="my-form-control">
                                    <option value="0">0 orang</option>
                                    <option value="1">1 orang</option>
                                    <option value="2">2 orang</option>
                                    <option value="3">3 orang</option>
                                    <option value="4">4 orang</option>
                                    <option value="5">5 orang</option>
                                    <option value="6">6 orang</option>
                                    <option value="7">7 orang</option>
                                    <option value="8">8 orang</option>
                                    <option value="9">9 orang</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    
                    <fieldset>
                        <legend class="my-form__set-title">Detail tentang perjalanan Anda</legend>
                        <p class="my-form__help">
                            Jabarkan dan ceritakan seperti apa perjalanan yang ingin anda lakukan bersama dengan buddy yang anda booking.
                        </p>
                        <div class="my-form-group {{ $errors->has('trip_details.destination') ? 'my-form--danger' : ''}}">
                            <label for="name">Kota Tujuan</label>
                            @php
                                $destination = session('trip_destination', null);
                                $cityCollection = collect($buddy->city_coverage);

                                $destinationIndex = false;

                                if ($destination) {
                                    $destinationIndex = $cityCollection->search(function($item, $key) use($destination) {
                                        return strpos(strtolower($item), strtolower($destination)) !== false;
                                    });
                                }
                            @endphp
                            <select
                                id="destination"
                                name="trip_details[destination]"
                                class="my-form-control"
                                required>
                                @foreach ($buddy->city_coverage as $index => $city)
                                    <option value="{{ $city }}" {{ $index === $destinationIndex ? 'selected' : '' }}>{{ $city }}</option>
                                @endforeach
                            </select>
                            
                            @if ($errors->has('trip_details.destination'))
                                <div class="my-form__error">
                                    {{ $errors->first('trip_details.destination') }}
                                </div>
                            @endif
                        </div>
                        <div class="my-form-group {{ $errors->has('name') ? 'my-form--danger' : ''}}">
                        </div>
                        <fieldset>
                            <legend class="my-form__title">Tujuan Perjalanan</legend>
                            <custom-checkbox
                                name="trip_details[trip_type][]"
                                id="tripType"
                                :manual-input="true"
                                :checkboxes="[
                                    {
                                        id: 'bisnis',
                                        value: 'bisnis',
                                        label: 'Bisnis'
                                    },
                                    {
                                        id: 'akademis',
                                        value: 'akademis',
                                        label: 'Akademis'
                                    },
                                    {
                                        id: 'liburan',
                                        value: 'liburan',
                                        label: 'Liburan'
                                    },
                                    {
                                        id: 'pengobatan',
                                        value: 'pengobatan',
                                        label: 'Pengobatan'
                                    },
                                    {
                                        id: 'studibanding',
                                        value: 'studi banding',
                                        label: 'Studi Banding'
                                    }
                                ]">
                            </custom-checkbox>

                            
                            @if ($errors->has('trip_details.trip_type'))
                                <div class="my-form__error">
                                    <span>Tujuan perjalanan harus diisi.</span>
                                </div>
                            @endif
                        </fieldset>
                        
                        <div class="my-form-group">
                            <label for="trip_description">Deskripsi Perjalanan</label>
                            <textarea
                                    id="trip_description"
                                    name="trip_details[trip_description]"
                                    class="my-form-control"
                                    required>{{ old('trip_details.trip_description') }}</textarea>

                            @if ($errors->has('trip_details.trip_description'))
                                <div class="my-form__error">
                                    {{ $errors->first('trip_details.trip_description') }}
                                </div>
                            @endif
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend class="my-form__set-title">Pesan untuk Buddy</legend>
                        <p class="my-form__help">
                            Sampaikan hal-hal diluar detil perjalanan yang perlu anda beritahukan kepada buddy seperti anda mempunyai preferensi seperti apa, punya pantangan makanan seperti apa dan sebagainya.
                        </p>
                        <div class="my-form-group">
                            <textarea
                                id="message"
                                name="message_to_buddy"
                                class="my-form-control"
                                required>{{ old('message_to_buddy') }}</textarea>

                            @if ($errors->has('message_to_buddy'))
                                <div class="my-form__error">
                                    {{ $errors->first('message_to_buddy') }}
                                </div>
                            @endif
                        </div>
                    </fieldset>
                    <div class="my-form__terms-wrapper">
                        <input type="checkbox" required>
                        <p class="my-form__terms">
                            Dengan melakukan pengajuan booking buddy, maka anda secara sadar memenuhi <a href="{{ url('syarat-dan-ketentuan') }}">Syarat dan Ketentuan</a> kami.
                        </p>
                    </div>
                    <div id="orderReview">
                        <order-review
                            form-id="bookingForm"
                            :buddy-price="{{ $buddy->price }}">
                        </order-review>
                    </div>
                </form>
            </div>
            <div class="col-lg-5 order-1 order-lg-2">
                <div class="my-card my-card--default my-card--with-media my-card--sticky-top">
                    <div class="my-card__media">                    
                        <img src="{{ Helper::getProfilePicture($buddy->user) }}">
                    </div>
                    <div class="my-card__body">
                        <div class="buddy">
                            <p class="buddy--heading">Buddy Pilihan Anda</p>
                            <p class="buddy--name">{{ $buddy->nickname }}</p>
                            <p class="buddy--location">                            
                                <span class="buddy--country">{{ $buddy->country }}</span>
                            </p>
                            <a href="{{ $previous }}" class="my-button my-button--default my-button--block">Ganti Buddy</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
@endsection