$(document).ready(function() {
    {
        let toggler = $('.toggler');

        toggler.on('click', function(e) {
            e.preventDefault();

            let target = $($(this).data('target'));
            let expanded = JSON.parse(target.attr('aria-expanded'));
            expanded = !expanded;
    
            target.attr('aria-expanded', JSON.stringify(expanded));
        });
    }

    {
        $(document).on('click', function(e) {
            let toggler = $('.toggler');

            toggler.each(function() {
                let target = $($(this).data('target'));
                let clickTarget = $(e.target);
                let current = $(this);
                
                // Hide toggler target
                let condition = (
                    // If click target is toggler
                    current.is(clickTarget) ||
                    
                    // If click target happens inside toggler
                    current.has(clickTarget).length > 0 ||
                    
                    // If click target happens inside target
                    target.has(clickTarget).length > 0 ||
                    
                    // If click target is target
                    target.is(clickTarget)
                );

                if (!condition) {
                    target.attr('aria-expanded', 'false');
                }
            });
        });
    }
});