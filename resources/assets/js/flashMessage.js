$(document).ready(function () {
    $('.profile__flash-message__close').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.profile__flash-message').remove();
    });

    $('.flash-message__close').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.flash-message').remove();
    });

    {
        let flashMessage = $('.flash-message');
        flashMessage.addClass('active');
        setTimeout(() => {
            flashMessage.removeClass('active');
    
            setTimeout(() => {
                flashMessage.remove();
            }, 1000);
        }, 10000);
    }

});