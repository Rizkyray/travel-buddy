import Vue from "vue";
import Autocomplete from "v-autocomplete";

Vue.component("date-picker", require("./components/DatePicker.vue"));
Vue.component("custom-datepicker", require("vuejs-datepicker"));
Vue.component("custom-checkbox", require("./components/CustomCheckbox.vue"));
Vue.component(
  "country-city",
  require("./components/forms/CountryCityForm.vue")
);


new Vue({
  el: "#app"
});
