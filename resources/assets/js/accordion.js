$(document).ready(function() {
    let accordions = document.querySelectorAll('.tb-accordion');
    accordions.forEach((accordion) => {
        let accordionHeader = accordion.querySelector('.tb-accordion__header');
        let toggler = accordion.querySelector('.tb-accordion__toggler')
        let accordionContent = accordion.querySelector('.tb-accordion__content');
        accordionContent.style.height = accordionContent.offsetHeight + 'px';

        toggler.addEventListener('click', () => {
            accordion.classList.toggle('active');

            if (accordion.className.includes('active')) {
                accordionContent.style.height = 'auto';
                let height = accordionContent.offsetHeight;

                accordionContent.style.height = '0px';
                
                setTimeout(() => {
                    accordionContent.style.height = height + 'px';
                }, 0);
            } else {
                accordionContent.style.height = '0px';
            }
        });
    });
});