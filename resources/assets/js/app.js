require('./bootstrap.js');
require('./main.js');
require('./searchBuddy.js');
require('./flashMessage.js');
require('./header.js');
require('./accordion.js');

import Vue from 'vue';
import Autocomplete from 'v-autocomplete';

Vue.component('date-picker', require('./components/DatePicker.vue'));

Vue.component('custom-datepicker', require('vuejs-datepicker'));

Vue.component('custom-checkbox', require('./components/CustomCheckbox.vue'));

Vue.component('buddy-search', require('./components/BuddySearch.vue'));

Vue.component(
  'country-city',
  require('./components/forms/CountryCityForm.vue')
);

Vue.component(
  'v-tag-test',
  require('./components/forms/AutoCompleteTagTest.vue')
);

Vue.component(
  'v-autocomplete-test',
  require('./components/forms/AutocompleteTest.vue')
);

Vue.component(
  'v-header-buddy-search',
  require('./components/header/SearchBuddy.vue')
);

Vue.component('tb-modal', require('./components/Modal.vue'));

Vue.component('order-review', require('./components/order/OrderReview.vue'));

Vue.component('price-estimation', require('./components/PriceEstimation.vue'));

Vue.component('language-picker', require('./components/forms/Language.vue'));

Vue.use(Autocomplete);

new Vue({
  el: '#app',
});
