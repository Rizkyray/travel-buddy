require('slick-carousel');

function rotate(){
	var loop = true,
			itemInterval = 6000,
			animationSpeed = 500,
			$section = $("#intro-section"),
			$con = $section.find(".rotation"),
			$elems = $con.find("span"),
			$elems_no = $elems.length,
			$target = $con.find("span"),
			currentItem = 0,
			elem_html, el_height, $el;

	$elems.each(function(){
		$(this).attr("data-index", $(this).index());
	});

	var infiniteLoop = setInterval(function(){
		// el_height = 70;
        $el = $con.find("span[data-index="+currentItem+"]");
        el_height = $el.css('line-height');
		$el.animate({"margin-top": "-" + el_height}, animationSpeed, function(){
			$el.removeAttr("style");
			elem_html = $el.clone();
				$el.remove();
			$con.append(elem_html);
		});

		if(loop == true){
			if(currentItem == $elems_no - 1){
				currentItem = 0;
			}else{
				if(currentItem == undefined){
					currentItem = 0;
					}else {
				currentItem++;
					}
			}
		} else {
			if(currentItem == $elems_no - 2){
				clearInterval(infiniteLoop);
				currentItem = $elems_no - 1;
			}else{
				if(currentItem == undefined){
					currentItem = 0;
				} else {
					currentItem++;
				}
			}
		}
		setTimeout(function(){
			$section.attr("class","background-"+(currentItem)).fadeIn("slow");
		}, 0);

	}, itemInterval);
}

setTimeout(function(){ rotate(); }, 100);
// rotate();

// $(function(){
//     activate('img[src*=".svg"]');

//     function activate(string){
//         jQuery(string).each(function(){
//             var $img = jQuery(this);
//             var imgID = $img.attr('id');
//             var imgClass = $img.attr('class');
//             var imgURL = $img.attr('src');

//             jQuery.get(imgURL, function(data) {
//                 // Get the SVG tag, ignore the rest
//                 var $svg = jQuery(data).find('svg');

//                 // Add replaced image's ID to the new SVG
//                 if(typeof imgID !== 'undefined') {
//                     $svg = $svg.attr('id', imgID);
//                 }
//                 // Add replaced image's classes to the new SVG
//                 if(typeof imgClass !== 'undefined') {
//                     $svg = $svg.attr('class', imgClass+' replaced-svg');
//                 }

//                 // Remove any invalid XML tags as per http://validator.w3.org
//                 $svg = $svg.removeAttr('xmlns:a');

//                 // Replace image with new SVG
//                 $img.replaceWith($svg);

//             }, 'xml');
//         });
//     }
// });

$(document).ready(function(){
	$('.testimonials-carousel').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		adaptiveHeight: true,
		centerMode: true,
		variableWidth: true
    });
    
    $('.to-top').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    });

    $(window).scroll(function () {
        var toTop = $('.to-top');
        if ($(this).scrollTop() > 1000) {
            toTop.addClass('active');
        } else {
            toTop.removeClass('active');
        }
    });
});