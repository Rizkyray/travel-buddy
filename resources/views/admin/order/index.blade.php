@extends('admin.layouts.main')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
            @if ($orders->count() > 0)
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fa fa-table"></i> Daftar Order
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tanggal Order</th>
                                        <th>Order Number</th>
                                        <th>Informasi Pemesan</th>
                                        <th>Buddy yang Dipesan</th>
                                        {{-- <th>Tanggal Perjalanan</th> --}}
                                        {{-- <th>Trip Detail</th> --}}
                                        <th>Status</th>
                                        <th>Estimasi Harga</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tanggal Order</th>
                                        <th>Order Number</th>
                                        <th>Informasi Pemesan</th>
                                        <th>Buddy yang Dipesan</th>
                                        {{-- <th>Tanggal Perjalanan</th> --}}
                                        {{-- <th>Trip Detail</th> --}}
                                        <th>Status</th>
                                        <th>Estimasi Harga</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach ($orders as $index => $order)
                                        <tr>
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ (new Carbon($order->created_at))->formatLocalized('%e %b %Y %R') }}</td>
                                            <td>{{ $order->order_number }}</td>
                                            <td>
                                                {{ $order->name }}
                                                <br>
                                                {{ $order->email }}
                                                <br>
                                                {{ $order->phone }}
                                            </td>
                                            <td>{{ $order->buddy->nickname }}</td>
                                            {{-- <td>
                                                @php
                                                    $startDate = new Carbon($order->start_date);
                                                    $endDate = (new Carbon($order->end_date))->addDay();

                                                    $interval = new DateInterval('P1D');
                                                    $dateRange = new DatePeriod($startDate, $interval ,$endDate);
                                                @endphp

                                                @foreach ($dateRange as $index => $date)
                                                    @php
                                                        $dt = new Carbon($date);
                                                        $formatedDate = $dt->formatLocalized('%e %b %Y');
                                                    @endphp
                                                    {{ $formatedDate }} ({{ $order->hours_per_day[$index] }} Jam)
                                                    <br>
                                                @endforeach
                                            </td> --}}
                                            {{-- <td>
                                                Jumlah Dewasa: {{ $order->trip_details['total_adults'] }}
                                                <br>
                                                Jumlah Anak-anak: {{ $order->trip_details['total_children'] }}
                                                <br>
                                                Deskripsi Perjalanan: {{ $order->trip_details['trip_description'] }}
                                                <br>
                                                Pesan untuk buddy: {{ $order->message_to_buddy }}
                                            </td> --}}
                                            <td>
                                                @php
                                                    $orderStatus = $order->status;
                                                    $paymentStatus = $order->payment_status;

                                                    switch($orderStatus) {
                                                        case \App\Order::ORDER_SUBMITTED:
                                                            $orderStatusText = '<span class="text-info">Diajukan (submitted)<span>';
                                                            break;
                                                        case \App\Order::ORDER_ACCEPTED:
                                                            $orderStatusText = '<span class="text-success">Diterima (accepted)<span>';
                                                            break;
                                                        case \App\Order::ORDER_DECLINED:
                                                            $orderStatusText = '<span class="text-danger">Ditolak (declined)<span>';
                                                            break;
                                                    }

                                                    switch($paymentStatus) {
                                                        case \App\Order::PAYMENT_PAID:
                                                            $paymentStatusText = '<span class="text-success">Paid<span>';
                                                            break;
                                                        case \App\Order::PAYMENT_UNPAID:
                                                            $paymentStatusText = '<span class="text-danger">Unpaid<span>';
                                                            break;
                                                    }

                                                @endphp
                                                <p>Order Status: {!! $orderStatusText !!}</p>
                                                <p>Payment Status: {!! $paymentStatusText !!}</p>
                                            </td>
                                            <td>{{ Helper::formatCurrency($order->price_estimation) }}</td>
                                            <td><a href="{{ route('admin.order.item', ['id' => $order->id]) }}">Lihat Order</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="card mb-3">
                    <div class="card-header">
                        Daftar Order
                    </div>
                    <div class="card-body">
                        <p>Data order belum ada</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

