@extends('admin.layouts.main')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-md-6">
                @if (Session::has('success'))
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-success">{{ Session::get('success') }}</li>
                    </ul>
                @endif
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-7">
                <h2>Nomor Order: {{ $order->order_number }}</h2>
                <table class="table">
                    <tbody>
                        <tr>
                            <td width="200px">Tanggal Order</td>
                            <td>: {{ Helper::formatDate($order->created_at) }}<br>(Latest update {{ Helper::formatDate($order->updated_at) }})</td>
                        </tr>
                        <tr>
                            <td>Estimasi Biaya</td>
                            <td>: {{ Helper::formatCurrency($order->price_estimation) }}</p></td>
                        </tr>
                        <tr>
                            <td>Kota Tujuan</td>
                            <td>: {{ $order->trip_details['destination'] }}</td>
                        </tr>
                        <tr>
                            <td>Buddy</td>
                            <td>: {{ $order->buddy->nickname }}</td>
                        </tr>
                        <tr>
                            <td>Tujuan Perjalanan</td>
                            <td>:
                                @foreach ($order->trip_details['trip_type'] as $index => $type)
                                    {{ $type }}
                                    @if ($index !== count($order->trip_details['trip_type']) - 1)
                                        ,
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Rombongan</td>
                            <td>: {{ $order->trip_details['total_adults'] }} {{ ' orang dewasa' }}@if ((int) $order->trip_details['total_children'] > 0), {{ $order->trip_details['total_children'] }} orang anak @endif</td>
                        </tr>
                        <tr>
                            <td>Deskripsi Perjalanan</td>
                            <td>:</td>
                        </tr>
                        <tr>
                            <td colspan="2">{{ $order->trip_details['trip_description'] }}</td>
                        </tr>
                        <tr>
                            <td>Pesan untuk Buddy</td>
                            <td>:</td>
                        </tr>
                        <tr>
                            <td colspan="2">{{ $order->message_to_buddy }}</td>
                        </tr>
                    </tbody>
                </table>
                <h2>Rincian Perjalanan</h2>
                @php
                    $startDate = new Carbon($order->start_date);
                    $endDate = (new Carbon($order->end_date))->addDay();

                    $interval = new DateInterval('P1D');
                    $dateRange = new DatePeriod($startDate, $interval ,$endDate);
                @endphp
                <table class="table">
                    <thead>
                        <tr>
                            <th width="200px">Tanggal</th>
                            <th>Waktu (Jam)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dateRange as $index => $date)
                            @php
                                $dt = new Carbon($date);
                                $formatedDate = $dt->formatLocalized('%e %B %Y');
                            @endphp
                            <tr class="table__row">
                                <td>{{ $formatedDate }}</td>
                                <td>{{ $order->hours_per_day[$index] }} Jam</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
                <button form="reconfirmOrder" class="btn btn-outline-info">Reconfirm Order</button>
                <form
                    id="reconfirmOrder"
                    style="display:none"
                    method="POST"
                    action="{{ route('admin.order.confirm')}}">

                    {{ csrf_field() }}
                    <input type="text" name="id" value="{{ $order->id }}">
                </form>
                <a href="{{ route('admin.order.update.view', ['id' => $order->id]) }}" class="btn btn-primary">Edit</a>
            </div>
            <div class="col-md-4">
                <h2>Status</h2>
                <form
                    method="POST"
                    action="{{ route('admin.order.status') }}">

                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <input type="hidden" name="order_id" value="{{ $order->id }}">

                    <div class="form-group row">
                        <label for="status" class="col-sm-4 col-form-label">Status Order</label>
                        <select class="custom-select col-sm-8" id="status" name="status">
                            @foreach (\App\Order::ORDER_STATUS as $status)
                                <option value="{{ $status }}" {{ $status === $order->status ? 'selected' : '' }}>{{ $status }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="payment_status" class="col-sm-4 col-form-label">Status Pembayaran</label>
                        <select class="custom-select col-sm-8" id="payment_status" name="payment_status">
                            @foreach (\App\Order::PAYMENT_STATUS as $status)
                                <option value="{{ $status }}" {{ $status === $order->payment_status ? 'selected' : '' }}>{{ $status }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan Status Order</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection