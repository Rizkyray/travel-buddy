@extends('admin.layouts.main')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 mb-3">
                @if (Session::has('success'))
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-success">{{ Session::get('success') }}</li>
                    </ul>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 mb-3">
                <h2 class="my-form__title">
                    Edit order #{{ $order->order_number }}
                </h2>
                <form
                    id="bookingForm"
                    action="{{ route('admin.order.update', ['id' => $order->id] ) }}"
                    method="POST">

                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <input type="hidden" name="buddy_id" value="{{ $order->buddy->id }}">
                    
                    <fieldset class="form-group">
                        <div class="form-group {{ $errors->has('name') ? 'form--danger' : ''}}">
                            <label for="name">Nama Lengkap</label>
                            <input
                                id="name"
                                name="name"
                                type="text"
                                class="form-control"
                                value="{{ $order->name }}"
                                required>
                            
                            @if ($errors->has('name'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('name') }}
                                </small>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('name') ? 'form--danger' : ''}}">
                            <label for="name">Email</label>
                            <input
                                id="email"
                                name="email"
                                type="email"
                                class="form-control"
                                value="{{ $order->email }}"
                                required>
                                
                            @if ($errors->has('email'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('email') }}
                                </small>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('name') ? 'my-form--danger' : ''}}">
                            <label for="name">Nomor Telepon</label>
                            <input
                                id="phone"
                                name="phone"
                                type="text"
                                class="form-control"
                                value="{{ $order->phone }}"
                                required>
                            
                            @if ($errors->has('phone'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('phone') }}
                                </small>
                            @endif
                        </div>
                    </fieldset>

                    @php
                        $hoursPerDay = [];
                        
                        if ($order->hours_per_day) {
                            $hoursPerDay = collect($order->hours_per_day)->map(function($val) {
                                $intVal = (int) $val;
                                
                                if ($intVal === 0) {
                                    return null;
                                }

                                return (int) $val;
                            });
                        }
                        $hoursPerDay = json_encode($hoursPerDay);
                    @endphp

                    @php
                        $startDate = new Carbon($order->start_date);
                        $endDate = new Carbon($order->end_date);

                        $startDate = $startDate->format('D, d/m/Y');
                        $endDate = $endDate->format('D, d/m/Y');
                    @endphp

                    <date-picker
                        :start-date-prop="'{{ $startDate }}'"
                        :end-date-prop="'{{ $endDate }}'"
                        :hours-per-day-prop="{{ $hoursPerDay }}"
                        :price="{{ $order->buddy->price }}">
                    </date-picker>
                    
                    <fieldset clas="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="totalAdults">Dewasa</label>
                                    <select
                                        id="totalAdults"
                                        name="trip_details[total_adults]"
                                        class="form-control"
                                        required>
                                        <option value="1">1 orang</option>
                                        <option value="2">2 orang</option>
                                        <option value="3">3 orang</option>
                                        <option value="4">4 orang</option>
                                        <option value="5">5 orang</option>
                                        <option value="6">6 orang</option>
                                        <option value="7">7 orang</option>
                                        <option value="8">8 orang</option>
                                        <option value="9">9 orang</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="totalChildren">Anak-anak</label>
                                    <select
                                        id="totalChildren"
                                        name="trip_details[total_children]"
                                        class="form-control">
                                        <option value="0">0 orang</option>
                                        <option value="1">1 orang</option>
                                        <option value="2">2 orang</option>
                                        <option value="3">3 orang</option>
                                        <option value="4">4 orang</option>
                                        <option value="5">5 orang</option>
                                        <option value="6">6 orang</option>
                                        <option value="7">7 orang</option>
                                        <option value="8">8 orang</option>
                                        <option value="9">9 orang</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    
                    <fieldset class="form-group">
                        <div class="form-group {{ $errors->has('trip_details.destination') ? 'my-form--danger' : ''}}">
                            <label for="name">Kota Tujuan</label>
                            <select
                                id="destination"
                                name="trip_details[destination]"
                                class="form-control"
                                required>
                                @foreach ($order->buddy->city_coverage as $index => $city)
                                    <option value="{{ $city }}" {{ $city === $order->trip_details['destination'] ? 'selected' : '' }}>{{ $city }}</option>
                                @endforeach
                            </select>
                            
                            @if ($errors->has('trip_details.destination'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('trip_details.destination') }}
                                </small>
                            @endif
                        </div>
                        
                        <fieldset class="form-group">
                            <label for="tripType">Tujuan Perjalanan</label>
                            @php
                                $tripType = $order->trip_details['trip_type'];
                                $tripType = json_encode($tripType);
                            @endphp
                            <custom-checkbox
                                name="trip_details[trip_type][]"
                                id="tripType"
                                :selected="{{ $tripType }}"
                                :manual-input="true"
                                :checkboxes="[
                                    {
                                        id: 'bisnis',
                                        value: 'bisnis',
                                        label: 'Bisnis'
                                    },
                                    {
                                        id: 'akademis',
                                        value: 'akademis',
                                        label: 'Akademis'
                                    },
                                    {
                                        id: 'liburan',
                                        value: 'liburan',
                                        label: 'Liburan'
                                    },
                                    {
                                        id: 'pengobatan',
                                        value: 'pengobatan',
                                        label: 'Pengobatan'
                                    },
                                    {
                                        id: 'studi banding',
                                        value: 'studi banding',
                                        label: 'Studi Banding'
                                    }
                                ]">
                            </custom-checkbox>

                            @if ($errors->has('trip_details.trip_type'))
                                <small class="form-text text-danger">
                                    Tujuan perjalanan harus diisi.
                                </small>
                            @endif
                        </fieldset>
                        
                        <div class="form-group">
                            <label for="trip_description">Deskripsi Perjalanan</label>
                            <textarea
                                    id="trip_description"
                                    name="trip_details[trip_description]"
                                    class="form-control"
                                    required>{{ $order->trip_details['trip_description'] }}</textarea>

                            @if ($errors->has('trip_details.trip_description'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('trip_details.trip_description') }}
                                </small>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <label for="message">Pesan Untuk Buddy</label>
                            <textarea
                                id="message"
                                name="message_to_buddy"
                                class="form-control"
                                required>{{ $order->message_to_buddy }}</textarea>

                            @if ($errors->has('message_to_buddy'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('message_to_buddy') }}
                                </small>
                            @endif
                        </div>
                    </fieldset>

                    <button type="submit" class="btn btn-primary">Simpan Data Order</button>
                    <a href="{{ route('admin.order.item', ['id' => $order->id]) }}" class="btn btn-outline-secondary">Batal</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection