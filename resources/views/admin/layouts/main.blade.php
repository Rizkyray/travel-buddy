<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="csrf_token" content="{{csrf_token()}}"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#ffffff">
    <title>Travel Buddy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap core CSS-->
    <link href="{{ url('dashboard-assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="{{ url('dashboard-assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Page level plugin CSS-->
    <link href="{{ url('dashboard-assets/vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ url('dashboard-assets/css/sb-admin.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('dashboard-assets/css/admin.css') }}">

    
</head>

    <body class="fixed-nav">
        <div id="app">
            @include('admin.components.nav')
        
            @yield('content')

            @include('admin.components.footer')
        </div>

        <script src="{{ url('/') . mix('js/manifest.js') }}"></script>
		<script src="{{ url('/') . mix('js/vendor.js') }}"></script>
        
        <!-- Bootstrap core JavaScript-->
        <script src="{{ url('dashboard-assets/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ url('dashboard-assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

        <!-- Core plugin JavaScript-->
        <script src="{{ url('dashboard-assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <!-- Page level plugin JavaScript-->
        <script src="{{ url('dashboard-assets/vendor/datatables/jquery.dataTables.js') }}"></script>
        <script src="{{ url('dashboard-assets/vendor/datatables/dataTables.bootstrap4.js') }}"></script>
        <!-- Custom scripts for all pages-->
        <script src="{{ url('dashboard-assets/js/sb-admin.min.js') }}"></script>
        <!-- Custom scripts for this page-->
        <script src="{{ url('dashboard-assets/js/sb-admin-datatables.min.js') }}"></script>

        <script src="{{ url('dashboard-assets/js/admin.js') }}"></script>
        
        @yield('script')

    </body>
</html>
