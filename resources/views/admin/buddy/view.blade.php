@extends('admin.layouts.main')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-md-6 ml-3">
                <div class="profile__section">
                    <h1 class="profile__name">{{ $buddy->nickname }}</h1>
                    <p class="profile__description">{{ $buddy->description }}</p>
                </div>
                
                <div class="profile__section">
                    <h2 class="profile__title">Kota Cakupan</h2>
                    <ul class="profile__list city-coverage">
                        @foreach ($buddy->city_coverage as $city)
                            <li>{{ $city }}, {{ $buddy->country }}</li>
                        @endforeach
                    </ul>
                </div>

                <div class="profile__section">
                    <h2 class="profile__title">Kemampuan dan Minat Travelling</h2>
                    
                    <h3 class="profile__skills language">Bahasa</h3>
                    <ul class="profile__list">
                        <li>{{ $buddy->language }}</li>
                    </ul>
                    
                    <h3 class="profile__skills interest">Personal Interest</h3>
                    <ul class="profile__list">
                        <li>{{ Helper::printArray($buddy->interests) }}</li>
                    </ul>
                    
                    <h3 class="profile__skills skill">Additional Skills</h3>
                    <ul class="profile__list">
                        <li>{{ $buddy->skills }}</li>
                    </ul>
                    
                </div>

                <div class="profile__section">
                    <h2 class="profile__title">Pengalaman sebagai Buddy Travelling</h2>
                    <p class="profile__description">{{ $buddy->experience }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection