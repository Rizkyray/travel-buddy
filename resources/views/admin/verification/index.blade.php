@extends('admin.layouts.main')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 mb-3">
                @if (Session::has('success'))
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-success">{{ Session::get('success') }}</li>
                    </ul>
                @endif
            </div>
        </div>
        <div class="row">
            @if ($buddies->count() > 0)
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fa fa-table"></i> Daftarin Buddy
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Nokop.</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>HP</th>
                                        <th>Domisili</th>
                                        <th>Kota Cakupan</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>HP</th>
                                        <th>Domisili</th>
                                        <th>Kota Cakupan</th>
                                        <th>ktp</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach ($buddies as $index => $buddy)
                                        <tr>
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ $buddy->user->name }}</td>
                                            <td>{{ $buddy->user->email }}</td>
                                            <td>{{ $buddy->user->phone }}</td>
                                            <td>{{ $buddy->city }}, {{ $buddy->country }}</td>
                                            <td>{{ Helper::printArray($buddy->city_coverage) }}</td>
                                            <td>{{ Helper::getProfilePicture($user) }}</td>
                                            <td>
                                                <a href="{{ route('admin.buddy.view', ['id' => $buddy->id]) }}" class="btn btn-info">Lihat Buddy</a>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#deleteBuddy{{ $index }}">
                                                Hapus Buddy
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="deleteBuddy{{ $index }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>
                                                                Apakah Anda yakin akan menghapus Buddy {{ $buddy->user->name }}?
                                                                <br>
                                                                <br>
                                                                <span class="text-danger">Perhatian: Order yang dimiliki oleh Buddy {{ $buddy->user->name }} akan ikut terhapus.</span>
                                                            </p>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>

                                                            <form
                                                                style="display:none"
                                                                id="formDeleteBuddy{{ $buddy->id }}"
                                                                method="POST"
                                                                action="{{ route('admin.buddy.delete', ['id' => $buddy->id]) }}">
                                                                {{ csrf_field() }}
                                                                {{ method_field('DELETE')}}
                                                            </form>
                                                            <button type="submit" form="formDeleteBuddy{{ $buddy->id }}" class="btn btn-danger">Hapus Buddy</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="card mb-3">
                    <div class="card-header">
                        Daftar Buddy
                    </div>
                    <div class="card-body">
                        <p>Data buddy belum ada</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

