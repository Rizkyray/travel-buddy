@extends('frontend.layouts.main')

@section('content')
    <div class="my-hero">
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-2 order-md-1">
                    <div class="my-hero__body">
                        <h1 class="my-hero__title">Tentang Kami</h1>
                        <p>
                           Travel Buddy adalah sebuah platform yang mempertemukan Anda dengan orang Indonesia di negara tujuan yang akan menemani selama perjalanan. Kami menawarkan fleksibilitas dalam mengatur dan merencanakan perjalanan anda sesuai dengan permintaan dan kebutuhan. Tidak hanya sekedar menemani, para “Buddies” juga akan berbagi kisah dan pengetahuan lokal inspiratif yang akan membuat petualangan Anda semakin berkesan.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 order-1 order-md-2">
                    <div class="my-hero__image">
                        <img src="{{ asset('img/about-us/main-photo.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container about-us">
        <div class="row post">
            <div class="col-lg-10 mx-auto">
                <div class="story">
                    <div class="story__images">
                        <div class="story__image">
                            <img src="{{ asset('img/about-us/kisah-kami-2.jpg') }}" alt="">
                        </div>
                        <div class="story__image">
                            <img src="{{ asset('img/about-us/kisah-kami-1.jpg') }}" alt="">
                        </div>
                    </div>
                    <div class="story__body">
                        <h2 class="story__title">Kisah Kami</h2>
                        <p class="story__paragraph">
                           Kisah Travel Buddy berawal dari pelajaran-pelajaran yang diambil dari pengalaman Rizky Lasabuda selama 6 tahun tinggal di Belanda. Di saat traveling menjadi sebuah gaya hidup, kendala bahasa, keterbatasan pengetahuan, dan culture shock menjadi tantangan tersendiri dalam mewujudkan perjalanan yang mengesankan.
                        </p>
                        <p class="story__paragraph">
                            Sebagai contoh, ketika saya pertama kali ke Paris bersama orang tua, kami tidak begitu familiar dengan kota tersebut dan akhirnya hanya menikmati Menara Eiffle dan Champs Elysees saja. Atau ketika pertama kali ke Amsterdam, kami tidak terlalu mengerti mengenai transportasi umum maupun tempat-tempat wisata dan akhirnya mengandalkan bus hop-on-hop-off dan daftar “must-see” yang kami dapat dari Internet dan brosur hotel.
                        </p>
                         <p class="story__paragraph">
                             Awalnya saya merasa tidak ada yang salah dengan hal diatas, bahkan kami cukup puas dengan perjalanan tersebut. Namun ketika sudah cukup lama tinggal di luar negeri dan mengerti seluk-beluk mengenai tempat wisata, saya tersadar bahwa dengan rentang waktu dan biaya yang sama, saya bisa mendapatkan pengalaman jalan-jalan yang jauh lebih berkesan, baik dari segi tempat tujuan maupun kegiatan/aktifitas.
                        </p>
                        <p class="story__paragraph">
                            Dari sini saya melihat peluang bagi para pelajar dan WNI yang tinggal di luar negeri untuk menjadi solusi dari permasalahan tersebut. Dengan mengangkat konsep familiarity, terciptalah sebuah gagasan penyediaan jasa guide yang kenal betul dengan sejarah dan kebudayaan asing tapi masih memiliki akar budaya Indonesia. Dimulai di tahun 2017, Travel Buddy menjadi wujud nyata dari gagasan tersebut.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="story">
                    <div class="story__images">
                        <div class="story__image">
                            <img src="{{ asset('img/about-us/green.jpg') }}" alt="">
                        </div>
                        <div class="story__image">
                            <img src="{{ asset('img/about-us/kisah-kami-4.jpg') }}" alt="">
                        </div>
                    </div>
                    <div class="story__body">
                        <h2 class="story__title">Dampak Sosial</h2>
                        <p class="story__paragraph">
                            Travel Buddy hadir untuk membuka prespektif baru tentang nilai-nilai kebudayaan dan keberagaman. Hal ini dapat dicapai dengan memperkenalkan kepada Traveler nilai kehidupan yang dipegang oleh budaya asing, seperti kebiasaan saling bertegur sapa, mengantri, dan budaya membaca.
                        </p>
                        <p class="story__paragraph">
                            Travel Buddy juga mengajak Traveler dari Indonesia untuk melihat gaya hidup green living di negara-negara maju dengan harapan agar para traveler lebih bisa menghargai alam dan lingkungan di negaranya sendiri. Inilah salah satu bentuk kontribusi kami dalam menjadikan pariwisata sebagai sarana untuk membawa perubahan positif untuk masyrakat Indonesia.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="story">
                    <div class="story__images">
                        <div class="story__image">
                            <img src="{{ asset('img/about-us/rame-rame-2.jpg') }}" alt="">
                        </div>
                    </div>
                    <div class="story__body">
                        <h2 class="story__title">Komunitas Kami</h2>
                        <p class="story__paragraph">
                            Travel Buddy selalu berusaha untuk terus mengembangkan sebuah komunitas orang-orang yang punya semangat dan kecintaan dalam membangun Indonesia melalui parawisata. Saat ini kami sudah terhubung dengan puluhan "Buddies" yang tersebar di kota-kota besar Benua Eropa, Amerika, dan Asia.
                        </p>
                        <a href="{{ route('dashboard.buddy') }}" class="my-button my-button--orange">Bergabung dengan Kami</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection