@extends('frontend.layouts.main')

@section('content')
	@include('frontend.components.nav')
	<section id="search-buddy">
		<div class="container">
			<form class="form-search-city">
				<div class="form-group">
					<div class="wrapper-input">
						<input type="text" class="form-control" id="city" aria-describedby="cityHelp" placeholder='Try "Amsterdam"'>
						<button type="button" class="btn btn-search-buddy">Cari Buddy</button>
					</div>
				</div>
			</form>
			<p class="search-query">Hasil pencarian buddy dengan keyword "<span>semuanya</span>"</p>

			<div class="container card-container">
				<div class="row">
					<div class="col-3">
						<div class="card">
							<div class="pp-wrapper">
								<img class="card-img-top" src="{{asset('img/buddy-img/Griyana2.jpg')}}" alt="Card image cap">
								<div class="price-buddy">
									<p class="rate">
										IDR 120 K
										<span class="dur">/jam</span>
									</p>
								</div>
							</div>
							<div class="card-body">
								<div class="div-flex name-age">
									<h5 class="card-title">Mentari</h5>
									<p>Age: <span>25</span></p>
								</div>
								<p class="text-capitalize country"><span>den haag</span> / belanda</p>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">indonesia</p>
								</div>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">photography</p>
								</div>
								<button type="button" class="btn btn-search-buddy hollow">See Details</button>
								<button type="button" class="btn btn-search-buddy">Book Me</button>
							</div>
						</div>
					</div>
					<div class="col-3">
						<div class="card">
							<div class="pp-wrapper">
								<img class="card-img-top" src="{{asset('img/buddy-img/Griyana2.jpg')}}" alt="Card image cap">
								<div class="price-buddy">
									<p class="rate">
										IDR 120 K
										<span class="dur">/jam</span>
									</p>
								</div>
							</div>
							<div class="card-body">
								<div class="div-flex name-age">
									<h5 class="card-title">Mentari</h5>
									<p>Age: <span>25</span></p>
								</div>
								<p class="text-capitalize country"><span>den haag</span> / belanda</p>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">indonesia</p>
								</div>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">photography</p>
								</div>
								<button type="button" class="btn btn-search-buddy hollow">See Details</button>
								<button type="button" class="btn btn-search-buddy">Book Me</button>
							</div>
						</div>
					</div>
					<div class="col-3">
						<div class="card">
							<div class="pp-wrapper">
								<img class="card-img-top" src="{{asset('img/buddy-img/Griyana2.jpg')}}" alt="Card image cap">
								<div class="price-buddy">
									<p class="rate">
										IDR 120 K
										<span class="dur">/jam</span>
									</p>
								</div>
							</div>
							<div class="card-body">
								<div class="div-flex name-age">
									<h5 class="card-title">Mentari</h5>
									<p>Age: <span>25</span></p>
								</div>
								<p class="text-capitalize country"><span>den haag</span> / belanda</p>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">indonesia</p>
								</div>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">photography</p>
								</div>
								<button type="button" class="btn btn-search-buddy hollow">See Details</button>
								<button type="button" class="btn btn-search-buddy">Book Me</button>
							</div>
						</div>
					</div>
					<div class="col-3">
						<div class="card">
							<div class="pp-wrapper">
								<img class="card-img-top" src="{{asset('img/buddy-img/Griyana2.jpg')}}" alt="Card image cap">
								<div class="price-buddy">
									<p class="rate">
										IDR 120 K
										<span class="dur">/jam</span>
									</p>
								</div>
							</div>
							<div class="card-body">
								<div class="div-flex name-age">
									<h5 class="card-title">Mentari</h5>
									<p>Age: <span>25</span></p>
								</div>
								<p class="text-capitalize country"><span>den haag</span> / belanda</p>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">indonesia</p>
								</div>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">photography</p>
								</div>
								<button type="button" class="btn btn-search-buddy hollow">See Details</button>
								<button type="button" class="btn btn-search-buddy">Book Me</button>
							</div>
						</div>
					</div>
					<div class="col-3">
						<div class="card">
							<div class="pp-wrapper">
								<img class="card-img-top" src="{{asset('img/buddy-img/Griyana2.jpg')}}" alt="Card image cap">
								<div class="price-buddy">
									<p class="rate">
										IDR 120 K
										<span class="dur">/jam</span>
									</p>
								</div>
							</div>
							<div class="card-body">
								<div class="div-flex name-age">
									<h5 class="card-title">Mentari</h5>
									<p>Age: <span>25</span></p>
								</div>
								<p class="text-capitalize country"><span>den haag</span> / belanda</p>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">indonesia</p>
								</div>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">photography</p>
								</div>
								<button type="button" class="btn btn-search-buddy hollow">See Details</button>
								<button type="button" class="btn btn-search-buddy">Book Me</button>
							</div>
						</div>
					</div>
					<div class="col-3">
						<div class="card">
							<div class="pp-wrapper">
								<img class="card-img-top" src="{{asset('img/buddy-img/Griyana2.jpg')}}" alt="Card image cap">
								<div class="price-buddy">
									<p class="rate">
										IDR 120 K
										<span class="dur">/jam</span>
									</p>
								</div>
							</div>
							<div class="card-body">
								<div class="div-flex name-age">
									<h5 class="card-title">Mentari</h5>
									<p>Age: <span>25</span></p>
								</div>
								<p class="text-capitalize country"><span>den haag</span> / belanda</p>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">indonesia</p>
								</div>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">photography</p>
								</div>
								<button type="button" class="btn btn-search-buddy hollow">See Details</button>
								<button type="button" class="btn btn-search-buddy">Book Me</button>
							</div>
						</div>
					</div>
					<div class="col-3">
						<div class="card">
							<div class="pp-wrapper">
								<img class="card-img-top" src="{{asset('img/buddy-img/Griyana2.jpg')}}" alt="Card image cap">
								<div class="price-buddy">
									<p class="rate">
										IDR 120 K
										<span class="dur">/jam</span>
									</p>
								</div>
							</div>
							<div class="card-body">
								<div class="div-flex name-age">
									<h5 class="card-title">Mentari</h5>
									<p>Age: <span>25</span></p>
								</div>
								<p class="text-capitalize country"><span>den haag</span> / belanda</p>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">indonesia</p>
								</div>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">photography</p>
								</div>
								<button type="button" class="btn btn-search-buddy hollow">See Details</button>
								<button type="button" class="btn btn-search-buddy">Book Me</button>
							</div>
						</div>
					</div>
					<div class="col-3">
						<div class="card">
							<div class="pp-wrapper">
								<img class="card-img-top" src="{{asset('img/buddy-img/Griyana2.jpg')}}" alt="Card image cap">
								<div class="price-buddy">
									<p class="rate">
										IDR 120 K
										<span class="dur">/jam</span>
									</p>
								</div>
							</div>
							<div class="card-body">
								<div class="div-flex name-age">
									<h5 class="card-title">Mentari</h5>
									<p>Age: <span>25</span></p>
								</div>
								<p class="text-capitalize country"><span>den haag</span> / belanda</p>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">indonesia</p>
								</div>
								<div class="div-flex abilities">
									<img src="{{asset('img/icon/find-black.svg')}}">
									<p class="text-capitalize">photography</p>
								</div>
								<button type="button" class="btn btn-search-buddy hollow">See Details</button>
								<button type="button" class="btn btn-search-buddy">Book Me</button>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
@endsection