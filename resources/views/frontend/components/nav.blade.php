<div class="tb-navbar__wrapper">
    <nav class="tb-navbar">
        <a class="tb-navbar__logo desktop" href="{{ url('/') }}">
            <img src="{{asset('img/logo.png')}}" alt="">
        </a>
        <a class="tb-navbar__logo mobile" href="{{ url('/') }}">
            <img src="{{asset('img/logo-mobile.png')}}" alt="">
        </a>
        @if (!active(['home', 'buddy.search']))
            <v-header-buddy-search
                action="{{ route('buddy.search') }}"
                default-value="{{ app('request')->input('location') }}"
                algolia-app-id="{{ env('ALGOLIA_APP_ID') }}"
                algolia-api-key="{{ env('ALGOLIA_PUBLIC') }}">
            </v-header-buddy-search>
        @endif
        <button
            class="tb-navbar__toggler toggler"
            type="button"
            data-target="#tbNavbarNav"
            aria-label="Toggle navigation">
            <span class="tb-navbar__toggler-icon"><i class="fa fa-align-justify"></i></span>
        </button>
        <div class="tb-navbar__nav-wrapper" id="tbNavbarNav" aria-expanded="false">
            <div class="tb-navbar__nav">
                <a class="tb-navbar__nav-link" href="{{ route('buddy.join') }}">Menjadi Buddy Kami</a>
                <a class="tb-navbar__nav-link" href="{{ url('about-us') }}">Tentang Kami</a>
                <a class="tb-navbar__nav-link" href="{{ route('help.menggunakan-jasa-kami') }}">Bantuan</a>
                @if (Auth::check())
                    <div class="tb-navbar__menu">
                        <div
                            class="tb-navbar__menu-icon toggler"
                            data-target="#tbProfileNav">
                            <img src="{{ Helper::getProfilePicture(Auth::user()) }}">
                        </div>
                        <div
                            id="tbProfileNav"
                            class="tb-navbar__menu__nav"
                            aria-expanded="false">
                            <a
                                href="{{ route('dashboard.home') }}"
                                class="tb-navbar__menu__nav-item">
                                Profil
                            </a>
                           
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            
                            <a 
                                class="tb-navbar__menu__nav-item"
                                href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                Sign Out
                            </a>
                        </div>
                    </div>
                @else
                    <a class="tb-navbar__nav-link" href="{{ route('login') }}">Sign In</a>
                    <a class="tb-navbar__nav-link" href="{{ route('register') }}">Register</a>
                @endif
            </div>
        </div>
    </nav>
</div>