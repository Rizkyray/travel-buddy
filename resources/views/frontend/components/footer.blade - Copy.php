<footer>
    <div class="to-top">
        <a class="to-top__link" href="#"></a>
    </div>
    <div class="sticky-hotline">
      <a class="sticky-hotline__link" target="_blank" href="https://api.whatsapp.com/send?phone=6287789136398"><i class="fa fa-comments"></i></a>
    </div>
    <div class="container">
    <div class="row">
        <div class="col-lg-8">
        <img src="{{asset('img/logo.png')}}" class="img-fluid logo">
        <div class="row">
            <div class="address col-md-6">
            <address>
                Jalan Pelita Abdul Majid No.5<br>
                Kebayoran Baru, Jakarta<br>
                Indonesia<br>
                12150<br>
            </address>
            </div>
            <div class="col-md-6">
            <ul class="about">
                <li><a href="{{ url('about-us') }}">Tentang Kami</a></li>
                <li><a href="{{ route('buddy.join') }}">Menjadi Buddy</a></li>
                <li><a href="{{ route('help.menggunakan-jasa-kami') }}">Bantuan</a></li>
                <li><a href="{{ route('help.faq') }}">FAQ</a></li>
                <li><a href="{{ url('syarat-dan-ketentuan') }}">Syarat dan Ketentuan</a></li>
            </ul>
            </div>
        </div>
        </div>
        <div class="col-lg-4">
        <p id="hotline">Hotline</p>
        <div class="card">
            <div class="card-body">
            <div class="div-flex align-items-center">
                <i class="fas fa-phone"></i>
                <a class="hotline-phone" href="https://api.whatsapp.com/send?phone=6287789136398"> +62 877 8913 6398</a>
            </div>
            <div class="div-flex align-items-center">
                <i class="fas fa-envelope"></i>
                <a class="hotline-email" href="mailto:support@travelbuddy.id"> support@travelbuddy.id</a>
            </div>
            </div>
        </div>
        </div>
    </div>
    <hr>
    </div>
    <div class="container" id="footer-bawah">
    <div class="row">
        <div class="copyright col-lg-4 order-3 order-lg-1">
        <p>Copyright <span>TravelBuddy</span> 2018. All rights reserved.</p>
        </div>
        <div class="col-lg-5 order-2">
        </div>
        <div class="col-lg-3 order-1 order-lg-3">
        <ul class="social-icon">
            <li>
                <a href="//www.facebook.com/travelbuddyindo" target="_blank">
                    <i class="fab fa-facebook-square"></i>
                </a>
            </li>
            <li>
                <a href="//www.instagram.com/travelbuddyindonesia/" target="_blank">
                    <i class="fab fa-instagram"></i>
                </a>
            </li>
            <li>
                <a href="//twitter.com/Travelbuddyid" target="_blank">
                    <i class="fab fa-twitter"></i>
                </a>
            </li>
            <li>
                <a href="//www.youtube.com/channel/UCRKM4bmp-loo6-8mGuFgXOg/featured" target="_blank">
                    <i class="fab fa-youtube"></i>
                </a>
            </li>
        </ul>
        </div>
    </div>
    </div>
</footer>