@php
    if (Session::has('flash-info')) {
        $flashClass = 'info';
        $content = Session::get('flash-info');
    }
    if (Session::has('flash-warning')) {
        $flashClass = 'warning';
        $content = Session::get('flash-warning');
    }
    if (Session::has('flash-danger')) {
        $flashClass = 'danger';
        $content = Session::get('flash-danger');
    }
@endphp

@if (Session::has('flash-info') || Session::has('flash-warning') || Session::has('flash-danger'))
    <div class="flash-message {{ $flashClass }}">
        <div class="flash-message__content">
            <a href="#" class="flash-message__close">
                <i class="fa fa-times"></i>
            </a>
            <div class="flash-message__body">
                <p>
                    {!! $content['message'] !!}
                </p>
            </div>
        </div>
    </div>
@endif