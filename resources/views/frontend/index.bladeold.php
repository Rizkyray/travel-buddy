<meta name="csrf_token" content="{{csrf_token()}}"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="theme-color" content="#ffffff">
	<title>Travel Buddy Indonesia</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	 <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link rel="icon" href="{{ url('img/favicon.ico') }}">

	
   
	
	<!-- css files -->
	<link rel="stylesheet" href="{{ url('/') . mix('css/app2.css') }}" media="screen,projection">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ url('/') . mix('css/bootstrap.css') }}" media="screen,projectiondd">
   <!-- custom css -->
    <link rel="stylesheet" href="{{ url('/') . mix('css/font.css') }}" media="screen,projection">
    <!-- fontawesome css -->
	<!-- //css files -->
	
	<link href="css/css_slider.css" type="text/css" rel="stylesheet" media="all">

	<!-- google fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- //google fonts -->
	
</head>
<body>

<!-- header -->
<header>
	<div class="container">
		<!-- nav -->
		<nav class="py-md-4 py-3 d-lg-flex">
			<div id="logo" style="margin-top: -8px;"> 
				<h1 class="mt-md-0 mt-2"> <a href="{{ url('/') }}"><img src="{{asset('img/logo.png')}}" alt=""></a></h1>
			</div>
			<label for="drop" class="toggle"><span class="fa fa-bars"></span></label>
			<input type="checkbox" id="drop" />
			<ul class="menu ml-auto mt-1">
				<!-- <li class="active"><a href="#">Home</a></li> -->
				<li class=""><a href="{{ url('about-us') }}">Tentang Kami</a></li>
				<li class=""><a href="{{ route('help.menggunakan-jasa-kami') }}">Bantuan</a></li>
			<li class=""><a href="{{ route('buddy.join') }}">Menjadi Buddy</a></li>
				 @if (Auth::check())
				 <li class=""><a href="{{ route('dashboard.home') }}">Dashboard</a></li>
				 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                           <li class="">
                            <a 
                                class=""
                                href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                Sign Out
                            </a>
                        </li>
				<!--  <div
                            class="tb-navbar__menu-icon toggler"
                            data-target="#tbProfileNav">
                            <img src="{{ Helper::getProfilePicture(Auth::user()) }}">
                        </div></li> -->
				@else
				<li class="booking"><a href="{{ route('login') }}">Login</a></li>
				@endif	
			</ul>
		</nav>
		<!-- //nav -->
	</div>
</header>
<!-- //header -->
<!-- banner -->
<section class="banner_w3lspvt" id="home">
	<div class="csslider infinity" id="slider1">
		<input type="radio" name="slides" checked="checked" id="slides_1" />
		<input type="radio" name="slides" id="slides_2" />
		<input type="radio" name="slides" id="slides_3" />
		<input type="radio" name="slides" id="slides_4" />
		<ul>
			<li>	
				<div class="banner-top">
					<div class="overlay">
						<div class="container">
							<div class="w3layouts-banner-info">
								<h3 class="text-wh">Pergi wisata dengan cita rasa orang Indonesia</h3>
								<h4 class="text-wh">Nikmati perjalanan seperti di rumah Anda sendiri</h4>
								<div class="buttons mt-4">
									<a href="{{ url('about-us') }}" class="btn mr-2">Tentang kami</a>
									<a href="https://travelbuddy.id/buddy/search" class="btn" style="background: #f7572f;">Cari Buddy</a>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="banner-top1">
					<div class="overlay">
						<div class="container">
							<div class="w3layouts-banner-info">
								<h3 class="text-wh">Pergi belanja dengan cita rasa orang Indonesia</h3>
								<h4 class="text-wh">Nikmati perjalanan seperti di rumah Anda sendiri</h4>
								<div class="buttons mt-4">
									<a href="{{ url('about-us') }}" class="btn mr-2">Tentang kami</a>
									<a href="{{ route('buddy.search') }}" class="btn">Cari Buddy</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="banner-top2">
					<div class="overlay">
						<div class="container">
							<div class="w3layouts-banner-info">
								<h3 class="text-wh">Pergi bisnis dengan cita rasa orang Indonesia</h3>
								<h4 class="text-wh">Nikmati perjalanan seperti di rumah Anda sendiri</h4>
								<div class="buttons mt-4">
									<a href="{{ url('about-us') }}" class="btn mr-2">Tentang kami</a>
									<a href="{{ route('buddy.search') }}" class="btn">Cari Buddy</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="banner-top3">
					<div class="overlay1">
						<div class="container">
							<div class="w3layouts-banner-info">
								<h3 class="text-wh">Pergi Belajar dengan cita rasa orang Indonesia</h3>
								<h4 class="text-wh">Nikmati perjalanan seperti di rumah Anda sendiri</h4>
								<div class="buttons mt-4">
									<a href="{{ url('about-us') }}" class="btn mr-2">Tentang kami</a>
									<a href="{{ route('buddy.search') }}" class="btn">Cari Buddy</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<div class="arrows">
			<label for="slides_1"></label>
			<label for="slides_2"></label>
			<label for="slides_3"></label>
			<label for="slides_4"></label>
		</div>
	</div>
</section>
<!-- //banner -->


<!-- about -->
<section class="about py-5">
	<div class="container py-lg-5 py-sm-4">
		<div class="row">
			<div class="col-lg-6 about-left">
				<h3 class="mt-lg-3">Wujudkan liburan idamanmu bersama kami sekarang, <strong>Lets explore with us!</strong></h3>
				<p class="mt-4">
				Kami mengerti bahwa momen liburan adalah waktu yang sangat berharga bagi Anda. Lupakan cara berpergian konvensional, Travelbuddy akan membantu memudahkan Anda mendapatkan pengalaman baru yang tidak terlupakan.</p>
				<p class="mt-3"> <bold>"Local Expert"</bold> kami atau disebut Buddy akan berbagi pengalaman dan juga rekomendasi tempat terbaik di negara tujuan Anda. Anda hanya perlu duduk manis dan menikmati perjalanan bersama Travelbuddy.</p>
			</div>
			<div class="col-lg-6 about-right text-lg-right mt-lg-0 mt-5">
				<img src="{{asset('img/images/kimono.jpeg')}}" alt="" class="img-fluid abt-image" />
			</div>
		</div>
		<div class="row mt-5 text-center">
			<!-- <div class="col-lg-3 col-6">
				<div class="counter">
					<span class="fa fa-smile-o"></span>
					<div class="timer count-title count-number">1000+</div>
					<p class="count-text text-uppercase">happy customers</p>
				</div>
			</div>
			<div class="col-lg-3 col-6">
				<div class="counter">
					<span class="fa fa-ship"></span>
					<div class="timer count-title count-number">2271</div>
					<p class="count-text text-uppercase">Tours & Travels </p>
				</div>
			</div>
			<div class="col-lg-3 col-6 mt-lg-0 mt-5">
				<div class="counter">
					<span class="fa fa-users"></span>
					<div class="timer count-title count-number">200</div>
					<p class="count-text text-uppercase">destinations</p>
				</div>
			</div>
			<div class="col-lg-3 col-6 mt-lg-0 mt-5">
				<div class="counter">
					<span class="fa fa-gift"></span>
					<div class="timer count-title count-number">20+<span>years</span></div>
					<p class="count-text text-uppercase">experience</p>
				</div> -->
			</div>
		</div>
	</div>
</section>
<!-- //about -->

<!-- how to book -->
<section class="book py-5">
	<div class="container1 py-lg-5 py-sm-3">
		<h2 class="heading text-capitalize text-center"> How To Plan Your Trip</h2>
		<div class="row mt-5 text-center">
			<div class="col-lg-4 col-sm-6">
				<div class="grid-info">
					<div class="icon">
						<span class="fa fa-search"></span>
					</div>
					<h4>Temukan</h4>
					<p class="mt-3">Temukan tujuan dan Buddy pilihan kamu</p>
				</div>
			</div>
			<div class="col-lg-4 col-sm-6 mt-sm-0 mt-5">
				<div class="grid-info">
					<div class="icon">
						<span class="fa fa-hand-o-up"></span>
					</div>
					<h4>Rencanakan</h4>
					<p class="mt-3">Rencanakan Liburan Anda bersama Kami dan dapatkan rekomendasi terbaik dari "Local Expertise" kami</p>
				</div>
			</div>
			<div class="col-lg-4 col-sm-6 mt-lg-0 mt-5">
				<div class="grid-info">
					<div class="icon">
						<span class="fa fa-glass"></span>
					</div>
					<h4>Berangkat</h4>
					<p class="mt-3">Nikmati perjalanan dan pengalaman bersama Buddy terbaik kami</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- //how to book -->

<!-- Alasan -->
<section class="packages py-5">
	<div class="container py-lg-4 py-sm-3">
		<h3 class="heading text-capitalize text-center"> Mengapa TRAVELBUDDY</h3>
		<p class="text mt-2 mb-5 text-center">Karena kami mengerti momen Liburan Anda sangatlah berharga. Kami akan membantu memudahkan serta mewujudkan liburan idaman Anda.</p>
		<div class="row">
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center" style="margin-top: 0px;margin-bottom: 19px;">
				<div class="image-tour position-relative">
					<img src="{{asset('img/images/speak-bahasa.jpg')}}" alt="" class="img-fluid" />
					<p><span class="fa fa-hashtag"></span> <span style="font-weight: bold;">1	</span></p>
				</div>
				<div class="package-info">
					
					<h5 class="my-2">Speak in Bahasa</h5>
					<p class="">Mudahnya berkomunikasi dengan guide mu, cukup menggunakan Bahasa Indonesia.</p>
					<ul class="listing mt-3">
						<li></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center" style="margin-top: 0px;margin-bottom: 19px;">
				<div class="image-tour position-relative">
					<img src="{{asset('img/images/familiarity.jpg')}}" alt="" class="img-fluid" />
					<p><span class="fa fa-hashtag"></span> <span style="font-weight: bold;">2</span></p>
				</div>
				<div class="package-info" style="height: 203px;">
					
					<h5 class="my-2">Familiarity</h5>
					<p class="">Menjelajah bersama guide yang sudah akrab dengan destinasi kota tujuanmu.</p>
					<ul class="listing mt-3">
						<li></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center" style="margin-bottom: 19px;">
				<div class="image-tour position-relative">
					<img src="{{asset('img/images/customer-based.jpg')}}" alt="" class="img-fluid" />
					<p><span class="fa fa-hashtag"></span> <span style="font-weight: bold;">3</span></p>
				</div>
				<div class="package-info">
					
					
					<h5 class="my-2">Customer-based tour</h5>
					<p class="">Tentukan perjalanan dan kunjungi semua tempat sesuai dengan keinginanmu.</p>
					<ul class="listing mt-3">
						<li></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center" style="margin-top: 0px;margin-bottom: 19px;">
				<div class="image-tour position-relative">
					<img src="{{asset('img/images/flexible.jpg')}}" alt="" class="img-fluid" />
					<p><span class="fa fa-hashtag"></span> <span style="font-weight: bold;">4</span></p>
				</div>
				<div class="package-info" style="height: 203px;">
					
					<h5 class="my-2">Flexible</h5>
					<p class="">Jam dan waktu dapat menyesuaikan kebutuhanmu.</p>
					<ul class="listing mt-3">
						<li></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center" style="margin-bottom: 19px;">
				<div class="image-tour position-relative">
					<img src="{{asset('img/images/inspiring.jpg')}}" alt="" class="img-fluid" />
					<p><span class="fa fa-hashtag"></span> <span style="font-weight: bold;">5</span></p>
				</div>
				<div class="package-info" style="height: 181px;">
					
					<h5 class="my-2">Inspiring</h5>
					<p class="">Bawa pulang pengalaman jalan-jalan yang paling berkesan!.</p>
					<ul class="listing mt-3">
						<li></li>
					</ul>
				</div>
			</div>
				<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center" style="">
				<!-- <div class="image-tour position-relative"> -->
				<!-- <img src="//stagingtb.travelbuddy.id/img/images/logo.png" alt="" class="img-fluid" style="margin-top: 89px;">		 -->
				<!-- </div> -->
				<!-- <div class="package-info" style="height: 181px;">
					
					
					<h5 class="my-2">Cari Buddy</h5>
					<p class="">Langsung Cari Buddy Untuk Menemanimu Traveling.</p>
					<ul class="listing mt-3">
						<li></li>
					</ul>
				</div> -->
			</div>
		</div>
		
	</div>
</section>
<!-- Alasan -->

<!-- tour packages -->
<!-- <section class="packages py-5">
	<div class="container py-lg-4 py-sm-3">
		<h3 class="heading text-capitalize text-center"> Discover our tour packages</h3>
		<p class="text mt-2 mb-5 text-center">Vestibulum tellus neque, sodales vel mauris at, rhoncus finibus augue. Vestibulum urna ligula, molestie at ante ut, finibus vulputate felis.</p>
		<div class="row">
			<div class="col-lg-3 col-sm-6">
				<div class="image-tour position-relative">
					<img src="{{asset('img/images/p1.jpg')}}" alt="" class="img-fluid" />
					<p><span class="fa fa-tags"></span> <span>20$</span></p>
				</div>
				<div class="package-info">
					<h6 class="mt-1"><span class="fa fa-map-marker mr-2"></span>Paris, France.</h6>
					<h5 class="my-2">Sodales vel mauris</h5>
					<p class="">Vestibulum tellus neque, et velit mauris at, augue.</p>
					<ul class="listing mt-3">
						<li><span class="fa fa-clock-o mr-2"></span>Duration : <span>10 Days</span></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6">
				<div class="image-tour position-relative">
					<img src="{{asset('img/images/p2.jpg')}}" alt="" class="img-fluid" />
					<p><span class="fa fa-tags"></span> <span>20$</span></p>
				</div>
				<div class="package-info">
					<h6 class="mt-1"><span class="fa fa-map-marker mr-2"></span>Los Angles, USA.</h6>
					<h5 class="my-2">Sodales vel mauris</h5>
					<p class="">Vestibulum tellus neque, et velit mauris at, augue.</p>
					<ul class="listing mt-3">
						<li><span class="fa fa-clock-o mr-2"></span>Duration : <span>10 Days</span></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 mt-lg-0 mt-5">
				<div class="image-tour position-relative">
					<img src="{{asset('img/images/p3.jpg')}}" alt="" class="img-fluid" />
					<p><span class="fa fa-tags"></span> <span>20$</span></p>
				</div>
				<div class="package-info">
					<h6 class="mt-1"><span class="fa fa-map-marker mr-2"></span>Agra, India.</h6>
					<h5 class="my-2">Sodales vel mauris</h5>
					<p class="">Vestibulum tellus neque, et velit mauris at, augue.</p>
					<ul class="listing mt-3">
						<li><span class="fa fa-clock-o mr-2"></span>Duration : <span>10 Days</span></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 mt-lg-0 mt-5">
				<div class="image-tour position-relative">
					<img src="{{asset('img/images/p4.jpg')}}" alt="" class="img-fluid" />
					<p><span class="fa fa-tags"></span> <span>20$</span></p>
				</div>
				<div class="package-info">
					<h6 class="mt-1"><span class="fa fa-map-marker mr-2"></span>Paris, France.</h6>
					<h5 class="my-2">Sodales vel mauris</h5>
					<p class="">Vestibulum tellus neque, et velit mauris at, augue.</p>
					<ul class="listing mt-3">
						<li><span class="fa fa-clock-o mr-2"></span>Duration : <span>10 Days</span></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="view-package text-center mt-4">
			<a href="packages.html">View All Packages</a>
		</div>
	</div>
</section> -->
<!-- tour packages -->

<!-- text -->
<section class="text-content">
	<div class="overlay-inner py-5">
		<div class="container py-md-3">
			<div class="test-info">
				<h4 class="tittle">Nikmati Perjalanan Anda</h4>
				<p class="mt-3">Ingin wisata, belanja, bisnis hingga belajar? Waktu Anda sangatlah berharga bagi kami. Travelbuddy akan dengan senang hati membantu memudahkan serta mewujudkan liburan idaman Anda.</p>
				<div class="text-left mt-4">
						<!-- <a href="booking.html">Book Now</a> -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- //text -->
	
<!-- destinations -->
<section class="destinations py-5" id="destinations">
	<div class="container py-xl-5 py-lg-3">
		<h3 class="heading text-capitalize text-center"> Destinasi Terkenal</h3>
		<!-- <p class="text mt-2 mb-5 text-center">Vestibulum tellus neque, sodales vel mauris at, rhoncus finibus augue. Vestibulum urna ligula, molestie at ante ut, finibus vulputate felis.</p> -->
		<div class="row inner-sec-w3layouts-w3pvt-lauinfo">
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center">
				<h4 class="destination mb-3">China</h4>
				<div class="image-position position-relative">
					<img src="{{asset('img/images/china.jpg')}}" class="img-fluid" alt="">
					<div class="rating">
						<ul>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
						</ul>
					</div>
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>China</h4>
						<a href="https://travelbuddy.id/buddy/search?location=china">Cari Buddy</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center">
				<h4 class="destination mb-3">Malaysia</h4>
				<div class="image-position position-relative">
					<img src="{{asset('img/images/malaysia.jpg')}}" class="img-fluid" alt="">
					<div class="rating">
						<ul>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
						</ul>
					</div>
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>Malaysia</h4>
						<a href="https://travelbuddy.id/buddy/search?location=malaysia">Cari Buddy</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center mt-md-0 mt-4">
				<h4 class="destination mb-3">Japan</h4>
				<div class="image-position position-relative">
					<img src="{{asset('img/images/japan.jpg')}}" class="img-fluid" alt="">
					<div class="rating">
						<ul>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
						</ul>
					</div>
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>Japan</h4>
						<a href="http://stagingtb.travelbuddy.id/buddy/search?location=japan">Cari Buddy</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center mt-md-0 mt-4">
				<h4 class="destination mb-3">Singapore</h4>
				<div class="image-position position-relative">
					<img src="{{asset('img/images/singapore.jpg')}}" class="img-fluid" alt="">
					<div class="rating">
						<ul>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
							<li><span class="fa fa-star"></span></li>
						</ul>
					</div>
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>Singapore</h4>
						<a href="https://travelbuddy.id/buddy/search?location=singapore">Cari Buddy</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- destinations -->

<!--footer -->
<footer>
<section class="footer footer_w3layouts_section_1its py-5">
	<div class="container py-lg-4 py-3">
		<div class="row footer-top">
			<div class="col-lg-3 col-sm-6 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Alamat</h3>
				</div>
				<div class="footer-text">
					<p>Address : <br>
					Jalan Pelita Abdul Majid No.5<br>
					Kebayoran Baru, Jakarta 12150<br>
					Indonesia
					</p>
					<p>Phone : <br>
						<a href="https://api.whatsapp.com/send?phone=6287789136398">+62 87789136398</a></p>
					<p>Email : <a href="mailto:support@travelbuddy.id">support@travelbuddy.id</a></p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 footer-grid_section mt-sm-0 mt-4">
				<div class="footer-title">
					<h3>Tentang Kami</h3>
				</div>
				<div class="footer-text">
					<p>Travel Buddy adalah sebuah platform yang mempertemukan Anda dengan orang Indonesia di negara tujuan yang akan menemani selama perjalanan.</p>
				</div>
				<ul class="social_section_1info">
					<li class="mb-2 facebook"><a href="#"><span class="fa fa-facebook"></span></a></li>
					<li class="mb-2 twitter"><a href="#"><span class="fa fa-twitter"></span></a></li>
					<li class="google"><a href="#"><span class="fa fa-google-plus"></span></a></li>
					<li class="linkedin"><a href="#"><span class="fa fa-linkedin"></span></a></li>
				</ul>
			</div>
			<div class="col-lg-3 col-sm-6 mt-lg-0 mt-4 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Travel Places</h3>
				</div>
				<div class="row">
					<ul class="col-6 links">
						<li><a href="#choose" class="scroll">New Zealand </a></li>
						<li><a href="#overview" class="scroll">Paris, France </a></li>
						<li><a href="#pricing" class="scroll">Los Angles</a></li>
						<li><a href="#faq" class="scroll"> Darlington</a></li>
						<li><a href="#testimonials" class="scroll">Canada </a></li>
						<li><a href="#contact" class="scroll"> South Africa </a></li>
					</ul>
					<ul class="col-6 links">
						<li><a href="#">Spain </a></li>
						<li><a href="#">Turkey </a></li>
						<li><a href="#faq" class="scroll">Europe </a></li>
						<li><a href="#">Italy </a></li>
						<li><a href="#">Sweden </a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 mt-lg-0 mt-4 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Newsletter</h3>
				</div>
				<div class="footer-text">
					<p>By subscribing to our mailing list you will always get latest news and updates from us.</p>
					<form action="#" method="post">
						<input type="email" name="Email" placeholder="Enter your email..." required="">
						<button class="btn1"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
						<div class="clearfix"> </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</footer>
<!-- //footer -->

<!-- copyright -->
<div class="copyright py-3 text-center">
	<p>© 2019 Travel Buddy Indonesia. All Rights Reserved 
</div>
<!-- //copyright -->

<!-- move top -->
<div class="move-top text-right">
	<a href="#home" class="move-top"> 
		<span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
	</a>
</div>
<!-- move top -->

	
</body>
</html>