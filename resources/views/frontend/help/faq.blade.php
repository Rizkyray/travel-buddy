@extends('frontend.layouts.main')

@section('body-attribute')
data-target="#help-nav"
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3 sidebar-wrapper">
            @include('frontend.help.components.sidebar')
        </div>
        <div class="col-md-8">
            <div class="content post">
                <p class="help__title">Frequently Asked Question</p>
                <h1 id="FAQ" class="heading-1">FAQ</h1>

                <h2 id="booking" class="heading-2">Booking</h2>
                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="tb-accordion__title heading-4">Bagaimana cara memesan Buddy</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>
                            Untuk pemesanan, Anda terlebih dahulu memilih kota destinasi perjalanan. Setelah itu akan ditampilkan daftar Buddy yang dapat Anda pilih. Anda dapat mengklik Book Now pada Buddy yang dipilih, kemudian mengisi detail perjalanan Anda. Setelah menerima pembayaran, kami akan mengirimkan konfirmasi email kepada Anda.
                            </p>
                            <ul>
                                <li>Telepon : <a href="https://api.whatsapp.com/send?phone=6287789136398">+6287789136398</a></li>
                                <li>DM Instagram: <a href="https//www.instagram.com/travelbuddyindonesia/">@travelbuddyindonesia</a></li>
                                <li>Email: <a href="mailto:support@travelbuddy.id">support@travelbuddy.id</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Seberapa cepat saya bisa memesan Buddy sebelum sesi perjalanan dimulai?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>
                                Kami menyarankan Anda untuk memesan Buddy paling tidak seminggu sebelum keberangkatan. Akan tetapi, kami tetap akan melayani pemesanan last-minute.
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Berapa lama satu sesi perjalanan dan berapa harganya?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>
                                Sesi perjalanan Anda sangatlah fleksibel. Anda dapat mengatur jumlah dan lama sesi sesuai dari kebutuhan Anda. Harga mulai dari IDR 60,000 per jam. 
                            </p>
                        </div>
                    </div>
                </div>

                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Berapa banyak kolega atau keluarga yang bisa saya ajak untuk satu sesi perjalanan?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Jumlah kolega atau keluarga dapat menentukan kualitas perjalanan Anda. Akan tetapi, kami merekomendasi untuk jumlah optimal yaitu 6-7 orang.</p>
                        </div>
                    </div>
                </div>

                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Dapatkah saya memilih Buddy saya sendiri?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Tentu saja! Silahkan tentukan kota destinasi Anda terlebih dahulu.</p>
                        </div>
                    </div>
                </div>

                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Apa yang akan saya dapatkan dalam satu sesi?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Satu sesi termasuk:
                            </p>
                            <ul>
                                <li>Satu Buddy </li>
                                <li>Sesi yang sesuai permintaan Anda</li>
                                <li>Pengetahuan lokal</li>
                                <li>Konsultasi rancangan perjalanan dengan pihak Travel Buddy</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Apakah ada biaya tambahan lain yang harus saya siapkan untuk Buddy dalam satu sesi?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Harga yang Anda bayarkan sepenuhnya untuk membayar jasa Buddy. Oleh karenanya, biaya-biaya tambahan untuk Buddy dalam sesi perjalanan akan dibebankan kepada Anda, seperti:
                            </p>
                            <ul>
                                <li>Biaya transportasi untuk Buddy, baik itu dalam ataupun luar kota</li>
                                <li>Biaya masuk ke tempat wisata</li>
                                <li>Makan siang/malam (Kami mewajibkan kepada Traveler agar memberikan satu porsi makanan kepada Buddy untuk pemesanan sesi 5 jam dan kelipatannya)</li>
                                <li>Akomodasi (jika diperlukan)</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Bisakah saya mengganti jadwal?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Untuk penjadwalan ulang, paling tidak memberitahukan 3 hari sebelum sesi perjalanan dimulai melalui email atau telfon. Penggantian jadwal tidak akan dikenakan biaya tambahan; namun jika Anda kemudian melakukan pembatalan, maka perhitungan Regulasi Pengembalian Uang akan berlaku sesuai jadwal pertama Anda.</p>
                        </div>
                    </div>
                </div>

                <h2 id="pembatalan" class="heading-2">Pembatalan dan Regulasi Pengembalian Uang</h2>
                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Bisakah saya membatalkan pesanan saya?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Kami mengerti perubahan jadwal adalah hal yang wajar, oleh karenanya Anda dapat membatalkan pesanan dengan memberikan konfirmasi kepada Travel Buddy melalui email, telefon, atau SMS paling tidak 3 hari sebelum sesi perjalanan. Akan tetapi, perlu diingat bahwa:
                            </p>
                            <ul>
                                <li>Jika pembatalan dilakukan di luar kurun waktu 14x24 jam sebelum sesi perjalanan, anda mendapatkan 90% refund dari total pembayaran</li>
                                <li>Jika pembatalan dilakukan di luar kurun waktu 7x24 jam sebelum sesi perjalanan, anda mendapatkan 50% refund dari total pembayaran</li>
                                <li>Jika pembatalan dilakukan di luar kurun waktu 3x24 jam sebelum sesi perjalanan, anda mendapatkan  20% refund dari total pembayaran</li>
                                <li>Jika pembatalan dilakukan dalam kurun waktu 3x24 jam  sebelum sesi perjalanan, tidak ada pengembalian uang</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Bagaimanakan tata cara pengembalian uang?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Anda akan mendapatkan pengembalian uang setelah ada konfirmasi mengenai pembatalan tersebut. Kami akan menyelesaikan pengembalian uang  tidak lebih dari 5 hari kerja melalui transfer Bank. Jika ada biaya transfer, maka biaya tersebut akan dibebankan kepada Anda</p>
                        </div>
                    </div>
                </div>

                <h2 id="cara-pembayaran" class="heading-2">Tata Cara Pembayaran</h2>
                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Apa sajakah pilihan metode pembayaran yang disediakan?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Kami memiliki beberapa metode pembayaran untuk kemudahan anda, seperti Bank BCA, Jenius BTPN, dan Rabobank (khusus pembayaran dalam bentuk Euro).</p>
                        </div>
                    </div>
                </div>

                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Kapan waktu yang tepat untuk menyelesaikan pembayaran saya?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Setelah anda menerima invoice pembayaran, anda memiliki 5 hari (tidak melewati hari saat sesi perjalanan dimulai) untuk menyelesaikan pembayaran agar Buddy tersedia untuk Anda. </p>
                        </div>
                    </div>
                </div>

                <h2 id="perjalanan" class="heading-2">Masa Perjalanan</h2>
                <div class="tb-accordion">
                    <div class="tb-accordion__header tb-accordion__toggler">
                        <div class="tb-accordion__expand"><i class="fa fa-angle-down"></i></div>
                        <h3 class="heading-4 tb-accordion__title">Bagaimana jika saya terlambat datang?</h3>
                    </div>
                    <div class="tb-accordion__content">
                        <div class="tb-accordion__body">
                            <p>Segera beritahu Buddy secepatnya. Mohon diingat bahwa  Buddy mungkin memiliki  agenda lain. Kami menyarankan Anda untuk datang 15 menit sebelum sesi dimulai untuk menghindari hal-hal yang tidak diinginkan
                            </p>
                            <ul>
                                <li>Jika Anda terlambat dan waktu sesi sudah mulai, maka sesi tersebut akan berjalan dengan waktu yang tersisa. </li>
                                <li>Jika Buddy terlambat, maka sesi akan berjalan sesaat setelah pertemuan Anda dengan Buddy.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection