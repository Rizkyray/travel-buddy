@extends('frontend.layouts.main')

@section('body-attribute')
data-target="#help-nav"
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-3 sidebar-wrapper">
                @include ('frontend.help.components.sidebar')
            </div>
            <div class="col-lg-8">
                <div class="content post">
                    <h1 id="how-it-works" class="help__title">Menggunakan Jasa Kami</h1>
                    
                    <h2 id="alur-penggunaan" class="heading-1">Alur Penggunaan</h2>
                    <img src="{{ asset('img/help/alur-penggunaan.jpg')  }}" alt="Alur penggunaan travel buddy">
                    <hr>
                    
                    <h2 id="booking-buddy" class="heading-1">Booking Buddy</h2>
                    <div class="steps">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="step">
                                    <img src="{{ asset('img/help/step-1.png') }}" alt="">
                                    <span class="step__num">1</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="step">
                                    <img src="{{ asset('img/help/step-2.png') }}" alt="">
                                    <span class="step__num">2</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="step">
                                    <img src="{{ asset('img/help/step-3.png') }}" alt="">
                                    <span class="step__num">3</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="step">
                                    <img src="{{ asset('img/help/step-4.png') }}" alt="">
                                    <span class="step__num">4</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="step">
                                    <img src="{{ asset('img/help/step-5.png') }}" alt="">
                                    <span class="step__num">5</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <h2 id="pembayaran" class="heading-1">Metode Pembayaran</h2>
                    <ol>
                        <li>Setelah request guide, anda akan menerima email konfirmasi order dan customer invoice. Pastikan rincian biaya sudah sesuai</li>
                        <li>Segera lakukan transfer ke salah satu rekening bank berikut dan sertakan nomor order di berita transfer</li>
                        
                        <ul class="bank-list">
                            <li class="bank-list__item">
                                <img src="{{ asset('img/banks/bca.png') }}" alt="BCA" class="bank__logo">
                                <div class="bank__body">
                                    <p class="bank__title">BCA</p>
                                    <p class="bank__info">A.N. Rizky Sarakhsi Ersaid Lasabuda<br>No. Rekening: 6041758349</p>
                                </div>
                            </li>
                            <li class="bank-list__item">
                                <img src="{{ asset('img/banks/rabobank.png') }}" alt="Rabobank" class="bank__logo">
                                <div class="bank__body">
                                    <p class="bank__title">Rabobank</p>
                                    <p class="bank__info">
                                        A.N. RSE LASABUDA<br>
                                        IBAN: NL58 RABO 166376612<br>
                                        BIC: RABONL2U<br>
                                    </p>
                                </div>
                            </li>
                            <li class="bank-list__item">
                                <img src="{{ asset('img/banks/jenius.png') }}" alt="Jenius" class="bank__logo">
                                <div class="bank__body">
                                    <p class="bank__title">Jenius</p>
                                    <p class="bank__info">
                                        A.N. Rizky Sarakhsi Ersaid Lasabuda<br>
                                        No. Rekening: 90011539249<br>
                                        $rizkylasabuda<br>
                                    </p>
                                </div>
                            </li>
                        </ul>
                        <li>Setelah melakukan pembayaran harap konfirmasi melalui
                            <ul>
                                <li>email ke billing@travelbuddy.id</li>
                                <li>atau telpon ke 087789136398</li>
                            </ul>
                        </li>
                        <li>Harap melakukan pembayaran tidak lebih dari 5 hari setelah invoice dikirim. Jika anda mengalami kesulitan harap menghubungi customer service kami.</li>
                    </ol>
                    <hr>
                </div>
            </div>
        </div>
    </div>
@endsection