<div class="sidebar">
    <div class="heading">
        <h1 class="heading__title">Informasi</h1>
        <p class="heading__subtitle">Segala informasi yang berkaitan dengan Travel Buddy ada di sini</p>
    </div>
    <nav id="help-nav">
        <nav class="nav nav-pills vertical-nav">
            <a
                class="nav-link nav-link-header vertical-nav__title {{ active('help.menggunakan-jasa-kami', 'vertical-nav__title--active')}}"
                href="{{ route('help.menggunakan-jasa-kami') }}">
                Menggunakan Jasa Kami
            </a>
            <nav class="nav vertical-nav">
                <ul>
                    <li>
                        <a
                            class="nav-link"
                            data-target="#alur-penggunaan"
                            href="{{ route('help.menggunakan-jasa-kami') }}#alur-penggunaan">
                            Alur Penggunaan
                        </a>
                    </li>
                    <li>
                        <a
                            class="nav-link"
                            data-target="#booking-buddy"
                            href="{{ route('help.menggunakan-jasa-kami') }}#booking-buddy">
                            Booking Buddy
                        </a>
                    </li>
                    <li>
                        <a
                            class="nav-link"
                            data-target="#pembayaran"
                            href="{{ route('help.menggunakan-jasa-kami') }}#pembayaran">
                            Pembayaran
                        </a>
                    </li>
                </ul>
            </nav>
        </nav>
        <nav class="nav nav-pills vertical-nav">
            <a
                class="nav-link nav-link-header vertical-nav__title {{ active('help.faq', 'vertical-nav__title--active') }}"
                href="{{ route('help.faq') }}">
                FAQ
            </a>
            <nav class="nav vertical-nav">
                <ul>
                    <li>
                        <a
                            class="nav-link"
                            data-target="#booking"
                            href="{{ route('help.faq') }}#booking">
                            Booking
                        </a>
                    </li>
                    <li>
                        <a
                            class="nav-link"
                            data-target="#pembatalan"
                            href="{{ route('help.faq') }}#pembatalan">
                            Pembatalan
                        </a>
                    </li>
                    <li>
                        <a
                            class="nav-link"
                            data-target="#cara-pembayaran"
                            href="{{ route('help.faq') }}#cara-pembayaran">
                            Cara Pembayaran
                        </a>
                    </li>
                    <li>
                        <a 
                            class="nav-link"
                            data-target="#perjalanan"
                            href="{{ route('help.faq') }}#perjalanan">
                            Perjalanan
                        </a>
                    </li>
                </ul>
            </nav>
        </nav>
    </nav>
</div>