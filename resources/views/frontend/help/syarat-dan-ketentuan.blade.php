@extends('frontend.layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3 sidebar-wrapper">
        <div class="sidebar">
            <div class="heading">
                <h1 class="heading__title">Syarat dan Ketentuan</h1>
            </div>
        </div>
        </div>
        <div class="col-md-8">
            <div class="content post">
                <h1 class="heading-1">Syarat dan Ketentuan</h1>
                <p>
                    Berikut ini adalah Syarat dan Ketentuan (S&K) yang berlaku kepada para pengguna TRAVEL BUDDY. Syarat & ketentuan adalah perjanjian antara Pengguna dan TRAVEL BUDDY yang berisikan seperangkat peraturan yang mengatur kewajiban, hak, tanggung jawab pengguna dan TRAVEL BUDDY, serta tata cara penggunaan sistem layanan TRAVEL BUDDY.
                </p>
                <p>
                    Dengan menyelesaikan pemesanan di TRAVEL BUDDY, Anda tunduk kepada S&K dibawah ini. TRAVEL BUDDY memiliki hak untuk mengubah sebagian atau seluruh S&K. Dengan demikian, S&K yang berlaku di masa depan dapat berbeda dengan kondisi S&K saat ini. TRAVEL BUDDY tidak memiliki kewajiban untuk menginformasi perubahan ini. 
                </p>
                <p>
                    Melindungi privasi dan informasi krusial pelanggan adalah prioritas utama kami. S&K dibawah ini juga merangkap kebijakan privasi terkait pengumpulan data dan penggunaan data dilakukan oleh TRAVEL BUDDY, termasuk melalui website kami. Dengan menggunakan website kami, Anda secara sadar dan menyetujui apa yang telah diatur secara keseluruhan dalam S&K ini:
                </p>

                <strong>1. Definisi dan Ketentuan Layanan</strong>
                <p>
                TRAVEL BUDDY adalah pihak yang menjalankan kegiatan usaha jasa web portal pada <a href="{{ route('home') }}">travelbuddy.id</a> , sebuah website yang berperan sebagai perantara untuk mempertemukan antara pengguna TRAVEL BUDDY di seluruh dunia. Pengguna adalah semua pihak yang menggunakan dan memanfaatkan layanan TRAVEL BUDDY, termasuk namun tidak terbatas pada TRAVELER, BUDDY, ataupun pihak lain yang sekedar berkunjung ke Situs TRAVEL BUDDY. 
                </p>
                <p>
                    TRAVELER adalah pengguna terdaftar yang telah menyelesaikan langkah-langkah pemesanan pada Situs, Aplikasi, atau Layanan, atau seorang Pengguna yang mengontak langsung TRAVEL BUDDY untuk menggunakan jasa Sesi Perjalanan dan membayar sejumlah uang berdasarkan harga yang tertera.
                </p>
                <p>
                    BUDDY adalah pengguna yang terdaftar yang melakukan tindakan promosi untuk memberikan jasa berupa Sesi Perjalanan kepada TRAVELER untuk mendapatkan uang atas imbalannya yang telah disetujui atas perjanjian lewat Situs, Aplikasi, atau Layanan.
                </p>
                <p>
                    Sesi Perjalanan adalah waktu pemesanan yang telah disepakati oleh TRAVELER dan BUDDY atas perjanjian melalui Situs, Aplikasi, atau Layanan.
                </p>
                <p>
                    TRAVEL BUDDY tidak bertanggung jawab terhadap seluruh kerugian yang menimpa TRAVELER dan BUDDY selama proses layanan ini berlangsung
                </p>

                <strong>2. Pendaftaran</strong>
                <p>
                Pengguna jasa TRAVEL BUDDY diwajibkan untuk mengisi formulir yang disediakan secara benar dan akurat. Informasi pengguna yang diberikan hanya akan digunakan untuk keperluan dan database pihak TRAVEL BUDDY. TRAVEL BUDDY menjamin kerahasiaan data-data dari Pengguna. TRAVELER dan BUDDY dapat mengubah data diri setelah verifikasi akun oleh TRAVEL BUDDY. 
                </p>

                <strong>3. Biaya Perjalanan</strong>
                <p>BUDDY adalah orang Indonesia yang berada di kota tujuan wisata. Oleh karenanya, tidak diperlukan biaya perjalanan ataupun biaya akomodasi. Namun, jika dalam sesi perjalanan dibutuhkan biaya lain seperti transportasi umum, maka biaya tersebut akan dibebankan kepada TRAVELER. Jika TRAVELER ingin dipandu ke daerah terpencil atau kota lain, kemungkinan akan ada biaya tambahan lain. Kami harap Anda untuk melakukan konsultasi dengan Support Team sebelum melakukan pemesanan.</p>
                
                <strong>4. Biaya Lainnya</strong>
                <p>Biaya lainnya adalah biaya yang belum diatur dan yang dapat timbul selama Sesi Perjalanan berlangsung sebagai contoh biaya untuk masuk ke tempat wisata, apabila TRAVELER memerlukan jasa dari BUDDY untuk menemani TRAVELER masuk ke suatu tempat wisata, maka TRAVELER akan menanggung biaya masuk dari BUDDY. Namun, TRAVELER juga memiliki pilihan untuk dapat memasuki tempat wisata tersebut tanpa ditemani oleh BUDDY, akan tetapi, waktu menunggu BUDDY akan tetap dihitung dalam sesi perjalanan tersebut.</p>

                <p>TRAVEL BUDDY mewajibkan TRAVELER untuk memberikan satu porsi makanan kepada BUDDY untuk pemesanan sesi 5 jam dan kelipatannya.</p>

                <strong>5. Waktu Pemesanan</strong>
                <p>TRAVEL BUDDY bekerja sebagai penyedia jasa untuk menggunakan jasa dari BUDDY. Dengan memesan jasa dari BUDDY, sebagai contoh dari jam 9 pagi sampai 6 sore, TRAVELER harus patuh pada slot waktu tersebut. Jikalau TRAVELER ternyata ingin menambah waktu pemesanan pada saat Sesi Perjalanan berlangsung, silahkan konsultasi dengan BUDDY dan pastikan untuk melapor ke TRAVEL BUDDY. Jika TRAVELER terlambat datang ke tempat janjian, maka waktu akan berjalan sesuai dengan pesanan awal TRAVELER. Sebaliknya, jika BUDDY terlambat datang ke tempat janjian, maka waktu akan berjalan setelah pertemuan berlangsung.</p>

                <strong>6. Cara Pemesanan</strong>
                <p>TRAVELER dapat memilih BUDDY yang ditampikan pada website travelbuddy.id untuk melakukan pemesanan. Setelah pemesanan berhasil, TRAVEL BUDDY akan menghubungi TRAVELER untuk mengkonfirmasi terkait pemesanan dan kesepakatan lainnya (Misal: cara membayar). Jika TRAVELER ingin membatalkan pemesanan, harap untuk melihat poin #10 mengenai  pengembalian uang.</p>

                <strong>7. Cara Pembayaran</strong>
                <p>Pembayaran harap dilakukan secepatnya dengan masa tenggat 5 hari setelah pemesanan berhasil. Pembayaran dapat dilakukan dengan tujuan rekening milikTRAVEL BUDDY, antara lain melalui Bank BCA, Jenius BTPN, atau RABOBANK (khusus pembayaran dalam Euro).</p>

                <strong>8. Masa Perjalanan</strong>
                <p>Selama masa perjalanan, TRAVELER dan BUDDY diwajibkan untuk saling menghormati. TRAVEL BUDDY hanya berlaku sebagai perantara. TRAVELER wajib mengikuti ketentuan yang sudah dipaparkan oleh BUDDY demi kemashlahatan bersama.</p>

                <strong>9. Kecurian dan Kecelakaan</strong>
                <p>BUDDY dan TRAVEL BUDDY tidak bertanggung jawab jika terjadi kecurian dan kecelakaan selama perjalanan berlangsung. Dengan menggunakan jasa TRAVEL BUDDY, TRAVELER melepaskan atau menyerahkan hak-hak hukum tertentu, termasuk hak untuk menuntut kompensasi klaim pada saat kecelakaan, saat ini maupun di masa yang akan datang.. TRAVELER juga setuju untuk melepaskan semua semua tuntutan terhadap  TRAVEL BUDDY dan BUDDY, yang mana dapat berbentuk perusahaan, untuk cedera pribadi, kematian, kerusakan properti, atau kerugian lain yang terjadi selama penggunaan jasa dari  TRAVEL BUDDY. </p>

                <strong>10. Penjadwalan Ulang</strong>
                <p>Perihal penjadwalan ulang, TRAVELER paling lambat 3x24 jam sebelum perjalanan. Penjadwalan ulang tidak dikenakan biaya tambahan. Namun, jika TRAVELER kemudian melakukan pembatalan, maka perhitungan Pembatalan Sesi Perjalanan akan berlaku sesuai dengan jadwal pertama TRAVELER. </p>

                <strong>11. Pembatalan Sesi Perjalanan</strong>
                <p>Untuk pembatalan, TRAVELER dapat memberitahukan TRAVEL BUDDY secepatnya melalui email atau telfon.</p>
                <ul>
                    <li>Jika pembatalan dilakukan di luar kurun waktu 14x24 jam sebelum sesi perjalanan, anda mendapatkan 90% refund dari total pembayaran</li>
                    <li>Jika pembatalan dilakukan di luar kurun waktu 7x24 jam sebelum sesi perjalanan, anda mendapatkan 50% refund dari total pembayaran</li>
                    <li>Jika pembatalan dilakukan di luar kurun waktu 3x24 jam sebelum sesi perjalanan, anda mendapatkan 20% refund dari total pembayaran</li>
                    <li>Jika pembatalan dilakukan dalam kurun waktu 3x24 jam  sebelum sesi perjalanan, tidak ada pengembalian uang</li>
                </ul>

                <strong>12. Pengembalian Uang</strong>
                <p>TRAVELER akan mendapatkan pengembalian uang setelah ada konfirmasi mengenai pembatalan Sesi Perjalanan. TRAVEL BUDDY akan menyelesaikan pengembalian uang maksimal5 hari kerja melalui bank transfer. Jikalau ada biaya transfer, maka biaya tersebut akan dibebankan kepada TRAVELER.</p>

                <strong>13. Email dan Promosi</strong>
                <p>TRAVEL BUDDY dapat mengontak Pengguna melalui email, push notifications, SMS, atau dengan medium lain kapan saja untuk memberikan informasi terbaru tentang diskon spesial, promosi, atau sesuatu yang berhubungan dengan TRAVEL BUDDY. Dengan terdaftar sebagai Pengguna jasa TRAVEL BUDDY, Pengguna setuju untuk berlangganan agar mendapat kan informasi tersebut melalui medium kami. Pengguna dapat memberhentikan masa langganan dengan mengklik “berhenti langganan” pada tombol yang tertera dalam email TRAVEL BUDDY, atau menghubungi langsung Support Team kami.</p>

                <strong>14. Pengumpulan Data Pribadi</strong>
                <p>TRAVEL BUDDY akan mengumpulkan data pribadi TRAVELER, tingkah laku belanja, perjalanan masa lalu, yang mana termasuk tapi tidak terbatas pada nama dan email, surat tagihan, kartu kredit, informasi demografis seperti umur, jenis kelamin, dan foto untuk memberikan pengalaman yang lebih baik, informasi yang relevan, dan menyelesaikan transaksi. 
TRAVEL BUDDY dapat mengumpulkan informasi tentang cookies dan store information tentang komputer hardware dan software. Informasi ini dapat terdiri dari: IP address, tipe browser, nama domain, waktu akses, dan acuan situs web. Informasi ini digunakan untuk servis, menjaga kualitas servis, dan untuk menyediakan statistik mengenai penggunaan website.</p>
                
                <br>
                <p>
                    TRAVEL BUDDY mendorong Pengguna untuk meninjau S&K TRAVEL BUDDY sehingga dengan menggunakan jasa TRAVEL BUDDY, Anda setuju untuk memberikan TRAVEL BUDDY aksesuntuk mengumpulkan informasi pribadi Anda. Kami akan pastikan kalau informasi sensitif ini tidak akan dijual atau diberikan kepada pihak ketiga yang tidak ada hubungannya dengan TRAVEL BUDDY. 
                </p>

                <p>SAYA MENGKONFIRMASI BAHWA SAYA TELAH MEMBACA DAN MEMAHAMI PERJANJIAN INI SEBELUM MENGGUNAKAN JASA DARI  TRAVEL BUDDY DAN SAYA MENGETAHUI SECARA SADAR BAHWA DENGAN MENANDATANGANI PERJANJIAN INI SAYA MELEPASKAN HAK-HAK HUKUM TERTENTU YANG SAYA ATAU PEWARIS SAYA, SAUDARA SEDARAH, PENGACARA DAN ADMINISTRATOR, SURUHAN, DAN PERWAKILAN YANG MUNGKIN AKAN MELAWAN  TRAVEL BUDDY, Karyawannya, direkturnya, pemegang sahamnya dan Buddynya.</p>
            </div>
        </div>
    </div>
</div>
@endsection