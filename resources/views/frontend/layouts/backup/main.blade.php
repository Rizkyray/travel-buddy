<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta name="csrf_token" content="{{csrf_token()}}"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="theme-color" content="#ffffff">
	<title>Travel Buddy</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" href="{{ url('img/favicon.ico') }}">

	<link rel="stylesheet" href="{{ url('/') . mix('css/app.css') }}" media="screen,projection">
    
    @if (App::environment('production') || App::environment('staging'))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-64644140-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-64644140-2');
        </script>
    @endif
</head>

    <body data-spy="scroll" @yield('body-attribute')>
        @include('frontend.components.flash-message')
        <div id="app">
            @include('frontend.components.nav')
		
		    @yield('content')

		    @include('frontend.components.footer')
        </div>

		{{--  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>  --}}
		<script src="{{ url('/') . mix('js/manifest.js') }}"></script>
		<script src="{{ url('/') . mix('js/vendor.js') }}"></script>
		<script src="{{ url('/') . mix('js/app.js') }}"></script>
		@yield('script')

	</body>
</html>
