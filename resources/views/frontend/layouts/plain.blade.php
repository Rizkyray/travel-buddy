<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta name="csrf_token" content="{{csrf_token()}}"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="theme-color" content="#ffffff">
	<title>Travel Buddy</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="icon" href="{{ url('img/favicon.ico') }}">

	<link rel="stylesheet" href="{{url('css/app.css')}}" media="screen,projection">

{{-- 	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/faviconss/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" href="{{ asset('img/faviconss/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image/png" href="{{ asset('img/faviconss/favicon-16x16.png') }}" sizes="16x16">
	<link rel="manifest" href="{{ asset('img/faviconss/manifest.json') }}">
	<link rel="mask-icon" href="{{ asset('img/faviconss/safari-pinned-tab.svg') }}" color="#5bbad5"> --}}
</head>
	<body>
        <div id="app">
		
		    @yield('content')

        </div>

		<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
		<script src="{{url('js/manifest.js')}}"></script>
		<script src="{{url('js/vendor.js')}}"></script>
		<script src="{{url('js/app.js')}}"></script>
		@yield('script')

	</body>
</html>
