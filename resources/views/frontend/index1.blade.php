@extends('frontend.layouts.main2')
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
 		
 <!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
@section('content')

 <script>
$(window).load(function()
{
    $('#myModal').modal('show');
});
</script>
	<section id="intro-section" class="background-0">
        <div class="overlay-grey"></div>
		<div class="wrapper-hero">
			<div class="container">
				<div class="header-hero">
					<h1 id="header-inline">Pergi
						<div class="rotation" id="header-inline-box">
							<span>Belanja</span>
							<span>Berbisnis</span>
							<span>Wisata</span>
							<span>Belajar</span>
						</div>
					dengan cita rasa orang Indonesia</h1>   
				</div>
                <label for="city" class="sub-heading">Perlu diteman? Mau kemana?</label>
                <buddy-search
                    method="GET"
                    action="{{ route('buddy.search') }}"
					default-keyword="{{ app('request')->input('location') }}"
					algolia-app-id="{{ env('ALGOLIA_APP_ID') }}"
                    algolia-api-key="{{ env('ALGOLIA_PUBLIC') }}">

                </buddy-search>
                <small id="cityHelp" class="form-text input-footer">
                    Most Popular City :
                    <ul>
                        <li>
                            <a href="{{ route('buddy.search', ['location' => 'Berlin']) }}" class="input-footer-bold">Berlin</a>
                        </li>
                        <li>
                            <a href="{{ route('buddy.search', ['location' => 'Paris']) }}" class="input-footer-bold">Paris</a>
                        </li>
                        <li>
                            <a href="{{ route('buddy.search', ['location' => 'London']) }}" class="input-footer-bold">London</a>
                        </li>
                        <li>
                            <a href="{{ route('buddy.search', ['location' => 'Tromso']) }}" class="input-footer-bold">Tromso</a>
                        </li>
                        <li>
                            <a href="{{ route('buddy.search', ['location' => 'Tokyo']) }}" class="input-footer-bold">Tokyo</a>
                        </li>
                        <li>
                            <a href="{{ route('buddy.search', ['location' => 'Kuala Lumpur']) }}" class="input-footer-bold">Kuala Lumpur</a>
                        </li>
                    </ul>
                </small>
			</div>
		</div>
    </section>
    
	<section id="about-us">
		<div class="container">
			<div class="about-us-content">
				<div class="row">
					<div class="col-lg-5 align-self-center">
						<img class="about-us-img" src="{{asset('img/about-us/about-us.png')}}" alt="tentang travel buddy">
					</div>
					<div class="col-lg-7 align-self-center">
						<h2 class="heading-1">Tentang Kami</h2>
						<div class="about-us-text">
							<p>
								Travel Buddy adalah sebuah platform yang mempertemukan Anda dengan orang Indonesia di negara tujuan yang akan menemani selama perjalanan.
							</p>
							<p>
								Kami menawarkan  fleksibilitas dalam mengatur dan merencanakan perjalanan anda sesuai dengan permintaan dan kebutuhan.</p> 
							<p>
								Tidak hanya sekedar menemani, para ''Buddies" juga akan berbagi kisah dan pengetahuan lokal inspiratif yang akan membuat petualangan 	Anda semakin berkesan.
							</p>
							<a href="{{ url('about-us') }}" class="my-button my-button--orange">Baca Lebih Lanjut</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="hiw">
		<div class="container">
			<div class="row">
				<div class="col">
					<p class="h1">Bagaimana cara <span class="orange">Travel Buddy</span> <br>dapat membantumu?</p>
					<div class="row">
						<div class="col-xl-7">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe src="https://www.youtube.com/embed/vFrwYSKxqzg?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</div>
						</div>
						<div class="col-xl-5">
							<p class="sub-heading">Langkah-langkah utama:</p>
							<div class="div-flex how-to-step">
								<div class="list-accent"></div>
								<ul class="how-to-step-list">
									<li class="media">
										<div class="circle-wrapper div-flex">
											<i class="fas fa-search"></i>
										</div>
										<div class="media-body">
											<h5 class="mt-0 mb-1">Find.</h5>
											<span>Cari paket atau tujuan destinasi perjalanan kamu</span>
										</div>
									</li>
									<li class="media">
										<div class="circle-wrapper div-flex">
											<i class="fas fa-hand-point-up"></i>
										</div>
										<div class="media-body">
											<h5 class="mt-0 mb-1">Request.</h5>
											<span>Pesan Buddy pilihanmu</span>
										</div>
									</li>
									<li class="media">
										<div class="circle-wrapper div-flex">
											<i class="fas fa-glass-martini"></i>
										</div>
										<div class="media-body">
											<h5 class="mt-0 mb-1">Enjoy.</h5>
											<span>Jelajahi kota tujuan dengan Buddy pilihanmu</span>
										</div>
									</li>
								</ul>
              				</div>
              				<a href="{{ route('help.menggunakan-jasa-kami') }}" class="btn btn-search-buddy">Pelajari lebih lanjut</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="wwi">
		<div class="container">
			<div class="row">
				<div class="col-xl-4 col-md-6">
					<div class="card values-intro">
						<div class="card-body">
							<div class="card-text">Lima alasan mengapa <span>kamu</span> sebaiknya berpergian bersama <span>kami</span></div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="card values">
						<div class="img-wrapper">
							<img class="card-img-top" src="{{asset('img/why-travel/speak-bahasa.jpg')}}" alt="Card image cap">
							<div class="num-wrapper">
								<p class="num">1</p>
							</div>
						</div>
						<div class="card-body">
							<h5 class="card-title">Speak in Bahasa</h5>
							<div class="card-text">Mudahnya berkomunikasi dengan guide mu, cukup menggunakan Bahasa Indonesia</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="card values">
						<div class="img-wrapper">
							<img class="card-img-top" src="{{asset('img/why-travel/familiarity.jpg')}}" alt="Card image cap">
							<div class="num-wrapper">
								<p class="num">2</p>
							</div>
						</div>
						<div class="card-body">
							<h5 class="card-title">Familiarity</h5>
							<div class="card-text">Menjelajah bersama guide yang sudah akrab dengan destinasi kota tujuanmu</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="card values">
						<div class="img-wrapper">
							<img class="card-img-top" src="{{asset('img/why-travel/customer-based.jpg')}}" alt="Card image cap">
							<div class="num-wrapper">
								<p class="num">3</p>
							</div>
						</div>
						<div class="card-body">
							<h5 class="card-title">Customer-based tour</h5>
							<div class="card-text">Tentukan perjalanan dan kunjungi semua tempat sesuai dengan keinginanmu</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="card values">
						<div class="img-wrapper">
							<img class="card-img-top" src="{{asset('img/why-travel/flexible.jpg')}}" alt="Card image cap">
							<div class="num-wrapper">
								<p class="num">4</p>
							</div>
						</div>
						<div class="card-body">
							<h5 class="card-title">Flexible</h5>
							<div class="card-text">Jam dan waktu dapat menyesuaikan kebutuhanmu</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="card values">
						<div class="img-wrapper">
							<img class="card-img-top" src="{{asset('img/why-travel/inspiring.jpg')}}" alt="Card image cap">
							<div class="num-wrapper">
								<p class="num">5</p>
							</div>
						</div>
						<div class="card-body">
							<h5 class="card-title">Inspiring</h5>
							<div class="card-text">Bawa pulang pengalaman jalan-jalan yang paling berkesan!</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	{{-- <section id="buddy-maps">
		<div class="container">
			<div class="row">
				<div class="col intro-header">
					<p class="h1"><span class="orange">Travel Buddy</span> ada di Den Haag</p>
					<p class="sub-heading">dan 99 kota lainnya</p>
				</div>
			</div>
		</div>
		<div id="map"></div>
	</section> --}}

	<section id="testimonials">
		<div class="container">
			<div class="row">
				<div class="col intro-header">
					<p class="h1">Dengar apa kata mereka tentang <br>layanan <span class="orange">Travel Buddy</span></p>
				</div>
			</div>
			<div class="testimonials-carousel">
				<div class="testimonial-item-wrapper">
					<div class="testimonial-item">
						<img class="testimonial-photo" src="{{asset('img/reviewer/pelajar.jpg')}}" alt="Generic placeholder image">
						<div class="testimonial-text">
							<p>Sangat mudah mendapatkan guide yang cocok di Travel Buddy. Setelah membaca review yang bagus dari traveler lain, saya menjadi percaya diri memilih Chris untuk menemani perjalanan saya di Den Haag. Chris orang yang ramah dan enak untuk diajak ngobrol, sehingga saya tidak canggung dan lebih bisa menikmati liburan sebelum saya harus kembali meneruskan kuliah. Saya sangat merekomendasikan Chris untuk memandu teman-teman yang berencana berlibur ke Den Haag.</p>
							<p class="author-buddy text-capitalize"><span class="name">Dian Zulfa Anggraeni</span> <span class="place">-- Den Haag</span></p>
						</div>
					</div>
				</div>
				<div class="testimonial-item-wrapper">
					<div class="testimonial-item">
						<img class="testimonial-photo" src="{{asset('img/reviewer/Astrid.jpg')}}" alt="Generic placeholder image">
						<div class="testimonial-text">
							<p>Terima kasih Rizky sudah mendampingi group APTISI Surakarta pada bulan Oktober tahun lalu. Rombongan kami terdiri dari pimpinan perguruan tinggi swasta di wilayah Solo Raya. Tentu tidak mudah mendampingi beliau-beliau yang semua sudah terbiasa dilayani. Kunjungan kami ke Eropa sangat berkesan, khususnya ketika di negara Belanda karena mendapat kesempatan untuk mengintip salah satu kampus besar disana yaitu Utrecht University. Hal ini bisa terlaksana dengan baik karena support kawan-kawan PPI yang banyak membantu kami. Tapi overall semua happy selama kami di Belanda, kecuali ada 3 peserta yang sempat tersesat ya hehe. Once again, thank you so much!</p>
							<p class="author-buddy text-capitalize"><span class="name">Astrid</span> <span class="place">-- Belanda</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	@if (Auth::check())

	@else()
	<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-header">
           
           <button type="button" class="close" data-dismiss="modal">&times;</button>
       <br>
          <h4 class="modal-title"></h4></div>
        
        <div class="modal-body">
        	<div class="modal-body1">
        	<div class="modalimage"><img src="{{asset('img/logo.png')}}" alt="" style="width: 100%;"></div>
        </div>
          <h2 class="popups">WUJUDKAN IMPIANMU</h2>
          <p class="popups">Rasakan pengalaman baru dalam Traveling keluar negeri.</p>
          <p class="popups">Temukan inspirasi untuk mewujudkan impian kamu.</p>
          <p style="text-align:center;"><a href="https://travelbuddy.id/register" class="my-button my-button--orange" style="border-radius: 9px!important;font-size: 1.1rem!important;font-weight: 500!important;margin-top: 20px!important;">Gabung Sekarang</a></p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      </div>
    </div>
	@endif
			
@endsection

@section('script')
		
		{{-- <script>
			// var map;
			// function initMap() {
			// 	map = new google.maps.Map(document.getElementById('map'), {
			// 		center: {lat: 32.5202286, lng: 27.8302895},
			// 		zoom: 2,
			// 		styles: [
			// 							{
			// 									"featureType": "all",
			// 									"elementType": "labels.text.fill",
			// 									"stylers": [
			// 											{
			// 													"saturation": 36
			// 											},
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 40
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "all",
			// 									"elementType": "labels.text.stroke",
			// 									"stylers": [
			// 											{
			// 													"visibility": "on"
			// 											},
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 16
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "all",
			// 									"elementType": "labels.icon",
			// 									"stylers": [
			// 											{
			// 													"visibility": "off"
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "administrative",
			// 									"elementType": "geometry.fill",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 20
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "administrative",
			// 									"elementType": "geometry.stroke",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 17
			// 											},
			// 											{
			// 													"weight": 1.2
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "landscape",
			// 									"elementType": "geometry",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 20
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "poi",
			// 									"elementType": "geometry",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 21
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "road.highway",
			// 									"elementType": "geometry.fill",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 17
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "road.highway",
			// 									"elementType": "geometry.stroke",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 29
			// 											},
			// 											{
			// 													"weight": 0.2
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "road.arterial",
			// 									"elementType": "geometry",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 18
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "road.local",
			// 									"elementType": "geometry",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 16
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "transit",
			// 									"elementType": "geometry",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 19
			// 											}
			// 									]
			// 							},
			// 							{
			// 									"featureType": "water",
			// 									"elementType": "geometry",
			// 									"stylers": [
			// 											{
			// 													"color": "#000000"
			// 											},
			// 											{
			// 													"lightness": 17
			// 											}
			// 									]
			// 							}
			// 					]
			// 	});
			// }
		
		</script> --}}

		{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFkPz2UGIn2X2NB6p7y0lCmCMKVg77_cI&callback=initMap"
		async defer></script> --}}
		
		 <!--  #3 -->
		 

  </div>
</div>

@endsection