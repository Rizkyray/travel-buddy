@extends('frontend.layouts.main')

@section('content')
<div class="join-buddy-hero">
    <div class="join-buddy-hero__image">
        <img src="{{ asset('img/become-a-buddy/hero.jpg') }}" alt="Bergabung bersama kami">
    </div>

    <div class="join-buddy-hero__content">
        <div class="join-buddy-hero__caption">
            <h1 class="join-buddy-hero__title">Bergabung Bersama Kami</h1>
            <p class="join-buddy-hero__subtitle">
                Ayo ikut berkontribusi dalam menjadikan pariwisata sebagai pembawa perubahan positif bagi masyarakat Indonesia!
            </p>
        </div>
        <div class="join-buddy-hero__price-est">
            <price-estimation>
                <template slot="cta">
                    <a href="{{ route('dashboard.buddy') }}" class="my-button my-button--block my-button--peach">Bergabung Menjadi Buddy</a>
                </template>
            </price-estimation>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-10 mx-auto">

            <div class="join-buddy">
                <div class="join-buddy__tip-num">
                    <span>01</span>
                </div>
                <div class="join-buddy__tip">
                    <h2 class="join-buddy__header">Mengapa menjadi Buddy</h2>
                    <p class="join-buddy__body">
                        Dengan bergabung menjadi Buddy, kamu tidak perlu lagi memikirkan bagaimana memasarkan jasamu sebagai guide. Kamu tinggal menunggu Traveler berdatangan sambil mempersiapkan perjalanan yang mengesankan.
                    </p>
                    
                    <p class="join-buddy__body">
                        Selain itu, kamu juga akan tergabung dengan komunitas orang-orang hebat dari berbagai belahan dunia yang memiliki semangat dan tujuan yang sama. Kami juga siap memberikan fasilitas dan pengetahuan yang kamu butuhkan untuk menjadi the Best Buddy. 
                    </p>
                </div>
            </div>
            
            <div class="join-buddy">
                <div class="join-buddy__tip-num"><span>02</span></div>
                <div class="join-buddy__tip">
                    <h2 class="join-buddy__header">Price include / Biaya lain-lain</h2>
                    <p class="join-buddy__body">
                        Kamu bebas merekomendasikan tempat wisata yang dituju sesuai dengan keinginan Traveler. Tarif yang tertera di Travel Buddy tidak termasuk biaya makan, transportasi, atau biaya masuk tempat wisata; melainkan, biaya-biaya tersebut dapat dibebankan kepada Traveler*. Tip dari Traveller sepenuhnya menjadi hak milik kamu. 
                    </p>
                    <p>* Silahkan periksa <a href="{{ url('syarat-dan-ketentuan') }}">syarat dan ketentuan</a> yang berlaku.</p>
                </div>
            </div>
            
            <div class="join-buddy">
                <div class="join-buddy__tip-num"><span>03</span></div>
                <div class="join-buddy__tip">
                    <h2 class="join-buddy__header">Jangan Lupa</h2>
                    <p class="join-buddy__body">
                        Kamu di sini tidak hanya sebatas menemani Traveler jalan-jalan saja. Sudah menjadi tugas kamu untuk berbagi apa yang telah kamu pelajari di luar negeri dari segi budaya, pengetahuan, dan kemampuan kamu kepada Traveler yang sedang kamu guide, karena bisa jadi mereka merupakan para stakeholder penting.
                    </p>
                </div>
            </div>
            
            <div class="join-buddy">
                <div class="join-buddy__tip-num"><span>04</span></div>
                <div class="join-buddy__tip">
                    <h2 class="join-buddy__header">Tips dari Kami</h2>
                    <p class="join-buddy__body">
                        Knowledge is Power! Kerahkan pengetahuan lokal tentang kotamu. Kenali travel hack, tempat-tempat tersembunyi, kuliner yang nikmat dan hal-hal lain yang jarang diketahui orang. Tetap waspada dengan penipuan dan tourist trap yang biasa menjebak Traveler awam.
                    </p>
                </div>
            </div>
            
            <div class="join-buddy">
                <div class="join-buddy__tip-num"><span>05</span></div>
                <div class="join-buddy__tip">
                    <h2 class="join-buddy__header">Atur Jadwal</h2>
                    <p class="join-buddy__body">
                        Kami mengerti jadwalmu yang tak menentu. Untuk itu kami memberi kemudahan dalam mengatur alokasi waktu yang bisa kamu sediakan untuk Travel Buddy.
                    </p>
                    <p class="join-buddy__body">
                        Pilih waktu yang nyaman bagimu. Jangan sampai Travel Buddy mengganggu kesibukanmu dan jangan lupa untuk selalu jaga kesehatanmu ya!
                    </p>

                    <a href="https://api.whatsapp.com/send?phone=6287789136398" class="my-button my-button--green my-button--icon-whatsapp mt-3 mr-2">Hubungi Kami</a>
                    <a href="{{ route('dashboard.buddy') }}" class="my-button my-button--peach">Bergabung Menjadi Buddy</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection