@extends('frontend.layouts.main')

@section('content')

<div class="profile__banner profile__section">
    <img src="{{ url('img/about-us/green.jpg') }}" alt="">
</div>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="profile__picture-wrapper">
                <div class="profile__picture">
                    <img src="{{ Helper::getProfilePicture($buddy->user) }}" alt="{{ $buddy->nickname }}">
                    <div class="profile__price">
                            <span>{{ Helper::formatCurrency($buddy->price, 'IDR') }}</span> 
                            <span class="profile__price-time"> / jam</span>
                    </div>
                </div>
            </div>
           <h1 class="profile__name" style="text-align: center">{{ $buddy->nickname }}</h1>
            <a href="{{ route('buddy.book', ['id' => $buddy->id]) }}" class="profile__book-me my-button my-button--orange my-button--block">Pilih Buddy</a>
        </div>
        <div class="col-md-8">
            <div class="profile__section">
               <h2 class="profile__title">Tentang {{ $buddy->nickname }}</h2>
                <p class="profile__description">{{ $buddy->description }}</p>
            </div>
            
            <div class="profile__section">
                <h2 class="profile__title">Kota Cakupan</h2>
                <ul class="profile__list city-coverage">
                    @foreach ($buddy->city_coverage as $city)
                        <li>{{ $city }}, {{ $buddy->country }}</li>
                    @endforeach
                </ul>
            </div>

            <div class="profile__section">
                <h2 class="profile__title">Kemampuan dan Minat Travelling</h2>
                
                <h3 class="profile__skills language">Bahasa</h3>
                <ul class="profile__list">
                    <li>{{ $buddy->language }}</li>
                </ul>
                
                <h3 class="profile__skills interest">Personal Interest</h3>
                <ul class="profile__list">
                    <li>{{ Helper::printArray($buddy->interests) }}</li>
                </ul>
                
                <h3 class="profile__skills skill">Additional Skills</h3>
                <ul class="profile__list">
                    <li>{{ $buddy->skills }}</li>
                </ul>
                
            </div>

            <div class="profile__section">
                <h2 class="profile__title">Pengalaman sebagai Buddy Travelling</h2>
                <p class="profile__description">{{ $buddy->experience }}</p>
            </div>

            <div class="profile__section">
                <h2 class="profile__title">Tertarik menggunakan jasa {{ $buddy->nickname }} sebagai Buddy mu?</h2>

                <a href="{{ route('buddy.book', ['id' => $buddy->id]) }}" class="profile__pick-me my-button my-button--orange">Pilih {{ $buddy->nickname }} Menjadi Buddy</a>
            </div>
        </div>
    </div>
</div>
@endsection