<meta name="csrf_token" content="{{csrf_token()}}"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="theme-color" content="#ffffff">
	<title>Travel Buddy Indonesia</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	 <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link rel="icon" href="{{ url('img/favicon.ico') }}">
     @if (App::environment('production') || App::environment('staging'))
       <!--google analyticsnya rizky-->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-64644140-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-64644140-2');
        </script> -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135438878-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-135438878-1');
		</script>


    @endif
	
   
	
	<!-- css files -->
	<link rel="stylesheet" href="{{ url('/') . mix('css/app3.css') }}" media="screen,projection">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ url('/') . mix('css/bootstrap.css') }}" media="screen,projectiondd">
   <!-- custom css -->
    <link rel="stylesheet" href="{{ url('/') . mix('css/font.css') }}" media="screen,projection">
    <!-- fontawesome css -->
	<!-- //css files -->
		<!-- //Custom Theme files -->
	<!-- web font -->
	<link href="//fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<!-- //web font -->
</head>

<body>
	<h1>Comely Profile Widget</h1>
	<div class="main-agileits">
		<div class="inner-w3-agile">
			<!-- left section -->
			<div class="left-section">
				<div class="left-btns">
					<a href="#" class="btn1">
						<i class="fas fa-user-plus"></i>Follow</a>
					<a href="#" class="btn2">
						<i class="far fa-envelope"></i>Message</a>
				</div>
			</div>
			<!-- //left section -->
			<!-- right section -->
			<div class="right-w3-agile">
				<div class="right-info">
					<h2>Kelly Rogers</h2>
					<span class="designation">Creative designer</span>
					<p class="para-wthree">
						<a href="mailto:example@mail.com">mail@example.com</a>
					</p>
					<p class="para-wthree">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore et dolore magna
						aliqua uta enim ad minim ven iam quis nostrud</p>
					<div class="social-w3layouts">
						<ul>
							<li>
								<a href="#">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fab fa-twitter"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fab fa-google-plus-g"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="right-grids-w3-agileits">
					<div class="grids g1">
						<h6>
							<span>448</span>Tweets</h6>
					</div>
					<div class="grids g2">
						<h6>
							<span>600</span>Followers</h6>
					</div>
					<div class="grids g3">
						<h6>
							<span>218</span>Following</h6>
					</div>
				</div>
			</div>
			<!-- //right section -->
		</div>
	</div>
	<!-- copyright-w3ls -->
	<div class="copyright-w3ls">
		<p> © 2018 Comely Profile Widget . All rights reserved | Design by
			<a href="http://w3layouts.com/" target="_blank">W3layouts</a>
		</p>
	</div>
	<!-- //copyright-w3ls -->
</body>

</html>