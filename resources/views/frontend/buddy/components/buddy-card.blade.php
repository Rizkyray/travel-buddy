<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
 
 <script>  
    $('#myModal1').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>



<div class="card">
    <div class="profile__picture-wrapper">
        <div class="pp-wrapper">
            <img class="card-img-top" src="{{ Helper::getProfilePicture($buddy->user) }}" alt="Buddy picture">
            <div class="price-buddy">
                <p class="rate">
                    {{ Helper::formatCurrency($buddy->price, 'IDR') }}
                    <span class="dur">/jam</span>
                </p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="div-flex name-age">
            <h5 class="card-title">{{ $buddy->nickname }}</h5>
            <p>Age: <span>{{ $buddy->age() }}</span></p>
        </div>
        <p class="text-capitalize country"><span>{{ Helper::printArray($buddy->city_coverage)  }}</span> / {{ $buddy->country }}</p>
        <div class="div-flex abilities">
            <i class="fas fa-comment-alt abilities__icon"></i>
            <p class="text-capitalize">{{ $buddy->language }}</p>
        </div>
        <div class="div-flex abilities">
            <i class="fa fa-heart abilities__icon"></i>
            <p class="text-capitalize">{{ Helper::printArray($buddy->interests) }}</p>
        </div>
         @if (Auth::check())
        <div class="div-flex button-wrapper">
            <a href="{{ route('buddy.profile', ['id' => $buddy->id]) }}" class="btn btn-search-buddy hollow">See Details</a>
            <a href="{{ route('buddy.book', ['id' => $buddy->id]) }}" class="btn btn-search-buddy" >Book Me</a>
            @else
             <div class="div-flex button-wrapper">
                <a href="" class="btn btn-search-buddy hollow" data-toggle="modal" data-target="#myModal1">See Details</a>
            <a href="" class="btn btn-search-buddy" data-toggle="modal" data-target="#myModal1">Book Me</a>
          @endif
        </div>
    </div>
</div>


<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-header">
           
           <button type="button" class="close" data-dismiss="modal">&times;</button>
       <br>
          <h4 class="modal-title"></h4></div>
        
        <div class="modal-body">
            <div class="modal-body1">
            <div class="modalimage"><img src="{{asset('img/logo.png')}}" alt="" style="width: 100%;"></div>
            
        </div>
          <h2 class="popups">WUJUDKAN IMPIANMU</h2>
          <p class="popups">Mau Tau lebih Lengkap Tentang Buddy Kita.</p>
          <p class="popups">Register Dulu Yuk.</p>
          <p style="text-align:center;"><a href="https://travelbuddy.id/register" class="my-button my-button--orange" style="border-radius: 9px!important;font-size: 1.1rem!important;font-weight: 500!important;margin-top: 20px!important;">Gabung Sekarang</a></p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      </div>
    </div>  
