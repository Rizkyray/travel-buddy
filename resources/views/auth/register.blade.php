@extends('frontend.layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="login-form">
                <div class="login-form__header">
                    <h1 class="login-form__title">
                        Daftar Travel Buddy
                    </h1>
                </div>
                <form class="login-form__form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="my-form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" >Nama</label>
                        <input
                            id="name"
                            type="text"
                            class="my-form-control"
                            name="name"
                            value="{{ old('name') }}"
                            placeholder="Nama Lengkap"
                            required autofocus>
    
                        @if ($errors->has('name'))
                            <span class="my-form__error">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                
                    <div class="my-form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email</label>
                        <div class="my-form-group--icon-mail">
                            <input
                                id="email"
                                type="email"
                                class="my-form-control"
                                name="email"
                                value="{{ old('email') }}"
                                placeholder="e.g username@domain.com"
                                required>
                        </div>
                        @if ($errors->has('email'))
                            <span class="my-form__error">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                
                    <div class="my-form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>
                        <div class="my-form-group--icon-password">
                            <input
                                id="password"
                                type="password"
                                class="my-form-control"
                                name="password"
                                placeholder="Password"
                                required>
                        </div>
                        @if ($errors->has('password'))
                            <span class="my-form__error">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                    </div>
                
                    <div class="my-form-group">
                        <label for="password-confirm">Konfirmasi Password</label>
                        <div class="my-form-group--icon-password">
                            <input 
                                id="password-confirm"
                                type="password"
                                class="my-form-control"
                                name="password_confirmation"
                                placeholder="Konfirmasi Password"
                                required>
                        </div>
                    </div>
                    <button type="submit" class="login-form__submit">Register</button>
                    <div class="login-form__footer">
                        <p>Sudah punya akun Travel Buddy? <a href="{{ route('login') }}">Sign in di sini.</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
