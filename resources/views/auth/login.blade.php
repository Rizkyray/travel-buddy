@extends('frontend.layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="login-form">
               <div class="login-form__header">
                    <h1 class="login-form__title">
                        Sign in ke Travel Buddy
                    </h1>
                </div>
                <form class="login-form__form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="my-form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email</label>
                        <div class="my-form-group--icon-mail">
                            <input
                                id="email"
                                type="email"
                                class="my-form-control"
                                name="email"
                                value="{{ old('email') }}"
                                placeholder="Email address"
                                required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <span class="my-form__error">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                    <div class="my-form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>
                        <div class="my-form-group--icon-password">
                            <input
                                id="password"
                                type="password"
                                class="my-form-control"
                                name="password"
                                placeholder="Password"
                                required>
                        </div>
                        @if ($errors->has('password'))
                            <span class="my-form__error">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                    </div>
                    <div class="my-form-group my-form-check">
                        <input
                            id="remember"
                            type="checkbox"
                            class="my-form-check-input"
                            name="manual_input"
                            {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember">Remember Me</label>
                    </div>
                    <button type="submit" class="login-form__submit">Login</button>
                    <div class="login-form__footer">
                        <p>
                            Belum punya akun? <a href="{{ route('register') }}">Daftar di sini.</a>
                        </p>
                        <p>
                            <a href="{{ route('password.request') }}">Lupa Password?</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
