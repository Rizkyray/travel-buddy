@extends('frontend.layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="login-form">
                <div class="login-form__header">
                    <h1 class="login-form__title">
                        Reset Password
                    </h1>
                </div>
                <form class="login-form__form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <div class="my-form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email</label>
                        <div class="my-form-group--icon-mail">
                            <input
                                id="email"
                                type="email"
                                class="my-form-control"
                                name="email"
                                value="{{ old('email') }}"
                                placeholder="Email address"
                                required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <span class="my-form__error">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="login-form__submit">Reset Password</button>
                    <div class="login-form__footer">
                        <p>Sudah punya akun Travel Buddy? <a href="{{ route('login') }}">Login di sini</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
