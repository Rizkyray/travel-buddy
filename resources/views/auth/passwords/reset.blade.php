@extends('frontend.layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="login-form">
               <div class="login-form__header">
                    <h1 class="login-form__title">
                        Reset Password
                    </h1>
                </div>
                <form class="login-form__form" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="my-form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email</label>
                        <div class="my-form-group--icon-mail">
                            <input
                                id="email"
                                type="email"
                                class="my-form-control"
                                name="email"
                                value="{{ old('email') }}"
                                placeholder="Email address"
                                required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <span class="my-form__error">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                    <div class="my-form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>
                        <div class="my-form-group--icon-password">
                            <input
                                id="password"
                                type="password"
                                class="my-form-control"
                                name="password"
                                placeholder="Password"
                                required>
                        </div>
                        @if ($errors->has('password'))
                            <span class="my-form__error">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                    </div>

                    <div class="my-form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password_confirmation">Konfirmasi Password</label>
                        <div class="my-form-group--icon-password">
                            <input
                                id="password_confirmation"
                                type="password"
                                class="my-form-control"
                                name="password_confirmation"
                                placeholder="Konfirmasi Password"
                                required>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <span class="my-form__error">
                                {{ $errors->first('password_confirmation') }}
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="login-form__submit">Ubah Password</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
