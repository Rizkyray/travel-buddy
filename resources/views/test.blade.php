@extends('frontend.layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <language-picker></language-picker>
        </div>
    </div>
</div>
@endsection