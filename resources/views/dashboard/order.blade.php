@extends('frontend.layouts.main')

@section('content')
@include('dashboard.components.tab')
<div class="content">   
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="order-received-buddy">
                    @if ($user->buddy->orders->count() > 0)
                        <div class="accordion-order__head">
                            <p>Nomor Order</p>
                            <p>Kota Pilihan</p>
                            <p>Waktu Perjalanan</p>
                            <p>Total Biaya</p>
                            <p>Status Order</p>
                        </div>
                        @foreach ($user->buddy->orders as $order)
                            <div class="tb-accordion accordion-order">
                                <div class="tb-accordion__header">
                                    <h3 class="tb-accordion__title tb-accordion__toggler">
                                        Order #{{ $order->order_number }}
                                    </h3>
                                    <p class="accordion-order__destination">
                                        {{ $order->trip_details['destination'] }}
                                    </p>
                                    <p class="accordion-order__date">
                                        {{ (new Carbon($order->start_date))->format('d/m/Y') }} - {{ (new Carbon($order->end_date))->format('d/m/Y') }}
                                    </p>
                                    <p class="accordion-order__price">
                                        {{ Helper::formatCurrency($order->price_estimation) }}
                                    </p>
                                    <div class="accordion-order__action">
                                        @if ($order->status === \App\Order::ORDER_SUBMITTED)
                                             <tb-modal
                                                class-name="my-button my-button--peach"
                                                button-value="Accept">
                                                <template slot="body">
                                                    <p>Apakah Anda yakin akan menerima order?</p>
                                                </template>
                                                <template slot="proceed">
                                                    <button
                                                        type="submit"
                                                        form="order{{ $order->id }}Accept"
                                                        class="my-button my-button--peach">
                                                        Accept
                                                    </button>
                                                </template>
                                            </tb-modal>
                                            <tb-modal
                                                class-name="my-button my-button--grey"
                                                button-value="Decline">
                                                <template slot="body">
                                                    <p>Apakah Anda yakin akan menolak order?</p>
                                                </template>
                                                <template slot="proceed">
                                                    <button
                                                        type="submit"
                                                        form="order{{ $order->id }}Decline"
                                                        class="my-button my-button--peach">Decline
                                                    </button>
                                                </template>
                                            </tb-modal>
                                            <form
                                                method="POST"
                                                id="order{{ $order->id }}Accept"
                                                style="display: none"
                                                action="{{ route('buddy.order.accept') }}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="order_id" value="{{ $order->id }}">
                                            </form>
                                            <form
                                                method="POST"
                                                id="order{{ $order->id }}Decline"
                                                style="display: none"
                                                action="{{ route('buddy.order.decline') }}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="order_id" value="{{ $order->id }}">
                                            </form>
                                        @elseif ($order->status === \App\Order::ORDER_ACCEPTED)
                                            Order diterima
                                        @elseif ($order->status === \App\Order::ORDER_DECLINED)
                                            Order ditolak
                                        @endif
                                    </div>
                                </div>
                                <div class="tb-accordion__content">
                                    <div class="tb-accordion__body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell title">Pelanggan</div>
                                                    <div class="accordion-order__cell value">{{ $order->name }}</div>
                                                </div>
                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell title">Email</div>
                                                    <div class="accordion-order__cell value">{{ $order->email }}</div>
                                                </div>
                                                @if($order->payment_status = "PAYMENT_UNPAID")
                                                <div class="accordion-order__row">
                                                   <div class="accordion-order__cell title"><!--  Telepon -->belom bayar</div>
                                                    <div class="accordion-order__cell value"><!--{{ $order->phone }}--></div>
                                                </div>
                                                @else
                                                <div class="accordion-order__row">
                                                   <div class="accordion-order__cell title"><!--  Telepon -->bayar</div>
                                                    <div class="accordion-order__cell value"><!--{{ $order->phone }}--></div>
                                                </div>
                                                @endif

                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell title">Rombongan</div>
                                                    <div class="accordion-order__cell value">{{ $order->trip_details['total_adults'] }} {{ ' orang dewasa' }}@if ((int) $order->trip_details['total_children'] > 0), {{ $order->trip_details['total_children'] }} orang anak @endif
                                                    </div>
                                                </div>
                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell title">Tujuan Perjalanan</div>
                                                    <div class="accordion-order__cell value">
                                                        @foreach ($order->trip_details['trip_type'] as $index => $type)
                                                            {{ $type }}
                                                            @if ($index !== count($order->trip_details['trip_type']) - 1)
                                                                ,
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell title">Deskripsi Perjalanan</div>
                                                    <div class="accordion-order__cell value"></div>
                                                </div>
                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell value full">{{ $order->trip_details['trip_description'] }}</div>
                                                </div>
                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell title">Pesan untuk Buddy</div>
                                                    <div class="accordion-order__cell value"></div>
                                                </div>
                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell value full">
                                                        {{ $order->message_to_buddy }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell title">Detail Perjalanan:</div>
                                                </div>
                                                <div class="accordion-order__row">
                                                    <br>
                                                </div>
                                                @php
                                                    $startDate = new Carbon($order->start_date);
                                                    $endDate = (new Carbon($order->end_date))->addDay();

                                                    $interval = new DateInterval('P1D');
                                                    $dateRange = new DatePeriod($startDate, $interval ,$endDate);
                                                @endphp
                                                <div class="accordion-order__row">
                                                    <div class="accordion-order__cell">Tanggal</div>
                                                    <div class="accordion-order__cell">Waktu</div>
                                                    <div class="accordion-order__cell">Harga</div>
                                                </div>
                                                @foreach ($dateRange as $index => $date)
                                                    @php
                                                        $dt = new Carbon($date);
                                                        $formatedDate = $dt->formatLocalized('%e %B %Y');
                                                    @endphp
                                                        <div class="accordion-order__row">
                                                            <div class="accordion-order__cell value">{{ $formatedDate }}</div>
                                                            <div class="accordion-order__cell value">{{ $order->hours_per_day[$index] }} Jam</div>
                                                            <div class="accordion-order__cell value">{{ Helper::formatCurrency((int) $order->price_per_day * (int) $order->hours_per_day[$index]) }}</div>
                                                        </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <h2 class="heading-3" style="min-height:320px">
                            Anda belum memiliki order
                        </h2>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection