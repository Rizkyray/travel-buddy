@extends('frontend.layouts.main')

@section('content')
@include('dashboard.components.tab')

<div class="container">
    <div class="row">
        <div class="col-lg-3 sidebar-wrapper">
            <div class="sidebar">
                <nav class="nav vertical-nav">
                    <ul>
                        <li>
                            <a class="nav-link active">
                                Ganti Password
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="col-md-6">
            <div class="content">
                <form
                    method="POST"
                    action="{{ route('user.changePassword') }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <fieldset class="fieldset-bordered">
                        <legend class="profile__title">Ubah Password</legend>

                        <div class="my-form-group">
                            <label for="current_password">Password Sekarang</label>
                            <div class="my-form-group--icon-password">
                                <input
                                    name="current_password"
                                    type="password"
                                    class="my-form-control"
                                    placeholder="Password Lama">
                            </div>
                            @if ($errors->has('current_password'))
                                <span class="my-form__error">
                                    {{ $errors->first('current_password') }}
                                </span>
                            @endif
                        </div>
                        <div class="my-form-group">
                            <label for="password">Password Baru</label>
                            <div class="my-form-group--icon-password">
                                <input
                                    name="password"
                                    type="password"
                                    class="my-form-control"
                                    placeholder="Password Baru">
                            </div>
                            @if ($errors->has('password'))
                                <span class="my-form__error">
                                    {{ $errors->first('password') }}
                                </span>
                            @endif
                        </div>
                        <div class="my-form-group">
                            <label for="password_confirmation">Konfirmasi Password Baru</label>
                            <div class="my-form-group--icon-password">
                                <input
                                    name="password_confirmation"
                                    type="password"
                                    class="my-form-control"
                                    placeholder="Konfirmasi Password baru">
                            </div>
                        </div>

                        <button type="submit" class="my-button my-button--orange">Ganti Password</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection