<div class="profile__tab">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="tb-tab-nav">
                    <li
                    class="tb-tab-nav__item {{ active('dashboard') }}">
                        <a href="{{ route('dashboard.home') }}">Data Personal</a>
                    </li>
                    <li class="tb-tab-nav__item {{ active('dashboard/buddy') }}">
                        <a href="{{ route('dashboard.buddy') }}">
                            {{ $user->buddy ? 'Data Buddy' : 'Become a Buddy' }}
                        </a>
                    </li>
                    <li class="tb-tab-nav__item {{ active('dashboard/settings') }}">
                        <a href="{{ route('dashboard.settings') }}">Pengaturan</a>
                    </li>
                    @if ($user->buddy) 
                        <li class="tb-tab-nav__item {{ active('dashboard/order') }}">
                            <a href="{{ route('dashboard.order') }}">
                                Order
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>