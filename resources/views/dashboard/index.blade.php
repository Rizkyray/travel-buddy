@extends('frontend.layouts.main')

@section('content')
@include('dashboard.components.tab')

<div class="content">
    <div class="container">
        @php
            if (Session::has('info')) {
                $profileFlashClass = 'info';
                $content = Session::get('info');
            }
            if (Session::has('warning')) {
                $profileFlashClass = 'warning';
                $content = Session::get('warning');
            }
            if (Session::has('danger')) {
                $profileFlashClass = 'danger';
                $content = Session::get('danger');
            }
        @endphp
        @if (Session::has('info') || Session::has('warning') || Session::has('danger'))
            <div class="profile__flash-message {{ $profileFlashClass }}">
                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="profile__flash-message__close">
                            <i class="fa fa-times"></i>
                        </a>
                        @if (!empty($content['title']))
                            <h4 class="profile__flash-message__title">
                                {{ $content['title'] }}
                            </h4>
                        @endif
                        <p class="profile__flash-message__body">
                            {{ $content['message']  }}
                        </p>
                    </div>
                </div>
            </div>
        @endif
        <form
            method="POST"
            action="{{ route('user.update') }}"
            enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-3">
                    <h2 class="profile__title">Foto Profil Anda</h2>
                    <div class="profile__picture-wrapper">
                        <div class="profile__picture">
                            <img id="profilePictureEl" src="{{ Helper::getProfilePicture($user) }}">
                            <div class="profile__picture-action-wrapper">
                                <a href="#" class="profile__picture-action profile__picture-action--cancel"></a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <span class="photo-input__error hide"></span>
                        <label
                            for="photoInput"
                            class="my-button my-button--grey my-button--block my-button--icon-photo">
                            Ganti Foto Profil
                        </label>
                        <input
                            id="photoInput"
                            name="photo"
                            class="photo-input"
                            type="file">
                    </div>
                </div>
                <div class="col-md-6 ml-3">
                    <div>
                        <fieldset>
                            <legend class="profile__title">Informasi personal Anda</legend>
                            <div class="my-form-group {{ $errors->has('name') ? 'my-form--danger' : ''}}">
                                <label for="name">Nama Lengkap</label>
                                <input
                                    id="name"
                                    name="name"
                                    type="text"
                                    class="my-form-control"
                                    value="{{ $user->name }}"
                                    placeholder="Nama Lengkap"
                                    required autofocus>
                            </div>
                        
                            <div class="my-form-group {{ $errors->has('name') ? 'my-form--danger' : ''}}">
                                <label for="name">Email</label>
                                <input
                                    disabled
                                    id="email"
                                    name="email"
                                    type="email"
                                    class="my-form-control"
                                    value="{{ $user->email }}"
                                    placeholder="Alamat Email"
                                    required>
    
                                @if ($errors->has('email'))
                                    <span class="my-form__error">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif
                            </div>
                        
                            <div class="my-form-group {{ $errors->has('name') ? 'my-form--danger' : ''}}">
                                <label for="name">Nomor Telepon</label>
                                <input
                                    id="phone"
                                    name="phone"
                                    type="text"
                                    class="my-form-control"
                                    value="{{ $user->phone }}"
                                    placeholder="e.g. 081223227xxx"
                                    required>
                                
                                @if ($errors->has('phone'))
                                    <div class="my-form__error">
                                        {{ $errors->first('phone') }}
                                    </div>
                                @endif
                            </div>
                            <button type="submit" class="my-button my-button--orange my-button--large">Simpan</button>
                        </fieldset>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>
    {

        let photoInput = document.getElementById('photoInput');
        let profilePictureEl = document.getElementById('profilePictureEl');
        let originalSrc = profilePictureEl.src;
        let cancelBtn = document.querySelector('.profile__picture-action--cancel');
        let formError = document.querySelector('.photo-input__error');
        
        photoInput.addEventListener('change', handlePhotoInput);
        function handlePhotoInput() {
            let reader = new FileReader();
            let photoFile = photoInput.files[0];        
            const MAX_SIZE = 2000000;

            hideError();

            if (photoFile.size > MAX_SIZE) {
                showError('Ukuran gambar tidak boleh lebih dari 2MB.');
                photoInput.value = '';
                return;
            }

            if (photoFile.type !== 'image/jpeg' && photoFile.type !== 'image/png') {
                showError('Gambar harus memiliki ekstensi .png atau .jpg');
                photoInput.value = '';
                return;
            }

            reader.readAsDataURL(photoFile);
            reader.addEventListener('load', function() {
                profilePictureEl.src = reader.result;
                document.querySelector('.profile__picture').classList.add('changed');
            });
        }

        cancelBtn.addEventListener('click', function(e) {
            e.preventDefault();
            
            photoInput.value = '';
            profilePictureEl.src = originalSrc;
            
            document.querySelector('.profile__picture').classList.remove('changed');
        })

        function showError(content) {
            formError.classList.remove('hide');
            formError.innerHTML = content;
        }

        function hideError() {
            formError.classList.add('hide');
        }
    }
</script>
@endsection


