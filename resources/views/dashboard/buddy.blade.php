@extends('frontend.layouts.main')

@section('body-attribute')
data-target="#buddyFormNav"
@endsection

@section('content')
@include('dashboard.components.tab')

@php
     if (!$user->buddy) {
        $nickname = old('nickname');
        $birthyear = old('birthyear', 0);
        $country = old('country');
        $city = old('nickname');
        $city_coverage = old('city_coverage', []);
        $interests = old('interests');
        $skills = old('skills');
        $language = old('language');
        $description = old('description');
        $experience = old('experience');
        $price = old('price');
        $ktp_foto=old('ktp_foto');
        $ktp_selfie=old('ktp_selfie');
       
       
        $data_komplit=old('data_komplit');
    	  $no_rek=old('no_rek');
    	  $nama_rekening=old('nama_rekening');
		$bank=old('bank');
		 
		  $nama_lengkap=old('nama_lengkap');
		  $no_id=old('no_id');
		  $tanggal_lahir=old('tanggal_lahir');

    }
@endphp
 <!--  
         $visa=old('visa');
          $buku_tabungan=old('buku_tabungan');
       $cabang=old('cabang');
       
       
       $permit=old('permit'); 
         -->
<div class="container">
    <div class="row">
        <div class="col-lg-3 sidebar-wrapper">
            <div class="sidebar">
                <nav id="buddyFormNav" class="nav vertical-nav">
                    <ul>
                        <li>
                            <a class="nav-link" href="#personal-information">
                                Informasi Personal
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#domisili">
                                Domisili
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#interests">
                                Ketertarikan (Interest)
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#skills">
                                Kemampuan (Skill)
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#description">
                                Deskripsi Singkat
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#experience">
                                Data Pelengkap
                            </a>
                        </li>
                
                    </ul>
                </nav>
            </div>
        </div>
        <div class="col-md-6">
            <div class="content">
                <form id="buddyForm" 
                action="{{ route('buddy.create') }}" 
                method="POST"
                enctype="multipart/form-data">
                
                    {{ csrf_field() }}
                    <fieldset class="fieldset-bordered">
                        <legend
                            id="personal-information"
                            class="profile__title">Informasi Pribadi</legend>
                        <div class="my-form-group">
                            <label for="interest">Nama Panggilan</label>
                           
                             <input
                               
                                id="nickname"
                                type="text"
                                class="my-form-control"
                                name="nickname"
                                value="{{ $user->buddy ? $user->buddy->nickname : $nickname }}"
                                placeholder="Nama Panggilan"
                                required autofocus>  
                                               </div>
    
                        <div class="my-form-group">
                            <label for="interest">Tahun Lahir</label>
                            <select
                                id="birthyear"
                                name="birthyear"
                                class="my-form-control"
                                required>
                                    @php
                                        $birthyear = $user->buddy ? $user->buddy->birthyear : $birthyear;
                                    @endphp
                                    <option value="" disabled selected>YYYY</option>
                                    @for ($i = date('Y') - 18; $i > 1950; $i--)
                                        <option
                                            value="{{ $i }}"
                                            {{ $i == $birthyear ? 'selected' : ''}}>
                                            {{ $i }}
                                        </option>
                                    @endfor
                            </select>
                            <div class="my-form__help-below">
                                <p>Mencantumkan tanggal lahir membantu memastikan Anda mendapatkan Traveler yang lebih tepat sesuai dengan usia Anda. Untuk rincian selengkapnya, harap kunjungi <a href="{{ url('/syarat-dan-ketentuan') }}">Kebijakan</a> kami.</p>
                            </div>
                        </div>
                    </fieldset>
    
                    <country-city
                        value-country="{{ ($user->buddy ? $user->buddy->country : $country) }}"
                        value-city="{{ ($user->buddy ? $user->buddy->city : $city) }}"
                        :value-city-coverage="{{ ($user->buddy ? json_encode($user->buddy->city_coverage) : json_encode($city_coverage)) }}"
                        value-price="{{ $user->buddy ? $user->buddy->price : $price }}">
                    </country-city>
    
                    <fieldset class="fieldset-bordered">
                        <legend
                            id="interests"
                            class="profile__title">Ketertarikan (Interest)</legend>
                        <p class="my-form__help">
                            Tambahkan hal-hal yang Anda sukai.
                        </p>
                        @php
                            if ($user->buddy) {
                                $interests = $user->buddy->interests;
                                $interests = json_encode($interests);
                            }
                        @endphp
                        <custom-checkbox
                                name="interests[]"
                                id="interests"
                                :selected="{{ $user->buddy ? $interests : '[]' }}"
                                :required="true"
                                :manual-input="true"
                                :checkboxes="[
                                    {
                                        id: 'Sightseeing',
                                        value: 'Sightseeing',
                                        label: 'Sightseeing'
                                    },
                                    {
                                        id: 'Shopping',
                                        value: 'Shopping',
                                        label: 'Shopping'
                                    },
                                    {
                                        id: 'Nature',
                                        value: 'Nature',
                                        label: 'Nature'
                                    },
                                    {
                                        id: 'Culture',
                                        value: 'Culture',
                                        label: 'Culture'
                                    },
                                    {
                                        id: 'foodsanddrinks',
                                        value: 'Foods & Drinks',
                                        label: 'Foods & Drinks'
                                    },
                                    {
                                        id: 'History',
                                        value: 'History',
                                        label: 'History'
                                    },
                                    {
                                        id: 'Education',
                                        value: 'Education',
                                        label: 'Education'
                                    },
                                    {
                                        id: 'Party',
                                        value: 'Party',
                                        label: 'Party'
                                    },
                                    {
                                        id: 'Nightlife',
                                        value: 'Nightlife',
                                        label: 'Nightlife'
                                    }
                                ]">
                            </custom-checkbox>
                    </fieldset>
    
                    <fieldset class="fieldset-bordered">
                        <legend
                            id="skills"
                            class="profile__title">Kemampuan (Skill)</legend>

                          @php
                            $initialLanguages = $user->buddy ? $user->buddy->language : $language;
                            $initialLanguages = explode(', ', $initialLanguages);
                            $initialLanguages = json_encode($initialLanguages);
                          @endphp
                        <div class="my-form-group">
                            <language-picker :initial-languages="{{ $initialLanguages }}"></language-picker>
                        </div>
                        <div class="my-form-group">
                            <label for="skills">Lainnya</label>
                            <input
                                id="skills"
                                type="text"
                                name="skills"
                                class="my-form-control"
                                value="{{ $user->buddy ? $user->buddy->skills : $skills }}"
                                placeholder="e.g Fotografi, Menyetir, etc"
                                required>
                            <p class="my-form__help-below">
                                Tambahkan latar belakang pendidikan dan keterampilan lainnya, seperti: fotografi, SIM internasional, dsb.
                            </p>
                        </div>
                    </fieldset>
    
                    <fieldset class="fieldset-bordered">
                        <legend
                            class="profile__title">Deskripsi Singkat</legend>
                        <p class="my-form__help">
                            Ceritakan secara singkat mengenai diri Anda.
                        </p>
                        <div class="my-form-group">
                            <div class="textarea-with-indicator">
                                <span class="textarea-indicator" data-target="description"></span>
                                <textarea
                                        id="description"
                                        name="description"
                                        class="my-form-control"
                                        maxlength="140"
                                        required>{{ $user->buddy ? $user->buddy->description : $description }}</textarea>
                            </div>
                        </div>
                    </fieldset>
    
                    <fieldset class="fieldset-bordered">
                        <legend
                            class="profile__title">Pengalaman</legend>
                        <p class="my-form__help">
                            Ceritakan pengalaman traveling Anda di luar negeri atau pengalaman lain saat menemani traveller.
                        </p>
                        <div class="my-form-group">
                            <div class="textarea-with-indicator">
                                <span class="textarea-indicator" data-target="experience"></span>
                                <textarea
                                        id="experience"
                                        name="experience"
                                        class="my-form-control"
                                        maxlength="140"
                                        required>{{ $user->buddy ? $user->buddy->experience : $experience }}</textarea>
                            </div>
                        </div>
                    </fieldset>
                    
                     <fieldset class="fieldset-bordered">
                        <legend
                            class="profile__title">Upload Data Diri Pelengkap</legend>
                            <p class="my-form__help">
                           Untuk Keperluan atau Keamanan Menjadi Buddy Anda Harus Memasukan Beberapa Data Pelangkap Diri.
                        </p>
                       </br>
                        <h4>Informasi Tanda Pengenal</h4>
                              		 <label for="interest">Nomor Tanda Pengenal</label>
                                 <input                         
                                id="no_id"
                                type="text"
                                class="my-form-control"
                                name="no_id"
                                value="{{ $user->buddy ? $user->buddy->no_id : $no_id }}"
                                placeholder="Nomor KTP/Passport"
                                required autofocus>  
                                          
                              <label for="interest">Nama Lengkap</label>
                             <input
                               
                                id="nama_lengkap"
                                type="text"
                                class="my-form-control"
                                name="nama_lengkap"
                                value="{{ $user->buddy ? $user->buddy->nama_lengkap : $nama_lengkap }}"
                                placeholder="Nama Lengkap"
                                required autofocus>  
                                              

                                   
                                 <label for="interest">Tanggal Lahir</label>
                           
                             <input
                               
                                id="tanggal_lahir"
                                type="date"
                                class="my-form-control"
                                name="tanggal_lahir"
                                value="{{ $user->buddy ? $user->buddy->tanggal_lahir : $tanggal_lahir }}"
                                placeholder="ta"
                                required autofocus> 
                                <br><br>
                     
                                                </ul>
                        <div class="my-form-group">
                            <div class="textarea-with-indicator">
                                <span class="textarea-indicator" data-target="pelengkap"></span>
                                

                              <!-- <input
                               hidden
                               id="ktp_foto"
                               type="text"
                               class="my-form-control"
                               name="ktp_foto"
                               value="{{ $user->buddy ? $user->buddy->ktp_foto : $ktp_foto }}"
                               placeholder="Nama Panggilan"
                               required autofocus>  
                                             -->
                            </div>
                         <br>	
                    
                      
                        <div class="my-form-group">
                            <div class="textarea-with-indicator">
                                <span class="textarea-indicator" data-target="datadiri"></span>
                                 
                            </div>
                            <br>
                             <h4>Informasi Bank</h4>
                                <p class="my-form__help">
                           Untuk Kemudahan Travelbuddy Melakukan Pembayaran,Mohon Untuk Mengisi Informasi Bank Anda.
                        </p>
                        <br>
                              		<label for="interest">Nomor Rekening</label>
                                 <input                         
                                id="no_rek"
                                type="text"
                                class="my-form-control"
                                name="no_rek"
                                value="{{ $user->buddy ? $user->buddy->no_rek : $no_rek }}"
                                placeholder="Nomor Rekening"
                                required autofocus>  
                                          
                              <label for="interest">Nama Pemilik Rekening</label>
                             <input
                               
                                id="nama_rekening"
                                type="text"
                                class="my-form-control"
                                name="nama_rekening"
                                value="{{ $user->buddy ? $user->buddy->nama_rekening : $nama_rekening }}"
                                placeholder="Nama Pemilik Rekening"
                                required autofocus>  
                                              

                                               <label for="interest">Bank Penerbit</label>
                             <input
                               
                                id="bank"
                                type="text"
                                class="my-form-control"
                                name="bank"
                                value="{{ $user->buddy ? $user->buddy->bank : $bank }}"
                                placeholder="Bank Penerbit"
                                required autofocus>  

                               <!--   <label for="interest">Cabang Bank</label> -->
                           
                            <input
                               hidden
                                id="cabang"
                                type="text"
                                class="my-form-control"
                                name="cabang"
                                value="cabang"
                                placeholder="Cabang Bank"
                                required autofocus>  
                            </div> 
                                             <!--  <h4>Upload Buku Tabungan</h4>

                              	  @if(!empty($user->buddy))
                              	     <img src="{{url($user->buddy->buku_tabungan)}}"style ="width: 40%">

                              @else
                              
                            
                              	     <img src="https://increasify.com.au/wp-content/uploads/2016/08/default-image.png"style ="width: 40%">
                              @endif
                        <img src="https://bilba.go-jek.com/dist/img/identitas/tabungan.png" style="width: 47%;margin-left: 38px;">
                            <ul>
                                                  <li>Informasi Rekening Dapat Diambil Dari Foto Halaman Pertama dan Kedua buku Tabungan atau EBanking yang Mencakup Informasi Nomor Rekening.</li>
                                                  <li>Nama Pemilik rekening (Harus Sama Dengan Nama Buddy), dan Informasi Bank (Nama Bank &amp; Cabang). Foto Tidak Blur.</li>
                                                  <li>Maksimal 2MB per File.</li>
                                                </ul>
                        
                        <div class="my-form-group">
                            <div class="textarea-with-indicator">
                                <span class="textarea-indicator" data-target="datadiri"></span>
                                  <input
                            id="buku_tabungan"
                            name="buku_tabungan"
                            class="my-form-control"
                            type="file"> -->
                         <br><br>   
                       <!-- <h4>Upload Copy Residence Permit</h3>
                       	  @if(!empty($user->buddy))
                              <img src="{{url($user->buddy->visa)}}"style ="width: 40%">
                              @else
                              
                       		<img src="https://increasify.com.au/wp-content/uploads/2016/08/default-image.png"style ="width: 40%">
                       @endif
                       <img src="{{asset('img/icon/permit.jpg')}}" style="width: 47%;margin-left: 38px;">
                       <ul>
                                                  <li>Upload Foto Residence Permit Tempat Anda Tinggal Sekarang</li>
                                                  <li>Foto Tidak Blur.</li>
                                                  <li>Maksimal 2MB per file.</li>
                                                </ul>
                       
                      
                    
                        <div class="my-form-group">
                            <div class="textarea-with-indicator">
                                <span class="textarea-indicator" data-target="datadiri"></span>
                                  <input
                            id="permit"
                            name="permit"
                            class="my-form-control"
                            type="file">

                            <br><br> -->
                           <!--    <h4>Upload Copy Passport</h3>
                             @if(!empty($user->buddy))
                                <img src="{{url($user->buddy->permit)}}"style ="width: 40%">
                              @else
                              
                            <img src="https://increasify.com.au/wp-content/uploads/2016/08/default-image.png"style ="width: 40%">
                              @endif
                              <img src="{{asset('img/icon/passport.jpg')}}" style="width: 47%;margin-left: 38px;">
                            <ul>
                                                  <li>Informasi Passport Dapat Diambil Dari Foto Halaman Yang Menampilkan Seperti Contoh Gambar Diatas </li>
                                                  <li>Foto tidak blur.</li>
                                                  <li>Maksimal 2MB per file.</li>
                                                </ul>
                        
                        <div class="my-form-group">
                            <div class="textarea-with-indicator">
                                <span class="textarea-indicator" data-target="datadiri"></span>
                                  <input
                            id="permit"
                            name="permit"
                            class="my-form-control"
                            type="file">
 -->
                

                        </div>
                    </fieldset> 
                    <fieldset>
                        <legend class="profile__title">Simpan Profil Buddy</legend>
                        <button type="submit"class="my-button my-button--peach my-button-large">Simpan Profil Buddy</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        let form = document.getElementById('buddyForm')
        form.addEventListener('keypress', (e) => {
            if (e.keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        let indicators = document.querySelectorAll('.textarea-indicator');
        indicators.forEach(indicator => {
          let target = document.getElementById(indicator.dataset.target);
          indicator.innerText = `${target.value.length}/140`;
          target.addEventListener('input', (e) => {
            e.target.value = e.target.value.substring(0, 140);
            indicator.innerText = `${e.target.value.length}/140`;
          })
        });
        
    </script>
@endsection