@extends('mail.order.layout')

@section('content')
<div class="email__header">
    <img src="{{ url('img/email/header.jpg') }}" alt="">

    <h1>Konfirmasi Email Akun Travel Buddy</h1>
</div>

<div class="email__body">
    <p>Selamat datang {{ $user->name }}!</p>
    <p>Terima Kasih telah mendaftar di Travel Buddy. klik tombol di bawah ini untuk mengkonfirmasi alamat email Anda.</p>

    <a href="{{ url('verifyemail/' . $user->email_token) }}" class="my-button my-button--large">Konfirmasi Email</a>

    <p>
        atau klik tautan di bawah ini.
        <br>
        <a href="{{ url('verifyemail/' . $user->email_token) }}">{{ url('verifyemail/' . $user->email_token) }}</a>
    </p>
    
    <p>
        Email ini dikirim oleh sistem notifikasi Travel Buddy, <strong>mohon untuk tidak membalas email ini.</strong>
        Jika Anda memiliki pertanyaan lebih lanjut, hubungi kami melalui email:
        <a href="mailto:support@travelbuddy.id">support@travelbuddy.id</a>
        atau telepon:
        <a href="tel:+62 877 8913 6398">+62 877 8913 6398</a>
    </p>
</div>
@endsection
