@extends('mail.order.layout')

@section('content')
<div class="email__header">
    <img src="{{ url('img/email/header.jpg') }}" alt="">

    <h1>Reset Password Akun Travel Buddy</h1>
</div>

<div class="email__body">
    <p>Email ini dikirimkan karena kami menerima permintaan untuk melakukan reset password terhadap akun Anda.</p>
    <p>Untuk mereset password Anda, silahkan klik tombol berikut.</p>

    <a href="{{ route('password.reset', $token) }}" class="my-button my-button--large">Reset Password</a>

    <p>
        atau klik tautan di bawah ini.
        <br>
        <a href="{{ route('password.reset', $token) }}">{{ route('password.reset', $token) }}</a>
    </p>
    
    <p>
        Email ini dikirim oleh sistem notifikasi Travel Buddy, <strong>mohon untuk tidak membalas email ini.</strong>
        Jika Anda memiliki pertanyaan lebih lanjut, hubungi kami melalui email:
        <a href="mailto:support@travelbuddy.id">support@travelbuddy.id</a>
        atau telepon:
        <a href="tel:+62 877 8913 6398">+62 877 8913 6398</a>
    </p>
</div>
@endsection
