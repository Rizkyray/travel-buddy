@extends('mail.order.layout')

@section('content')
@php
    $ppn = round(((int) $order->price_estimation * 0.05));
    $totalPrice = $order->price_estimation + $ppn;
@endphp
<div class="email__header">
    <img src="{{ url('img/email/header.jpg') }}" alt="">

    <h1>Travel Buddy</h1>
    <h2>Konfirmasi Order Booking Travel Buddy</h2>
</div>


<div class="email__body">
    <p>
        Halo, <strong>{{ $order->name }}</strong><br>
        Kami telah menerima Order anda dengan detail sebagai berikut :
    </p>

    <p class="order-number">
        Nomor Order <strong>{{ $order->order_number }}</strong>
    </p>
    
    <table>
        <tr>
            <td class="sub-heading">Amount Due</td>
            <td>: <strong>{{ Helper::formatCurrency($totalPrice) }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Due Date</td>
            <td>: <strong>Sabtu, {{ Carbon::now()->addDays(5)->formatLocalized('%A, %e %B %Y') }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Kota Tujuan</td>
            <td>: <strong>{{ $order->trip_details['destination'] }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Buddy</td>
            <td>: <strong>{{ $order->buddy->nickname }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Tanggal Perjalanan</td>
            <td>: <strong>{{ (new Carbon($order->start_date))->formatLocalized('%e %B %Y') }} – {{ (new Carbon($order->end_date))->formatLocalized('%e %B %Y') }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Rombongan</td>
            <td>: <strong>{{ $order->trip_details['total_adults'] }}{{ ' orang dewasa' }}@if ((int) $order->trip_details['total_children'] > 0), {{ $order->trip_details['total_children'] }} orang anak @endif</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Tujuan Perjalanan</td>
            <td>: <strong>@foreach ($order->trip_details['trip_type'] as $index => $type)
            {{ $type }}@if ($index !== count($order->trip_details['trip_type']) - 1),@endif
             @endforeach</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Deskripsi Perjalanan</td>
            <td>:</td>
        </tr>
        <tr>
            <td colspan="2"><strong>{{ $order->trip_details['trip_description'] }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Pesan untuk Buddy</td>
            <td>:</td>
        </tr>
        <tr>
            <td colspan="2"><strong>{{ $order->message_to_buddy }}</strong></td>
        </tr>
    </table>

    <h3 class="heading">Rincian Biaya</h3>
    @php
        $startDate = new Carbon($order->start_date);
        $endDate = (new Carbon($order->end_date))->addDay();

        $interval = new DateInterval('P1D');
        $dateRange = new DatePeriod($startDate, $interval ,$endDate);
    @endphp

    <table>
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Waktu</th>
                <th>Harga</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dateRange as $index => $date)
                @php
                    $dt = new Carbon($date);
                    $formatedDate = $dt->formatLocalized('%e %B %Y');
                @endphp
                <tr class="table__row">
                    <td>{{ $formatedDate }}</td>
                    <td>{{ $order->hours_per_day[$index] }} Jam</td>
                    <td>{{ Helper::formatCurrency((int) $order->price_per_day * (int) $order->hours_per_day[$index]) }}</td>
                </tr>
            @endforeach
            <tr class="no-border-bottom">
                @php
                    $totalHours = collect($order->hours_per_day)->sum();
                @endphp
                <td class="sub-heading">SUB-TOTAL <strong>({{ $index + 1 }} Hari)</strong></td>
                <td><strong>{{ $totalHours }} Jam</strong></td>
                <td><strong>{{ Helper::formatCurrency($order->price_estimation) }}</strong><br></td>
            </tr>
            <tr>
                <td class="sub-heading">ADM 5.00% PPN</td>
                <td></td>
                <td><strong>{{ Helper::formatCurrency($ppn) }}</strong><br></td>
            </tr>
            <tr class="no-border-bottom">
                <td class="sub-heading">Total</td>
                <td></td>
                <td><strong>{{ Helper::formatCurrency($totalPrice) }}</strong><br></td>
            </tr>
        </tbody>
    </table>

    <h3 class="heading">Pembayaran</h3>
    <p>Segera lakukan pembayaran ke no rekening kami dibawah ini</p>

    <div class="bank-list__item">
        <img src="{{ url('img/banks/bca.png') }}" alt="" class="bank__logo">
        <div class="bank__body">
            <p class="bank__title">BCA</p>
            <p class="bank__info">
                A.N. Rizky Sarakhsi Ersaid Lasabuda
                <br>
                No. Rekening: 6041758349
            </p>
        </div>
    </div>


    <div class="bank-list__item">
        <img src="{{ url('img/banks/rabobank.png') }}" alt="" class="bank__logo">
        <div class="bank__body">
            <p class="bank__title">RABOBANK</p>
            <p class="bank__info">
                A.N. RSE LASABUDA<br>
                IBAN: NL58 RABO 166376612<br>
                BIC: RABONL2U<br>
            </p>
        </div>
    </div>

    <div class="bank-list__item">
        <img src="{{ url('img/banks/jenius.png') }}" alt="" class="bank__logo">
        <div class="bank__body">
            <p class="bank__title">JENIUS (Bank BTPN)</p>
            <p class="bank__info">
                A.N. Rizky Sarakhsi Ersaid Lasabuda<br>
                No. Rekening: 90011539249<br>
                $rizkylasabuda<br>
            </p>
        </div>
    </div>

    <p>Catatan : Sertakan nomor order di berita transfer, harap memeriksa A.n sebelum memproses transaksi anda</p>
    <p>Setelah melakukan pembayaran harap konfirmasi melalui :</p>
    <ul>
        <li>email ke billing@travelbuddy.id</li>
        <li>atau telpon ke +62 87789136398</li>
    </ul>

    <hr>

    <div class="email-footer">
        <div class="logo">
            <img src="{{ url('img/logo.png') }}" alt="">
        </div>

        <p>
            Regards, <span class="travelbuddy">TravelBuddy</span><br>
            Jika ada permasalahan, silahkan hubungi customer support kami
        </p>
        
        <p class="contact">
            <i><svg width="14" height="14" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1600 1240q0 27-10 70.5t-21 68.5q-21 50-122 106-94 51-186 51-27 0-53-3.5t-57.5-12.5-47-14.5-55.5-20.5-49-18q-98-35-175-83-127-79-264-216t-216-264q-48-77-83-175-3-9-18-49t-20.5-55.5-14.5-47-12.5-57.5-3.5-53q0-92 51-186 56-101 106-122 25-11 68.5-21t70.5-10q14 0 21 3 18 6 53 76 11 19 30 54t35 63.5 31 53.5q3 4 17.5 25t21.5 35.5 7 28.5q0 20-28.5 50t-62 55-62 53-28.5 46q0 9 5 22.5t8.5 20.5 14 24 11.5 19q76 137 174 235t235 174q2 1 19 11.5t24 14 20.5 8.5 22.5 5q18 0 46-28.5t53-62 55-62 50-28.5q14 0 28.5 7t35.5 21.5 25 17.5q25 15 53.5 31t63.5 35 54 30q70 35 76 53 3 7 3 21z"/></svg></i>
            <a href="tel:+62%20877-8913-6398">+62 87789136398</a>
        </p>        

        <p class="contact">
            <i><svg width="14" height="14" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z"/></svg></i>
            <a href="mailto:support@travelbuddy.id">support@travelbuddy.id</a>
        </p>
        
        <div class="social-media-list">

            <a href="/" class="social-media">
                <i><svg width="24" height="24" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M711 1128l484-250-484-253v503zm185-862q168 0 324.5 4.5t229.5 9.5l73 4q1 0 17 1.5t23 3 23.5 4.5 28.5 8 28 13 31 19.5 29 26.5q6 6 15.5 18.5t29 58.5 26.5 101q8 64 12.5 136.5t5.5 113.5v176q1 145-18 290-7 55-25 99.5t-32 61.5l-14 17q-14 15-29 26.5t-31 19-28 12.5-28.5 8-24 4.5-23 3-16.5 1.5q-251 19-627 19-207-2-359.5-6.5t-200.5-7.5l-49-4-36-4q-36-5-54.5-10t-51-21-56.5-41q-6-6-15.5-18.5t-29-58.5-26.5-101q-8-64-12.5-136.5t-5.5-113.5v-176q-1-145 18-290 7-55 25-99.5t32-61.5l14-17q14-15 29-26.5t31-19.5 28-13 28.5-8 23.5-4.5 23-3 17-1.5q251-18 627-18z"/></svg></i>
            </a>
            
            <a href="/" class="social-media">
                <i><svg width="24" height="24" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1408 610q-56 25-121 34 68-40 93-117-65 38-134 51-61-66-153-66-87 0-148.5 61.5t-61.5 148.5q0 29 5 48-129-7-242-65t-192-155q-29 50-29 106 0 114 91 175-47-1-100-26v2q0 75 50 133.5t123 72.5q-29 8-51 8-13 0-39-4 21 63 74.5 104t121.5 42q-116 90-261 90-26 0-50-3 148 94 322 94 112 0 210-35.5t168-95 120.5-137 75-162 24.5-168.5q0-18-1-27 63-45 105-109zm256-194v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z"/></svg></i>
            </a>
            
            <a href="/" class="social-media">
                <i><svg width="24" height="24" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1152 896q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm138 0q0 164-115 279t-279 115-279-115-115-279 115-279 279-115 279 115 115 279zm108-410q0 38-27 65t-65 27-65-27-27-65 27-65 65-27 65 27 27 65zm-502-220q-7 0-76.5-.5t-105.5 0-96.5 3-103 10-71.5 18.5q-50 20-88 58t-58 88q-11 29-18.5 71.5t-10 103-3 96.5 0 105.5.5 76.5-.5 76.5 0 105.5 3 96.5 10 103 18.5 71.5q20 50 58 88t88 58q29 11 71.5 18.5t103 10 96.5 3 105.5 0 76.5-.5 76.5.5 105.5 0 96.5-3 103-10 71.5-18.5q50-20 88-58t58-88q11-29 18.5-71.5t10-103 3-96.5 0-105.5-.5-76.5.5-76.5 0-105.5-3-96.5-10-103-18.5-71.5q-20-50-58-88t-88-58q-29-11-71.5-18.5t-103-10-96.5-3-105.5 0-76.5.5zm768 630q0 229-5 317-10 208-124 322t-322 124q-88 5-317 5t-317-5q-208-10-322-124t-124-322q-5-88-5-317t5-317q10-208 124-322t322-124q88-5 317-5t317 5q208 10 322 124t124 322q5 88 5 317z"/></svg></i>
            </a>
            
            <a href="/" class="social-media">
                <i><svg width="24" height="24" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1376 128q119 0 203.5 84.5t84.5 203.5v960q0 119-84.5 203.5t-203.5 84.5h-188v-595h199l30-232h-229v-148q0-56 23.5-84t91.5-28l122-1v-207q-63-9-178-9-136 0-217.5 80t-81.5 226v171h-200v232h200v595h-532q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960z"/></svg></i>
            </a>
        </div>
</div>
@endsection
