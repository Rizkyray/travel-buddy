@extends('mail.order.layout')

@section('content')
@php
    $ppn = round(((int) $order->price_estimation * 0.05));
    $totalPrice = $order->price_estimation + $ppn;
@endphp
<div class="email__header">
    <img src="{{ url('img/email/header.jpg') }}" alt="">

    <h1>Travel Buddy</h1>
    <h2>Order Baru</h2>
</div>


<div class="email__body">
    <p>
        Halo, <strong>{{ $order->buddy->nickname }}</strong><br>
        Anda memiliki order baru dengan detail sebagai berikut:
    </p>

    <p class="order-number">
        Nomor Order <strong>{{ $order->order_number }}</strong>
    </p>
    
    <table>
        <tr>
            <td class="sub-heading">Kota Tujuan</td>
            <td>: <strong>{{ $order->trip_details['destination'] }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Pemesan</td>
            <td>: <strong>{{ $order->name }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Tanggal Perjalanan</td>
            <td>: <strong>{{ (new Carbon($order->start_date))->formatLocalized('%e %B %Y') }} – {{ (new Carbon($order->end_date))->formatLocalized('%e %B %Y') }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Rombongan</td>
            <td>: <strong>{{ $order->trip_details['total_adults'] }}{{ ' orang dewasa' }}@if ((int) $order->trip_details['total_children'] > 0), {{ $order->trip_details['total_children'] }} orang anak @endif</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Tujuan Perjalanan</td>
            <td>: <strong>@foreach ($order->trip_details['trip_type'] as $index => $type)
            {{ $type }}@if ($index !== count($order->trip_details['trip_type']) - 1),@endif
             @endforeach</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Deskripsi Perjalanan</td>
            <td>:</td>
        </tr>
        <tr>
            <td colspan="2"><strong>{{ $order->trip_details['trip_description'] }}</strong></td>
        </tr>
        <tr>
            <td class="sub-heading">Pesan untuk Buddy</td>
            <td>:</td>
        </tr>
        <tr>
            <td colspan="2"><strong>{{ $order->message_to_buddy }}</strong></td>
        </tr>
    </table>

    <h3 class="heading">Rincian Biaya</h3>
    @php
        $startDate = new Carbon($order->start_date);
        $endDate = (new Carbon($order->end_date))->addDay();

        $interval = new DateInterval('P1D');
        $dateRange = new DatePeriod($startDate, $interval ,$endDate);
    @endphp

    <table>
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Waktu</th>
                <th>Harga</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dateRange as $index => $date)
                @php
                    $dt = new Carbon($date);
                    $formatedDate = $dt->formatLocalized('%e %B %Y');
                @endphp
                <tr class="table__row">
                    <td>{{ $formatedDate }}</td>
                    <td>{{ $order->hours_per_day[$index] }} Jam</td>
                    <td>{{ Helper::formatCurrency((int) $order->price_per_day * (int) $order->hours_per_day[$index]) }}</td>
                </tr>
            @endforeach
            <tr class="no-border-bottom">
                @php
                    $totalHours = collect($order->hours_per_day)->sum();
                @endphp
                <td class="sub-heading">SUB-TOTAL <strong>({{ $index + 1 }} Hari)</strong></td>
                <td><strong>{{ $totalHours }} Jam</strong></td>
                <td><strong>{{ Helper::formatCurrency($order->price_estimation) }}</strong><br></td>
            </tr>
            <tr>
                <td class="sub-heading">ADM 5.00% PPN</td>
                <td></td>
                <td><strong>{{ Helper::formatCurrency($ppn) }}</strong><br></td>
            </tr>
            <tr class="no-border-bottom">
                <td class="sub-heading">Total</td>
                <td></td>
                <td><strong>{{ Helper::formatCurrency($totalPrice) }}</strong><br></td>
            </tr>
        </tbody>
    </table>

    <h3 class="heading">Persetujuan</h3>
    <p>Anda dapat menyetujui/menolak order ini melalui dashboard Travel Buddy</p>
    <a href="{{ route('dashboard.order') }}" class="my-button my-button--orange my-button--large">Go to Dashboard</a>
    <p>
        atau klik tautan di bawah ini.
        <br>
        <a href="{{ route('dashboard.order') }}">{{ route('dashboard.order') }}</a>
    </p>

    <hr>

    <div class="email-footer">
        <div class="logo">
            <img src="{{ url('img/logo.png') }}" alt="">
        </div>

        <p>
            Regards, <span class="travelbuddy">TravelBuddy</span><br>
            Jika ada permasalahan, silahkan hubungi customer support kami
        </p>
        
        <p class="contact">
            <i><svg width="14" height="14" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1600 1240q0 27-10 70.5t-21 68.5q-21 50-122 106-94 51-186 51-27 0-53-3.5t-57.5-12.5-47-14.5-55.5-20.5-49-18q-98-35-175-83-127-79-264-216t-216-264q-48-77-83-175-3-9-18-49t-20.5-55.5-14.5-47-12.5-57.5-3.5-53q0-92 51-186 56-101 106-122 25-11 68.5-21t70.5-10q14 0 21 3 18 6 53 76 11 19 30 54t35 63.5 31 53.5q3 4 17.5 25t21.5 35.5 7 28.5q0 20-28.5 50t-62 55-62 53-28.5 46q0 9 5 22.5t8.5 20.5 14 24 11.5 19q76 137 174 235t235 174q2 1 19 11.5t24 14 20.5 8.5 22.5 5q18 0 46-28.5t53-62 55-62 50-28.5q14 0 28.5 7t35.5 21.5 25 17.5q25 15 53.5 31t63.5 35 54 30q70 35 76 53 3 7 3 21z"/></svg></i>
            <a href="tel:+62%20877-8913-6398">+62 87789136398</a>
        </p>        

        <p class="contact">
            <i><svg width="14" height="14" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z"/></svg></i>
            <a href="mailto:support@travelbuddy.id">support@travelbuddy.id</a>
        </p>
</div>
@endsection
