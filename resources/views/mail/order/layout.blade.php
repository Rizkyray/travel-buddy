<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style>
        body {
            font-family: Helvetica, Arial, sans-serif;
            line-height: 1.5;
            color: rgb(25, 25, 25);
            background-color: #FAFBFB;
            padding-top: 40px;
            padding-bottom: 40px;
            font-size: 14px;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            background-color: #FAFBFB;
            border: 1px solid rgb(230, 230, 230);
        }
        
        .email__header {
        }

        .email__title {
        }

        .email__title-text {
            font-size: 16px;
            margin: 0;
        }

        @media screen and (min-width: 500px) {
            .email__title {
                border-radius: 24px;
            }
            .email__title-text {
                font-size: 20px;
            }
        }

        .order-number,
        .heading {
            font-size: 20px;
            color: rgba(36, 36, 36, 0.5);
            font-weight: bold;
        }

        .sub-heading {
            font-size: 14px;
            color: rgba(36, 36, 36, 0.75);
            font-weight: bold;
        }

        .sub-heading strong {
            color: #262626;
        }

        td.sub-heading {
            width: 150px;
        }


        .order-number strong {
            color: #ef4a02;
        }

        .email__body {
            padding: 24px;
        }

        h1,
        h2,
        h3,
        h4,
        h5 {
            color: rgb(36, 36, 36);
        }

        .bank-list {
            padding-left: 0;
            display: block;
        }
        .bank-list__item {
            list-style: none;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            margin-left: -12px;
            margin-right: -12px;
            margin-bottom: 12px;
        }
        .bank-list__item > * {
            margin-right: 12px;
            margin-left: 12px;
        }

        .bank__body {
            flex: 1 0 200px;
        }

        .bank__logo {
            width: 180px;
            flex: 0 0 180px;
            max-width: 180px;
            max-height: 120px;
            border-radius: 8px;
            border: 1px solid rgb(230, 230, 230);
        }

        .bank__title {
            font-size: 16px;
            margin-bottom: 0;
            font-weight: bold;
            margin-bottom: 0;
            margin-top: 0;
        }

        .bank__info {
            margin-top: 0;
        }

        .email__header {
            text-align: center;
        }

        img {
            max-width: 100%;
        }

        table {
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }

        th {
            width: 33.3%;
            color: rgba(36, 36, 36, 0.75);
        }

        td {
            padding-top: 8px;
            padding-bottom: 8px;
            border-bottom: 1px solid rgb(230, 230, 230);
        }

        tr.no-border-bottom td {
            border-bottom: none;
        }

        hr {
            margin-top: 24px;
            margin-bottom: 24px;
            border: none;
            border-bottom: 1px solid #b0bec5;
        }
        
        .email-footer p {
            color: rgba(38, 38, 38, 0.75);
            font-size: 13px;
            font-weight: bold;
            margin-top: 0;            
        }

        .email-footer .contact {
            margin-bottom: 8px;
        }

        .email-footer .contact svg {
            margin-right: 4px;
        }

        .email-footer svg {
            fill: rgba(38, 38, 38, 0.75);
        }

        .email-footer .social-media {
            margin-right: 12px;
            text-decoration: none;
        }

        .email-footer .logo {
            width: 200px;
            margin-top: 24px;
            margin-bottom: 24px;
        }


        span.travelbuddy {
            color: #ff8352
        }

        a {
            color: #ff8352;
            font-weight: normal;
        }

        .social-media-list {
            margin-top: 24px;
        }

        .my-button {
            display: inline-block;
            padding: 12px 16px;
            border-radius: 4px;
            background-color: #ef4a02;
            text-decoration: none;
            color: #FFF;
        }

        .my-button--large {
            display: block;
            width: 300px;
            max-width: 100%;
            margin: 0 auto;
            text-align: center;
            margin-bottom: 40px;
            font-size: 20px;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
</body>
</html>