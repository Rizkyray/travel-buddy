@extends('frontend.layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="login-form">
                <div class="login-form__header">
                    <h1 class="login-form__title">
                        Kirim Ulang Verifikasi
                    </h1>
                </div>
                <form class="login-form__form" method="POST" action="{{ url('register/verification/resend') }}">
                    {{ csrf_field() }}               
                    <div class="my-form-group my-form-group--icon-mail{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email</label>
                        <input
                            id="email"
                            type="email"
                            class="my-form-control"
                            name="email"
                            value="{{ old('email') }}"
                            placeholder="e.g username@domain.com"
                            required>
                            
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                    </div>
                    <button type="submit" class="login-form__submit">Kirim Ulang Verifikasi</button>
                    <div class="login-form__footer">
                        <p>Sudah punya akun Travel Buddy? <a href="">Login di sini</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection