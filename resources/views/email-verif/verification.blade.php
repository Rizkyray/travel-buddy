@extends('frontend.layouts.plain')

@section('content')
    <div class="popup">
        <div class="popup__dialog">
            <div class="popup__content">
                <h1 class="popup__title">
                    Terimakasih Sudah Mendaftar
                </h1>
                <p class="popup__body">
                    Happy travelling dengan Travel Buddy!
                    <br>
                    <br>
                    Silakan cek email Anda untuk melakukan verifikasi email.
                    <br>
                    <br>
                    Email mungkin memerlukan waktu 3 hingga 5 menit sampai terkirim. Periksa juga folder spam Anda.
                    <br>
                    <br>
                    Masih belum menerima email? <a href="{{ url('register/verification/resend') }}">kirim ulang</a>.
                </p>
            </div>
            <div class="popup__navigation">
                <a href="{{ url('/') }}" class="my-button my-button--orange">Kembali ke Halaman Utama</a>
            </div>
        </div>
    </div>
@endsection