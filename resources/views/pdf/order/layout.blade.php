<!DOCTYPE html>
<html lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <style>
        body {
            font-family: Helvetica, Arial, sans-serif;
        }
        
        p {
            margin: 0;
        }

        .clearfix {
            overflow: auto;
        }

        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }
        
        .logo {
            margin-bottom: 64px;
        }

        .logo-img {
            width: 300px;
        }
        
        .footer {
            margin-top: 40px;
            text-align: center;
        }

        .footer-img {
            width: 100px;
        }

        .status-wrapper {
            margin-bottom: 24px;
        }

        .status {
            width: 200px;
            float: right;
            background-color: #DF554A;
            border: 4px solid #AB302A;
        }

        .status.unpaid {
            background-color: #DF554A;
            border: 4px solid #AB302A;
        }


        .status h2 {
            text-align: center;
            margin: 8px 0;
            color: #FFF;
            font-size: 20px;
        }
        
        .header {
            text-align: right;
        }

        .header__company-name {
            text-transform: uppercase;
            font-size: 16px;
            font-weight: bold;
        }
        
        .invoice {
            margin-bottom: 24px;
        }
        
        .invoice__title {
            font-size: 18px;
            font-weight: bold;
            margin: 0;        
        }
        
        .invoiced-to {
            margin-bottom: 24px;
        }

        .invoiced-to__title {
            font-size: 16px;
            font-weight: bold;
            margin: 0;
        }

        .payment-detail {
            margin-bottom: 24px;
        }
        
        table {
            margin-bottom: 24px;
            border-collapse: collapse;
        }

        table.syled, .styled th, .styled td {
            border: 1px solid #F8D6BF;
        }

        .styled th {
            padding: 4px 16px;
        }

        .styled td {
            padding: 4px 16px;
        }

        td, th {
            padding-top: 4px;
            padding-bottom: 4px;
        }

        .align-right {
            text-align: right;
        }

        .align-left {
            text-align: left;
        }

        .footer .price {
            text-align: right;
        }

        .footer__title {
            font-weight: bold;
            text-align: right;
        }
        .styled thead, .styled .footer {
            background-color: #F4B083;
        }

        .styled thead th {
            text-align: left;
        }
   </style>
   @yield('style')
</head>
<body>
    @yield('content')
</body>
</html>