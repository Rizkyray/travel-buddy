@extends('pdf.order.layout')

@section('content')
    @php
        $ppn = round(((int) $order->price_estimation * 0.05));
        $totalPrice = $order->price_estimation + $ppn;
    @endphp

    <div class="container">      
        
        <div class="clearfix status-wrapper">
            <div class="status unpaid">
                <h2>UNPAID</h2>
            </div>
        </div>

        <div class="logo">
            <img class="logo-img" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaoAAABCCAMAAAAxMWKfAAAA8FBMVEUAAAAmJibwSwMvLy8nJyc0NDQoKCjvSwMoKCjxSgInJyf/UBkoKCjyTgQmJiYnJyfwSgLwSwLwSwLwSwMmJibwTAMoKCgnJyfwSgLwSwIoKCjzTAZDQ0PwSwLyUw2rgIDwSgLwSgImJib/YyUoKCj/cELySwQoKCgnJyfxSwPwSwLwSgPwSwPxSgQnJyf0TAXwSgLwSgPwSwPwSgImJibxTgfwSgImJiYmJiYvLy8oKCj3Tgj9VBInJycnJyfxTAP1TgrwSwTySwPwSwPwSwLvTAMrKyvwSgPxSwLySwTyTQYsLCzwSwPwTAUpKSnvSgJyubitAAAAT3RSTlMAgL4PMwhMwzh/OwofO3NX++HnmXlUKUnbZkAqA9YSAvbyfQYtAz4TZl56yKSKRC/ttqB3cCLOa10KJB0NYVFGGIVNlHJRHK5rNyYWqjMYNto7dAAACaxJREFUeNrtnGlb4jAQgKdcC8gCajlWueQQWBEvlEsQFVgVtf//3+yu0kySadFI3afu0/eTkGn1mdckk6QAi8B7yYGrCKaXPAPh5oc1zdnV0yV8DrXUkn1nY5Gw9l6C4Cr82pIQEL4Z9iTzreZn6NowlvhUYj1VxmpKrV0AT5UtLlJlGMnDDU+VHe5SZRj5oafKBrepMrJDT5U1rlNl5CueKkvcp8o481RZ4kJVpeGXUuWPiWSYmoHUUgRX8S5Vv/YFbs5aF7yr6ZdSJRN2ay/6kKptIOxnUdWBp8oKt6iCjUd0NfFUWeAaVVDYYqq+e6oscI8qaDJVqf9dVX1QDVQHfrAgWJ8P0jF/DpyhP6iG2+1oIP1cdFDVkKm6W8fN+f1N6n5C068Q67iq4M6Svwb8oc4yoBNaAE9u0G5oS0aB18vNK2MZkzRQcpat83ZDY+iJQN8pVZAUS8Bp5JUprUEiS6SyvnZ1eLC8yfHpTYWkXyH2MsK4pGM1a7taqYpmZQHFsK5xJOroM9zReMrtHEDOfBVNaKbgIhCqmskcTGIjTULfCTqj6lwaAM25axNkflpOapW7TYNnb1qRVKnE4vKhCTJnOKsqqkpDRk5f2sx2XJPpzqHPVA1wvQYEZqXBfmVPsyBedUTVD8Pk6SOqZmY8kr+SVCnE4l9zAhI1pvkAFFXtBDSZ9usIFtIsKM/rTBV0tSU9kFloJqaKGIoX2XFA1cOe2XxcUVdVOzSsSImqFGIv2Z+TLIDIDXY4VVXdMhmU+i+DX0OzJJ5GVWi5DhIhFr8sRwJlzY5QcU1VlSaOSNegrKp2YrNJdYPpV4zdtq1y2PVbFSVV9v/lxYRds46qgjp2RAHaMtBWEFVR9bgrcJ+aHm5yKZuoq7o1bNjD9CvGHrH3v0ndnzVEYG1V8ZegjPYmUS5Kz4EA9rfX+r+uaysoLxzbrk2Bsqo74018oBp7wF6LheYhGxkna6nCf/GYmMrGuD3u6TSwzl4EgAdnsQT8pdjQkE4omo6loyGuumwUHVI1BWVVk5LUPY5tVSnENq2PZTbYBS34oKpeJpyuRntlc24ZaUio/prIYDVOnPZInQei6ZhZubPAqjkz5apMqJZ2RNVWE9RV3fI3iMzOAYY3kSxJv2IsOrmwLlTvP6YqsYBX6j0tLM0s3Rgw+j1ZFQbOgWPMrn7tVOhEkJBj90s4oWq7AOqqhlxHOWU32DhMkvSrxWKhuAvIBanU1VSlAam+dKoeWswBT1RSBWwUGwPSL4vjYlozyQDCl5nlvgOq8j8q6qoi7PLjK+B4ypL0K8U+cDUp455U6mqq2iATZKmO90FkLKkKa5hsKlQPiheNQOLZbAk7Mldd7CqrOrDL3m6JpF8lFnwG/RNapFJXUtXIgUyadDi8ThdVBXV8yehInShOdjVI6NiZsmLvQVHVZdJ2X+GMT79qLECKbiAVSqRSV1KVBkIIpxDCDqoSY7tYVEgr4znWHoQ2NjlSrGcLaqpmTPIQJIrfuPSrxgJU2P7TLRFamnxIVR0IoxUag6hK2kF6JqZ70iJLjxN0s+n9z1YcCXxvXvuEWb2mogpfnQLhiku/SuySazax1ZY+N8kNlFSVi0Domm0YLzWiKujJc15Ol8a7He0d9NfYWNq95eaKppKqwxUnkhVuB0Ihlm72X8nbf0/KquigJM4tHbAggarEiS1exDeEyzPaO3hWUEU52sLSQknVKZbUlANMv0Is3e47lV4fwIdUJYBQ5DRS2pKqYgc7keQS5b7NYs3zqizO4gqqMH0FoPzC9CvGSnPbS703SWKvdEpVjqutKVE0Ib0xFhdVelBFVX09VbDLEnG7WlUEVfEl9QZQWph+lVh6NHUDf5iy6r3mmCrQsaijhARV1E2Y5DukvQP/mqrgRNjLNrO0BzKngqpH3FWl+DD9KrGMiLDhlzdf/QTnVHWwLKOMZFUQEk4RGzikLdn5VFWk4NoSdnAmIIIt34VdvX2gZDH9KrGMYZIbAZ9YpV5wUFUD53qKTlQ988V5HUdP8vsTAXty66r6yVIBf/ChEJFiSWiJWGz/0BLOB0qxdAKbcaJPwUFVPTxlJMw1ogpGXNdo0zVZDO2tYF1VLWEj59Fus+0Jyw/hsCJPJ5AzLv1KsXS19QgVttV+5KQq3MPz0x5noSrNvdXhDurJQsv/eaomJaEUvsZXIj5R1cT+4z7ne1z6VWKRYtYcATdSOJU6qWqu2bZWNQtVuTirQ2KkQ5JV8qeoGuLzXL/w/5lWxjMDVYlTV+mB7hVh+tViaWdLnbCfHFVV1MXnjZB+R1BFCod5CDfakTC5HbJwRNUVdxx7Jm6tZiuAFDZlVYf4FESFDGmYfpVYBNdS35JYqTuiih516AFAxAfEohYHVCFT8tj6oZhyGgSCCa26rqrC/XXe4BhKBxYH52Cyb5pCVbgeM/L33E1b5LhQIRZ5pB+rdFbVQkNGdZzDypqgirolh8KkXO/FODU7f9VX1VTtZQU2MX3C5NTEd45Tr31g4yfGYm14yn+dwiW8UGsek0N4lVhk3xApFZxTRVNfTkQH/px/vtPVBKJkdkMa9FkzdN8OxPz+50F7tDRfVVD1Nil2BoGULran179w6udVnSfF75U5u9s+KJFHWxRjkbzYug1Oq6qXtTeJksIQqZJqZBUBB1U98vMHhaqCyHseGFOMRaZi65HjqiCgogpdYKUu0V7tyjFVW2yAqR28UxX8Uki/Qiye/JJGJ1XZn1yUw0QV1uvWRTl5WpcwdkpV/lxa61BaRNXGhWHNaRIzrB6L8xsy+wxVxbFNX/ITVWSjr+wHQg7dU/vgkCpfQZjSLVwlmxtEFVxa95WT2iZJv1IsPqWElbrTquw/EBCCnLUqP0bb3LOqa5Z0YuCMqvyMHjhKbO0DHDNVSCRpEC4u2S/1gXIsckEe/nVcFcxH9GMg3H5t1LZojIEl/oyFrG41B06oOm7NiiBT2GZpxQL7AlUhu4+SgFKkAtDC9KvH0ufc9wpOq0JiPUFUJohHJERVDLMPdvTbXVF9Ir3ut8Ek9zbzvtu7fZuh5eiW9az89QOrDFAVCtjO8uJfYq8x/eqxdNN9Gz6TYDrT6OiaFm+EBkVYSd2kDyvoVzOJUTfeafTG4QX8C56a08PI3eyBVQaFV6jc8x+HpyffTravr2ibeiwt8Hfh88m5/FsU3AyuxX3g4Wpwh+sGPFwNW4lnXfblYx62y6opeLiaFqvUL8HDzUy4xxM9XA1W6g/g4WawUj8BD1fjVepfBqzUwcPVfOe+v8fD1Tx6lfoXwavUvww/8iY2lfpvo6lOm4VHHeIAAAAASUVORK5CYII=" alt="">
        </div>
        

        <div class="header">
            <p class="header__company-name">Travel Buddy Indonesia</p>
            <p class="header__address">
                Jalan Pelita Abdul Majid No.5<br>
                Kebayoran Baru, Jakarta 12150<br>
            </p>
        </div>
        <div class="invoice">
            <h2 class="invoice__title">Invoice #{{ $order->order_number }}</h2>
            <p>Invoice date: {{ Carbon::now()->formatLocalized('%A, %e %B %Y') }}</p>
            <p>Due date: {{ Carbon::now()->addDays(5)->formatLocalized('%A, %e %B %Y') }}</p>
        </div>

        <div class="invoiced-to">
            <h2 class="invoiced-to__title">Invoiced to</h2>
            <p>{{ $order->name }}</p>
        </div>

        <div class="payment-detail">
            @php
                $startDate = new Carbon($order->start_date);
                $endDate = (new Carbon($order->end_date))->addDay();

                $interval = new DateInterval('P1D');
                $dateRange = new DatePeriod($startDate, $interval ,$endDate);
            @endphp
            <table width="70%">
                <thead>
                    <tr>
                        <th width="40%" class="align-left">Tanggal</th>
                        <th width="30%" class="align-left">Waktu</th>
                        <th width="30%" class="align-left">Harga</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dateRange as $index => $date)
                        @php
                            $dt = new Carbon($date);
                            $formatedDate = $dt->formatLocalized('%e %B %Y');
                        @endphp
                        <tr class="table__row">
                            <td>{{ $formatedDate }}</td>
                            <td>{{ $order->hours_per_day[$index] }} Jam</td>
                            <td>{{ Helper::formatCurrency((int) $order->price_per_day * (int) $order->hours_per_day[$index]) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
             <p>
                Sub Total: <strong>{{ Helper::formatCurrency($order->price_estimation) }}</strong><br>
                5.00% PPN: <strong>{{ Helper::formatCurrency($ppn) }}</strong><br>
                Total: <strong>{{ Helper::formatCurrency($totalPrice) }}</strong><br>
            </p>
        </div>
        <div class="description">
            <table class="styled" width="100%">
                <thead>
                    <tr>
                        <th width="60%">Deskripsi</th>
                        <th width="40%">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dateRange as $index => $date)
                        @php
                            $dt = new Carbon($date);
                            $formatedDate = $dt->formatLocalized('%e %B %Y');
                        @endphp
                        <tr class="table__row">
                            <td>{{ $order->trip_details['destination'] }}, tanggal {{ $formatedDate }} selama {{ $order->hours_per_day[$index] }} jam</td>
                            <td class="align-right">{{ Helper::formatCurrency((int) $order->price_per_day * (int) $order->hours_per_day[$index]) }}</td>
                        </tr>
                    @endforeach
                    <tr class="footer">
                        <td class="footer__title">Subtotal</td>
                        <td class="price"><strong>{{ Helper::formatCurrency($order->price_estimation) }}</strong></td>
                    </tr>
                    <tr class="footer">
                        <td class="footer__title">PPN</td>
                        <td class="price"><strong>{{ Helper::formatCurrency($ppn) }}</strong></td>
                    </tr>
                    <tr class="footer">
                        <td class="footer__title">Total</td>
                        <td class="price"><strong>{{ Helper::formatCurrency($totalPrice) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="footer">
            <div class="footer-img-wrapper">
                <img class="footer-img" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaoAAABCCAMAAAAxMWKfAAAA8FBMVEUAAAAmJibwSwMvLy8nJyc0NDQoKCjvSwMoKCjxSgInJyf/UBkoKCjyTgQmJiYnJyfwSgLwSwLwSwLwSwMmJibwTAMoKCgnJyfwSgLwSwIoKCjzTAZDQ0PwSwLyUw2rgIDwSgLwSgImJib/YyUoKCj/cELySwQoKCgnJyfxSwPwSwLwSgPwSwPxSgQnJyf0TAXwSgLwSgPwSwPwSgImJibxTgfwSgImJiYmJiYvLy8oKCj3Tgj9VBInJycnJyfxTAP1TgrwSwTySwPwSwPwSwLvTAMrKyvwSgPxSwLySwTyTQYsLCzwSwPwTAUpKSnvSgJyubitAAAAT3RSTlMAgL4PMwhMwzh/OwofO3NX++HnmXlUKUnbZkAqA9YSAvbyfQYtAz4TZl56yKSKRC/ttqB3cCLOa10KJB0NYVFGGIVNlHJRHK5rNyYWqjMYNto7dAAACaxJREFUeNrtnGlb4jAQgKdcC8gCajlWueQQWBEvlEsQFVgVtf//3+yu0kySadFI3afu0/eTkGn1mdckk6QAi8B7yYGrCKaXPAPh5oc1zdnV0yV8DrXUkn1nY5Gw9l6C4Cr82pIQEL4Z9iTzreZn6NowlvhUYj1VxmpKrV0AT5UtLlJlGMnDDU+VHe5SZRj5oafKBrepMrJDT5U1rlNl5CueKkvcp8o481RZ4kJVpeGXUuWPiWSYmoHUUgRX8S5Vv/YFbs5aF7yr6ZdSJRN2ay/6kKptIOxnUdWBp8oKt6iCjUd0NfFUWeAaVVDYYqq+e6oscI8qaDJVqf9dVX1QDVQHfrAgWJ8P0jF/DpyhP6iG2+1oIP1cdFDVkKm6W8fN+f1N6n5C068Q67iq4M6Svwb8oc4yoBNaAE9u0G5oS0aB18vNK2MZkzRQcpat83ZDY+iJQN8pVZAUS8Bp5JUprUEiS6SyvnZ1eLC8yfHpTYWkXyH2MsK4pGM1a7taqYpmZQHFsK5xJOroM9zReMrtHEDOfBVNaKbgIhCqmskcTGIjTULfCTqj6lwaAM25axNkflpOapW7TYNnb1qRVKnE4vKhCTJnOKsqqkpDRk5f2sx2XJPpzqHPVA1wvQYEZqXBfmVPsyBedUTVD8Pk6SOqZmY8kr+SVCnE4l9zAhI1pvkAFFXtBDSZ9usIFtIsKM/rTBV0tSU9kFloJqaKGIoX2XFA1cOe2XxcUVdVOzSsSImqFGIv2Z+TLIDIDXY4VVXdMhmU+i+DX0OzJJ5GVWi5DhIhFr8sRwJlzY5QcU1VlSaOSNegrKp2YrNJdYPpV4zdtq1y2PVbFSVV9v/lxYRds46qgjp2RAHaMtBWEFVR9bgrcJ+aHm5yKZuoq7o1bNjD9CvGHrH3v0ndnzVEYG1V8ZegjPYmUS5Kz4EA9rfX+r+uaysoLxzbrk2Bsqo74018oBp7wF6LheYhGxkna6nCf/GYmMrGuD3u6TSwzl4EgAdnsQT8pdjQkE4omo6loyGuumwUHVI1BWVVk5LUPY5tVSnENq2PZTbYBS34oKpeJpyuRntlc24ZaUio/prIYDVOnPZInQei6ZhZubPAqjkz5apMqJZ2RNVWE9RV3fI3iMzOAYY3kSxJv2IsOrmwLlTvP6YqsYBX6j0tLM0s3Rgw+j1ZFQbOgWPMrn7tVOhEkJBj90s4oWq7AOqqhlxHOWU32DhMkvSrxWKhuAvIBanU1VSlAam+dKoeWswBT1RSBWwUGwPSL4vjYlozyQDCl5nlvgOq8j8q6qoi7PLjK+B4ypL0K8U+cDUp455U6mqq2iATZKmO90FkLKkKa5hsKlQPiheNQOLZbAk7Mldd7CqrOrDL3m6JpF8lFnwG/RNapFJXUtXIgUyadDi8ThdVBXV8yehInShOdjVI6NiZsmLvQVHVZdJ2X+GMT79qLECKbiAVSqRSV1KVBkIIpxDCDqoSY7tYVEgr4znWHoQ2NjlSrGcLaqpmTPIQJIrfuPSrxgJU2P7TLRFamnxIVR0IoxUag6hK2kF6JqZ70iJLjxN0s+n9z1YcCXxvXvuEWb2mogpfnQLhiku/SuySazax1ZY+N8kNlFSVi0Domm0YLzWiKujJc15Ol8a7He0d9NfYWNq95eaKppKqwxUnkhVuB0Ihlm72X8nbf0/KquigJM4tHbAggarEiS1exDeEyzPaO3hWUEU52sLSQknVKZbUlANMv0Is3e47lV4fwIdUJYBQ5DRS2pKqYgc7keQS5b7NYs3zqizO4gqqMH0FoPzC9CvGSnPbS703SWKvdEpVjqutKVE0Ib0xFhdVelBFVX09VbDLEnG7WlUEVfEl9QZQWph+lVh6NHUDf5iy6r3mmCrQsaijhARV1E2Y5DukvQP/mqrgRNjLNrO0BzKngqpH3FWl+DD9KrGMiLDhlzdf/QTnVHWwLKOMZFUQEk4RGzikLdn5VFWk4NoSdnAmIIIt34VdvX2gZDH9KrGMYZIbAZ9YpV5wUFUD53qKTlQ988V5HUdP8vsTAXty66r6yVIBf/ChEJFiSWiJWGz/0BLOB0qxdAKbcaJPwUFVPTxlJMw1ogpGXNdo0zVZDO2tYF1VLWEj59Fus+0Jyw/hsCJPJ5AzLv1KsXS19QgVttV+5KQq3MPz0x5noSrNvdXhDurJQsv/eaomJaEUvsZXIj5R1cT+4z7ne1z6VWKRYtYcATdSOJU6qWqu2bZWNQtVuTirQ2KkQ5JV8qeoGuLzXL/w/5lWxjMDVYlTV+mB7hVh+tViaWdLnbCfHFVV1MXnjZB+R1BFCod5CDfakTC5HbJwRNUVdxx7Jm6tZiuAFDZlVYf4FESFDGmYfpVYBNdS35JYqTuiih516AFAxAfEohYHVCFT8tj6oZhyGgSCCa26rqrC/XXe4BhKBxYH52Cyb5pCVbgeM/L33E1b5LhQIRZ5pB+rdFbVQkNGdZzDypqgirolh8KkXO/FODU7f9VX1VTtZQU2MX3C5NTEd45Tr31g4yfGYm14yn+dwiW8UGsek0N4lVhk3xApFZxTRVNfTkQH/px/vtPVBKJkdkMa9FkzdN8OxPz+50F7tDRfVVD1Nil2BoGULran179w6udVnSfF75U5u9s+KJFHWxRjkbzYug1Oq6qXtTeJksIQqZJqZBUBB1U98vMHhaqCyHseGFOMRaZi65HjqiCgogpdYKUu0V7tyjFVW2yAqR28UxX8Uki/Qiye/JJGJ1XZn1yUw0QV1uvWRTl5WpcwdkpV/lxa61BaRNXGhWHNaRIzrB6L8xsy+wxVxbFNX/ITVWSjr+wHQg7dU/vgkCpfQZjSLVwlmxtEFVxa95WT2iZJv1IsPqWElbrTquw/EBCCnLUqP0bb3LOqa5Z0YuCMqvyMHjhKbO0DHDNVSCRpEC4u2S/1gXIsckEe/nVcFcxH9GMg3H5t1LZojIEl/oyFrG41B06oOm7NiiBT2GZpxQL7AlUhu4+SgFKkAtDC9KvH0ufc9wpOq0JiPUFUJohHJERVDLMPdvTbXVF9Ir3ut8Ek9zbzvtu7fZuh5eiW9az89QOrDFAVCtjO8uJfYq8x/eqxdNN9Gz6TYDrT6OiaFm+EBkVYSd2kDyvoVzOJUTfeafTG4QX8C56a08PI3eyBVQaFV6jc8x+HpyffTravr2ibeiwt8Hfh88m5/FsU3AyuxX3g4Wpwh+sGPFwNW4lnXfblYx62y6opeLiaFqvUL8HDzUy4xxM9XA1W6g/g4WawUj8BD1fjVepfBqzUwcPVfOe+v8fD1Tx6lfoXwavUvww/8iY2lfpvo6lOm4VHHeIAAAAASUVORK5CYII=" alt="">
            </div>
            <a href="//travelbuddy.id">travelbuddy.id</a>
        </div>
    </div>
@endsection