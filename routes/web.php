<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/syarat-dan-ketentuan', function() {
    return view('frontend.help.syarat-dan-ketentuan');
});

/*
|--------------------------------------------------------------------------
| Buddy Routes
|--------------------------------------------------------------------------
*/
Route::prefix('buddy')->name('buddy.')->group(function() {
    Route::get('/','BuddyController@index')->name('home');
    
    // Route for booking a buddy
    Route::get('/{id}/book', 'BuddyController@book')->name('book');
    Route::post('/{id}/book', 'BuddyController@book')->name('book');
    Route::get('/book/success', function() {
        return view('frontend/buddy/order-success');
    })->name('book.success');
    
    // Buddy's profile
    Route::get('/{id}', 'BuddyController@profile')
        ->where(['id' => '[0-9]+'])
        ->name('profile');

    Route::post('/create', 'BuddyController@create')->name('create');
    
    Route::get('/search','BuddyController@search')->name('search');

    Route::post('order/accept','BuddyController@acceptOrder')->name('order.accept');
    Route::post('order/decline','BuddyController@declineOrder')->name('order.decline');
});

Route::get('/become-a-buddy', 'BuddyController@join')->name('buddy.join');

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
Route::prefix('admin')
    ->name('admin.')
    ->namespace('Admin')
    ->group(function() {
    
    route::get('buddy', 'BuddyController@index')->name('buddy');
    route::get('verification', 'BuddyController@verification')->name('verification');
    //  route::get('buddy/verification', function () {
    //     return 'hello';
    // });
    // route::get('buddy/verification', 'BuddyController@verification')->name('verification')
      

    route::delete('buddy/{id}', 'BuddyController@delete')->name('buddy.delete');

    route::get('buddy/view/{id}', 'BuddyController@view')->name('buddy.view');
    
    Route::get('order', 'OrderController@index')->name('order');
    Route::put('order/status', 'OrderController@orderStatus')->name('order.status');

    Route::get('order/{id}', 'OrderController@view')->name('order.item');
    Route::post('order/confirm', 'OrderController@confirm')->name('order.confirm');
    
    Route::get('order/{id}/update', 'OrderController@updateView')->name('order.update.view');
    Route::put('order/{id}/update', 'OrderController@update')->name('order.update');
    
    Route::get('/', 'HomeController@index')->name('home');
});

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
*/
Route::prefix('user')
    ->name('user.')
    ->group(function() {
    
    Route::put('/password', 'UserController@changePassword')->name('changePassword');
    Route::post('/update', 'UserController@update')->name('update');
    Route::post('/update/picture', 'UserController@updatePicture')->name('updatePicture');
});


Route::prefix('bantuan')->name('help.')->group(function () {
    Route::get('/', function() {
        return view('frontend.help.menggunakan-jasa-kami');
    })->name('home');
    Route::get('menggunakan-jasa-kami', function() {
        return view('frontend.help.menggunakan-jasa-kami');
    })->name('menggunakan-jasa-kami');
    Route::get('faq', function() {
        return view('frontend.help.faq');
    })->name('faq');
});

/*
|--------------------------------------------------------------------------
| About Us
|--------------------------------------------------------------------------
*/
Route::get('/about-us', function() {
    return view('frontend.about-us');
});

/*
|--------------------------------------------------------------------------
| Dashboard
|--------------------------------------------------------------------------
*/
Route::prefix('dashboard')->name('dashboard.')->group(function() {
    Route::get('/','DashboardController@index')->name('home');
    Route::get('buddy', 'DashboardController@buddy')->name('buddy');
    Route::get('order', 'DashboardController@order')->name('order');
    Route::get('settings', 'DashboardController@settings')->name('settings');
});

/*
|--------------------------------------------------------------------------
| Email verification
|--------------------------------------------------------------------------
*/
Route::get('/register/verification', function() {
    return view('email-verif.verification');
});
Route::get('/verification', function() {
    return view('email-verif.must-verif');
});

Route::get('/register/verification/resend', 'RegisterController@resend');
Route::post('/register/verification/resend', 'RegisterController@resend');
Route::get('/verifyemail/{token}', 'RegisterController@verify');


/*
|--------------------------------------------------------------------------
| API
|--------------------------------------------------------------------------
*/
Route::get('/api/country', 'CountryController@index');
Route::get('/api/city', 'CountryController@city');
Route::get('/api/price', 'CountryController@price');

/*
|--------------------------------------------------------------------------
| For testing purposes
|--------------------------------------------------------------------------
*/
// Route::post('testpost', function(\Illuminate\Http\Request $request) {
//     echo '<pre>';
//     var_dump($request->all());
//     echo '</pre>';
// });

// Route::get('testviewmail', function() {
//     $user = \App\Models\User::find(1);

//     return view ('mail.email-verification', compact('user'));
// });

// Route::get('testmail', function() {
//     $order = \App\Order::first();
//     Mail::to('husnimun@gmail.com')
//     ->send(new \App\Mail\ResetPassword('asdwasdwasdw'));
// });

// Route::get('testmail', function() {
//     $user = \App\Models\User::find(3);
//     Mail::to('husnimunaya@gmail.com')
//         ->send(new \App\Mail\EmailVerification($user));
// });

// Route::get('mailable', function() {
//     $order = \App\Order::find(1);

//     return new App\Mail\OrderReceivedBuddy($order);
// });

// Route::get('test', function() {
//     return view('test');
// });

// Route::get('buddysearch', function(\Illuminate\Http\Request $request) {
//     $location = $request->input('location');        
//     $buddies = \App\Buddy::search($location)->get();

//     return response()->json(['data' => $buddies], 200, [], JSON_NUMERIC_CHECK);
// });